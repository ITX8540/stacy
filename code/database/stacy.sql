--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

-- Started on 2016-12-06 22:17:57

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3976 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 1 (class 3079 OID 18527)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 3977 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- TOC entry 8 (class 3079 OID 18536)
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- TOC entry 3978 (class 0 OID 0)
-- Dependencies: 8
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- TOC entry 7 (class 3079 OID 18659)
-- Name: ltree; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS ltree WITH SCHEMA public;


--
-- TOC entry 3979 (class 0 OID 0)
-- Dependencies: 7
-- Name: EXTENSION ltree; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION ltree IS 'data type for hierarchical tree-like structures';


--
-- TOC entry 3 (class 3079 OID 20461)
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- TOC entry 3980 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- TOC entry 6 (class 3079 OID 18834)
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- TOC entry 3981 (class 0 OID 0)
-- Dependencies: 6
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- TOC entry 5 (class 3079 OID 20307)
-- Name: tsearch2; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS tsearch2 WITH SCHEMA public;


--
-- TOC entry 3982 (class 0 OID 0)
-- Dependencies: 5
-- Name: EXTENSION tsearch2; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tsearch2 IS 'compatibility package for pre-8.3 text search functions';


--
-- TOC entry 4 (class 3079 OID 20450)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- TOC entry 3983 (class 0 OID 0)
-- Dependencies: 4
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = public, pg_catalog;

--
-- TOC entry 3399 (class 2753 OID 20526)
-- Name: _uuid_ops; Type: OPERATOR FAMILY; Schema: public; Owner: stacy
--

CREATE OPERATOR FAMILY _uuid_ops USING gin;


ALTER OPERATOR FAMILY public._uuid_ops USING gin OWNER TO stacy;

--
-- TOC entry 3262 (class 2616 OID 20527)
-- Name: _uuid_ops; Type: OPERATOR CLASS; Schema: public; Owner: postgres
--

CREATE OPERATOR CLASS _uuid_ops
    DEFAULT FOR TYPE uuid[] USING gin FAMILY _uuid_ops AS
    STORAGE uuid ,
    OPERATOR 1 &&(anyarray,anyarray) ,
    OPERATOR 2 @>(anyarray,anyarray) ,
    OPERATOR 3 <@(anyarray,anyarray) ,
    OPERATOR 4 =(anyarray,anyarray) ,
    FUNCTION 1 (uuid[], uuid[]) uuid_cmp(uuid,uuid) ,
    FUNCTION 2 (uuid[], uuid[]) ginarrayextract(anyarray,internal,internal) ,
    FUNCTION 3 (uuid[], uuid[]) ginqueryarrayextract(anyarray,internal,smallint,internal,internal,internal,internal) ,
    FUNCTION 4 (uuid[], uuid[]) ginarrayconsistent(internal,smallint,anyarray,integer,internal,internal,internal,internal);


ALTER OPERATOR CLASS public._uuid_ops USING gin OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 213 (class 1259 OID 20555)
-- Name: Classification; Type: TABLE; Schema: public; Owner: stacy
--

CREATE TABLE "Classification" (
    "Id" uuid NOT NULL,
    "Name" text,
    "Description" text,
    "ClassificationTypeId" uuid NOT NULL,
    "ParentId" uuid
);


ALTER TABLE "Classification" OWNER TO stacy;

--
-- TOC entry 212 (class 1259 OID 20542)
-- Name: ClassificationType; Type: TABLE; Schema: public; Owner: stacy
--

CREATE TABLE "ClassificationType" (
    "Id" uuid NOT NULL,
    "Name" text,
    "Description" text,
    "ParentId" uuid
);


ALTER TABLE "ClassificationType" OWNER TO stacy;

--
-- TOC entry 211 (class 1259 OID 20536)
-- Name: Configuration; Type: TABLE; Schema: public; Owner: stacy
--

CREATE TABLE "Configuration" (
    "Key" character varying(100),
    "Value" text
);


ALTER TABLE "Configuration" OWNER TO stacy;

--
-- TOC entry 214 (class 1259 OID 20573)
-- Name: Event; Type: TABLE; Schema: public; Owner: stacy
--

CREATE TABLE "Event" (
    "Id" uuid NOT NULL,
    "EventTypeId" uuid NOT NULL,
    "ParentId" uuid
);


ALTER TABLE "Event" OWNER TO stacy;

--
-- TOC entry 217 (class 1259 OID 20619)
-- Name: EventContact; Type: TABLE; Schema: public; Owner: stacy
--

CREATE TABLE "EventContact" (
    "Id" uuid NOT NULL,
    "EventId" uuid NOT NULL,
    "Website" text,
    "Email" text,
    "PhoneNumber" text,
    "ExtraInfo" hstore
);


ALTER TABLE "EventContact" OWNER TO stacy;

--
-- TOC entry 219 (class 1259 OID 20645)
-- Name: EventDetails; Type: TABLE; Schema: public; Owner: stacy
--

CREATE TABLE "EventDetails" (
    "Id" uuid NOT NULL,
    "EventId" uuid NOT NULL,
    "Name" text,
    "Description" text,
    "Summary" text,
    "EventRoomId" uuid,
    "PresenterId" uuid,
    "StartsAt" timestamp with time zone,
    "EndsAt" timestamp with time zone,
    "Tags" text[],
    "ExtraInfo" hstore
);


ALTER TABLE "EventDetails" OWNER TO stacy;

--
-- TOC entry 215 (class 1259 OID 20588)
-- Name: EventLocation; Type: TABLE; Schema: public; Owner: stacy
--

CREATE TABLE "EventLocation" (
    "Id" uuid NOT NULL,
    "EventId" uuid NOT NULL,
    "Country" text,
    "City" text,
    "FullAddress" text,
    "Place" text,
    "ExtraInfo" hstore
);


ALTER TABLE "EventLocation" OWNER TO stacy;

--
-- TOC entry 218 (class 1259 OID 20632)
-- Name: EventPresenter; Type: TABLE; Schema: public; Owner: stacy
--

CREATE TABLE "EventPresenter" (
    "Id" uuid NOT NULL,
    "EventId" uuid NOT NULL,
    "Name" text,
    "Description" text,
    "ExtraInfo" hstore
);


ALTER TABLE "EventPresenter" OWNER TO stacy;

--
-- TOC entry 216 (class 1259 OID 20601)
-- Name: EventRoom; Type: TABLE; Schema: public; Owner: stacy
--

CREATE TABLE "EventRoom" (
    "Id" uuid NOT NULL,
    "EventId" uuid NOT NULL,
    "Name" text,
    "Description" text,
    "EventRoomTypeId" uuid NOT NULL,
    "ExtraInfo" hstore
);


ALTER TABLE "EventRoom" OWNER TO stacy;

--
-- TOC entry 3819 (class 2606 OID 20549)
-- Name: ClassificationType ClassificationType_pkey; Type: CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "ClassificationType"
    ADD CONSTRAINT "ClassificationType_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 3821 (class 2606 OID 20562)
-- Name: Classification Classification_pkey; Type: CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "Classification"
    ADD CONSTRAINT "Classification_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 3829 (class 2606 OID 20626)
-- Name: EventContact EventContact_pkey; Type: CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "EventContact"
    ADD CONSTRAINT "EventContact_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 3833 (class 2606 OID 20652)
-- Name: EventDetails EventDetails_pkey; Type: CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "EventDetails"
    ADD CONSTRAINT "EventDetails_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 3825 (class 2606 OID 20595)
-- Name: EventLocation EventLocation_pkey; Type: CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "EventLocation"
    ADD CONSTRAINT "EventLocation_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 3831 (class 2606 OID 20639)
-- Name: EventPresenter EventPresenter_pkey; Type: CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "EventPresenter"
    ADD CONSTRAINT "EventPresenter_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 3827 (class 2606 OID 20608)
-- Name: EventRoom EventRoom_pkey; Type: CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "EventRoom"
    ADD CONSTRAINT "EventRoom_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 3823 (class 2606 OID 20577)
-- Name: Event Event_pkey; Type: CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "Event"
    ADD CONSTRAINT "Event_pkey" PRIMARY KEY ("Id");


--
-- TOC entry 3834 (class 2606 OID 20550)
-- Name: ClassificationType ClassificationType_ParentId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "ClassificationType"
    ADD CONSTRAINT "ClassificationType_ParentId_fkey" FOREIGN KEY ("ParentId") REFERENCES "ClassificationType"("Id");


--
-- TOC entry 3835 (class 2606 OID 20563)
-- Name: Classification Classification_ClassificationTypeId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "Classification"
    ADD CONSTRAINT "Classification_ClassificationTypeId_fkey" FOREIGN KEY ("ClassificationTypeId") REFERENCES "ClassificationType"("Id");


--
-- TOC entry 3836 (class 2606 OID 20568)
-- Name: Classification Classification_ParentId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "Classification"
    ADD CONSTRAINT "Classification_ParentId_fkey" FOREIGN KEY ("ParentId") REFERENCES "Classification"("Id");


--
-- TOC entry 3842 (class 2606 OID 20627)
-- Name: EventContact EventContact_EventId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "EventContact"
    ADD CONSTRAINT "EventContact_EventId_fkey" FOREIGN KEY ("EventId") REFERENCES "Event"("Id");


--
-- TOC entry 3844 (class 2606 OID 20653)
-- Name: EventDetails EventDetails_EventId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "EventDetails"
    ADD CONSTRAINT "EventDetails_EventId_fkey" FOREIGN KEY ("EventId") REFERENCES "Event"("Id");


--
-- TOC entry 3839 (class 2606 OID 20596)
-- Name: EventLocation EventLocation_EventId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "EventLocation"
    ADD CONSTRAINT "EventLocation_EventId_fkey" FOREIGN KEY ("EventId") REFERENCES "Event"("Id");


--
-- TOC entry 3843 (class 2606 OID 20640)
-- Name: EventPresenter EventPresenter_EventId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "EventPresenter"
    ADD CONSTRAINT "EventPresenter_EventId_fkey" FOREIGN KEY ("EventId") REFERENCES "Event"("Id");


--
-- TOC entry 3840 (class 2606 OID 20609)
-- Name: EventRoom EventRoom_EventId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "EventRoom"
    ADD CONSTRAINT "EventRoom_EventId_fkey" FOREIGN KEY ("EventId") REFERENCES "Event"("Id");


--
-- TOC entry 3841 (class 2606 OID 20614)
-- Name: EventRoom EventRoom_EventRoomTypeId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "EventRoom"
    ADD CONSTRAINT "EventRoom_EventRoomTypeId_fkey" FOREIGN KEY ("EventRoomTypeId") REFERENCES "Classification"("Id");


--
-- TOC entry 3838 (class 2606 OID 20583)
-- Name: Event Event_EventTypeId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "Event"
    ADD CONSTRAINT "Event_EventTypeId_fkey" FOREIGN KEY ("EventTypeId") REFERENCES "Classification"("Id");


--
-- TOC entry 3837 (class 2606 OID 20578)
-- Name: Event Event_ParentId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: stacy
--

ALTER TABLE ONLY "Event"
    ADD CONSTRAINT "Event_ParentId_fkey" FOREIGN KEY ("ParentId") REFERENCES "Event"("Id");


--
-- TOC entry 3975 (class 0 OID 0)
-- Dependencies: 10
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO stacy;


-- Completed on 2016-12-06 22:17:58

--
-- PostgreSQL database dump complete
--

