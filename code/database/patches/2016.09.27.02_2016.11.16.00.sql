DO LANGUAGE plpgsql $$
DECLARE 
OLDVERSION text = '2016.09.27.02';
NEWVERSION text = '2016.11.16.00';
VCOUNT NUMERIC = 0;

BEGIN

SELECT COUNT(*) INTO VCOUNT FROM "Configuration" WHERE "Key" = 'DatabaseSchemaVersion' AND "Value" = OLDVERSION;

UPDATE "Configuration" SET "Value" = NEWVERSION WHERE "Key" = 'DatabaseSchemaVersion';


IF VCOUNT = 1 THEN

-- START CHANGES

ALTER TABLE "EventDetails"
	ALTER COLUMN "StartsAt" TYPE timestamp with time zone /* TYPE change - table: EventDetails original: date new: timestamp with time zone */,
	ALTER COLUMN "EndsAt" TYPE timestamp with time zone /* TYPE change - table: EventDetails original: date new: timestamp with time zone */;


--END CHANGES

RAISE NOTICE 'Transaction OK.';

ELSE

	RAISE EXCEPTION 'WRONG DB VERSION %', OLDVERSION;

END IF;

EXCEPTION WHEN OTHERS THEN
	RAISE NOTICE 'Transaction is rolled back Error: %, State: %', SQLERRM, SQLSTATE;


END$$;