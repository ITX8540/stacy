DO LANGUAGE plpgsql $$
DECLARE 
OLDVERSION text = '2016.09.27.01';
NEWVERSION text = '2016.09.27.02';
VCOUNT NUMERIC = 0;
EVENT_ID uuid;
CONF_EVENT_TYPE_ID uuid;
CONF_ROOM_TYPE_ID uuid;
CONF_Cl_ID uuid;
KEYNOTE_Cl_ID uuid;
WORKSHOP_Cl_ID uuid;
LECTURE_Cl_ID uuid;
AUDITORIUM_Cl_ID uuid;
CLASSROOM_Cl_ID uuid;
TOPCONF_ID uuid;
EVENT1 uuid;
EVENT2 uuid;
EVENT3 uuid;
EVENT4 uuid;
EVENT5 uuid;
EVENT_LOCATION_ID uuid;
EVENT_ROOM1 uuid;
EVENT_ROOM2 uuid;
EVENT_CONTACT1 uuid;
PRESENTER1 uuid;
PRESENTER2 uuid;
PRESENTER3 uuid;
PRESENTER4 uuid;
PRESENTER5 uuid;
EVENT_DATA1 uuid;
EVENT_DATA2 uuid;
EVENT_DATA3 uuid;
EVENT_DATA4 uuid;
EVENT_DATA5 uuid;
EVENT_DATA6 uuid;
BEGIN

SELECT COUNT(*) INTO VCOUNT FROM "Configuration" WHERE "Key" = 'DatabaseSchemaVersion' AND "Value" = OLDVERSION;

UPDATE "Configuration" SET "Value" = NEWVERSION WHERE "Key" = 'DatabaseSchemaVersion';


IF VCOUNT = 1 THEN

SELECT uuid_generate_v4() INTO CONF_EVENT_TYPE_ID;
SELECT uuid_generate_v4() INTO CONF_ROOM_TYPE_ID;

INSERT INTO "ClassificationType"
	("Id","Name", "Description", "ParentId")
VALUES
	(CONF_EVENT_TYPE_ID, 'EventType','Event  Type',NULL),
	(CONF_ROOM_TYPE_ID, 'RoomType','Room  Type',NULL);

-- EventType Ids
SELECT uuid_generate_v4() INTO CONF_Cl_ID;
SELECT uuid_generate_v4() INTO KEYNOTE_Cl_ID;
SELECT uuid_generate_v4() INTO WORKSHOP_Cl_ID;
SELECT uuid_generate_v4() INTO LECTURE_Cl_ID;

-- RoomType Ids
SELECT uuid_generate_v4() INTO AUDITORIUM_Cl_ID;
SELECT uuid_generate_v4() INTO CLASSROOM_Cl_ID;

INSERT INTO "Classification"
	("Id","Name", "Description","ClassificationTypeId", "ParentId")
VALUES
	(CONF_Cl_ID, 'Conference','Conference Event Type', CONF_EVENT_TYPE_ID, NULL),
	(KEYNOTE_Cl_ID, 'Keynote','Keynote', CONF_EVENT_TYPE_ID, NULL),
	(WORKSHOP_Cl_ID, 'Workshop','Practical workshop', CONF_EVENT_TYPE_ID, NULL),
	(LECTURE_Cl_ID, 'Lecture','Lecture', CONF_EVENT_TYPE_ID, NULL),
	(AUDITORIUM_Cl_ID, 'Auditorium','Auditorium', CONF_ROOM_TYPE_ID, NULL),
	(CLASSROOM_Cl_ID, 'Classroom','Classroom', CONF_ROOM_TYPE_ID, NULL);


SELECT uuid_generate_v4() INTO TOPCONF_ID;
SELECT uuid_generate_v4() INTO EVENT1;
SELECT uuid_generate_v4() INTO EVENT2;
SELECT uuid_generate_v4() INTO EVENT3;
SELECT uuid_generate_v4() INTO EVENT4;
SELECT uuid_generate_v4() INTO EVENT5;

INSERT INTO "Event"
	("Id", "EventTypeId", "ParentId")
VALUES
	(TOPCONF_ID, CONF_Cl_ID, NULL),
	(EVENT1, KEYNOTE_Cl_ID, TOPCONF_ID),
	(EVENT2, WORKSHOP_Cl_ID, TOPCONF_ID),
	(EVENT3, LECTURE_Cl_ID, TOPCONF_ID),
	(EVENT4, LECTURE_Cl_ID, TOPCONF_ID),
	(EVENT5, LECTURE_Cl_ID, TOPCONF_ID);

SELECT uuid_generate_v4() INTO EVENT_LOCATION_ID;

INSERT INTO "EventLocation"
	("Id", "EventId", "Country", "City", "FullAddress", "Place")
VALUES
	(EVENT_LOCATION_ID, TOPCONF_ID, 'Estonia', 'Tallinn', 'Viru väljak 4, 10111 Tallinn, Estonia', 'Sokos Hotel Viru');

SELECT uuid_generate_v4() INTO EVENT_ROOM1;
SELECT uuid_generate_v4() INTO EVENT_ROOM2;

INSERT INTO "EventRoom"
	("Id", "EventId", "Name", "Description", "EventRoomTypeId","ExtraInfo")
VALUES
	(EVENT_ROOM1, TOPCONF_ID, 'Grande 1', 'Bigger room', AUDITORIUM_Cl_ID, NULL),
	(EVENT_ROOM2, TOPCONF_ID, 'Bolero 1', 'Smaller room', CLASSROOM_Cl_ID, NULL);

SELECT uuid_generate_v4() INTO EVENT_CONTACT1;

INSERT INTO "EventContact"
	("Id", "EventId", "Website", "Email", "PhoneNumber", "ExtraInfo")
VALUES
	(EVENT_CONTACT1, TOPCONF_ID, 'http://topconf.com/tallinn-2016/', '', '', '"twitter" => "@TopconfEE", "hashtag" => "#TopconfEE"');

-- Insert Conference Event Data

SELECT uuid_generate_v4() INTO PRESENTER1;
SELECT uuid_generate_v4() INTO PRESENTER2;
SELECT uuid_generate_v4() INTO PRESENTER3;
SELECT uuid_generate_v4() INTO PRESENTER4;
SELECT uuid_generate_v4() INTO PRESENTER5;

INSERT INTO "EventPresenter"
	("Id", "EventId", "Name", "Description", "ExtraInfo")
VALUES
	(PRESENTER1, TOPCONF_ID, 'Arie van Bennekum', 'Co-author of the Agile Manifesto', ''),
	(PRESENTER2, TOPCONF_ID, 'Gleb Smirnov', 'Senior Software Engineer - Plumbr', ''),
	(PRESENTER3, TOPCONF_ID, 'Juergen Hoeller', 'Principal Engineer - Pivotal', ''),
	(PRESENTER4, TOPCONF_ID, 'Alex Canessa', 'Senior Full Stack Developer at Digital Detox', '"website" => "www.detox.com/alex"'),
	(PRESENTER5, TOPCONF_ID, 'Holger Bartel', 'Front end developer', '');

SELECT uuid_generate_v4() INTO EVENT_DATA1;
SELECT uuid_generate_v4() INTO EVENT_DATA2;
SELECT uuid_generate_v4() INTO EVENT_DATA3;
SELECT uuid_generate_v4() INTO EVENT_DATA4;
SELECT uuid_generate_v4() INTO EVENT_DATA5;
SELECT uuid_generate_v4() INTO EVENT_DATA6;

INSERT INTO "EventDetails"
	("Id", "EventId", "Name", "Description", "EventRoomId", "PresenterId", "StartsAt", "EndsAt",
		"Tags", "ExtraInfo")
VALUES
----(1, 'Topconf 2016', 'IT conference in Baltics.', '2016-11-16', '2016-11-17');
	(EVENT_DATA1, TOPCONF_ID, 'Topconf 2016', 'IT conference in Baltics.', NULL, NULL, '2016-12-16 00:00:00', '2016-12-17 23:55:00',NULL, NULL),
	(EVENT_DATA2, EVENT1, 'Opening Keynote: True Agile is what you are', 'We can see Agile literally on every continent. It is not a hype anymore, it has matured.',
		EVENT_ROOM1, PRESENTER1, '2016-12-16 09:20:00', '2016-12-16 10:00:00',
		'{JVM, HotSpot, source code, GC, JLS}', ''),
	(EVENT_DATA3, EVENT2, 'The Mysteries Lie In Our Heads, Not In The JVM', 'Every once in a while each engineer faces a problem novel to them. Oftentimes, a quick internet search yields a solution.',
		EVENT_ROOM1, PRESENTER2, '2016-12-16 10:20:00', '2016-12-16 11:00:00',
		'{JVM, HotSpot, source code, GC, JLS}', '"requirements" => "bring laptop with you"'),
	(EVENT_DATA4, EVENT3, 'Modern Java Component Design with Spring 4.3', 'Springs programming and configuration model has a strong design philosophy with respect to application components and configuration artifacts.',
		EVENT_ROOM1, PRESENTER3, '2016-12-16 12:00:00', '2016-12-16 12:40:00',
		'{Spring, Java}', ''),
	(EVENT_DATA5, EVENT4, 'MyoJS – From Gesture to Code', 'The Myo armband measures the electrical activity from your muscles to detect what gesture your hand is making.',
		EVENT_ROOM2, PRESENTER4, '2016-12-16 10:20:00', '2016-12-16 12:00:00',
		'{history of the gesture control, myo: the armband technology, web}', ''),
	(EVENT_DATA6, EVENT5, 'Web Performance in the Age of HTTP2', 'Web performance optimisation has been gaining ground and is slowly getting more of its deserved recognition.',
		EVENT_ROOM2, PRESENTER5, '2016-12-16 12:00:00', '2016-12-16 12:40:00',
		'{web performance, optimisation, best practices, front end}', '');



--END CHANGES

RAISE NOTICE 'Transaction OK.';

ELSE

	RAISE EXCEPTION 'WRONG DB VERSION %', OLDVERSION;

END IF;

EXCEPTION WHEN OTHERS THEN
	RAISE NOTICE 'Transaction is rolled back Error: %, State: %', SQLERRM, SQLSTATE;


END$$;
