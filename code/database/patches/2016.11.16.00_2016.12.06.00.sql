DO LANGUAGE plpgsql $$
DECLARE 
OLDVERSION text = '2016.11.16.00';
NEWVERSION text = '2016.12.06.00';
VCOUNT NUMERIC = 0;
EVENT_ID uuid;
CONF_EVENT_TYPE_ID uuid;
CONF_ROOM_TYPE_ID uuid;
CONF_Cl_ID uuid;
KEYNOTE_Cl_ID uuid;
WORKSHOP_Cl_ID uuid;
LECTURE_Cl_ID uuid;
BREAK_Cl_ID uuid;
AUDITORIUM_Cl_ID uuid;
CLASSROOM_Cl_ID uuid;
TTU_MEKTORY_ID uuid;
EVENT1 uuid;
EVENT2 uuid;
EVENT3 uuid;
EVENT4 uuid;
EVENT5 uuid;
EVENT6 uuid;
EVENT7 uuid;
EVENT_LOCATION_ID uuid;
EVENT_ROOM1 uuid;
PRESENTER1 uuid;
PRESENTER2 uuid;
PRESENTER3 uuid;
PRESENTER4 uuid;
PRESENTER5 uuid;
PRESENTER6 uuid;
PRESENTER7 uuid;
PRESENTER8 uuid;
PRESENTER9 uuid;
PRESENTER10 uuid;
PRESENTER11 uuid;
PRESENTER12 uuid;
PRESENTER13 uuid;
PRESENTER14 uuid;
PRESENTER15 uuid;
EVENT_CONTACT1 uuid;
EVENT_DATA1 uuid;
EVENT_DATA2 uuid;
EVENT_DATA3 uuid;
EVENT_DATA4 uuid;
EVENT_DATA5 uuid;
EVENT_DATA6 uuid;
EVENT_DATA7 uuid;
EVENT_DATA8 uuid;
BEGIN

SELECT COUNT(*) INTO VCOUNT FROM "Configuration" WHERE "Key" = 'DatabaseSchemaVersion' AND "Value" = OLDVERSION;

UPDATE "Configuration" SET "Value" = NEWVERSION WHERE "Key" = 'DatabaseSchemaVersion';


IF VCOUNT = 1 THEN


--delete 
DELETE FROM "EventDetails";
DELETE FROM "EventPresenter";
DELETE FROM "EventContact";
DELETE FROM "EventRoom";
DELETE FROM "EventLocation";
DELETE FROM "Event";
DELETE FROM "Classification";


SELECT uuid_generate_v4() INTO CONF_EVENT_TYPE_ID;
SELECT uuid_generate_v4() INTO CONF_ROOM_TYPE_ID;

INSERT INTO "ClassificationType"
	("Id","Name", "Description", "ParentId")
VALUES
	(CONF_EVENT_TYPE_ID, 'EventType','Event  Type',NULL),
	(CONF_ROOM_TYPE_ID, 'RoomType','Room  Type',NULL);

-- EventType Ids
SELECT uuid_generate_v4() INTO CONF_Cl_ID;
SELECT uuid_generate_v4() INTO KEYNOTE_Cl_ID;
SELECT uuid_generate_v4() INTO WORKSHOP_Cl_ID;
SELECT uuid_generate_v4() INTO LECTURE_Cl_ID;
SELECT uuid_generate_v4() INTO BREAK_Cl_ID;
-- RoomType Ids
SELECT uuid_generate_v4() INTO AUDITORIUM_Cl_ID;
SELECT uuid_generate_v4() INTO CLASSROOM_Cl_ID;

INSERT INTO "Classification"
	("Id","Name", "Description","ClassificationTypeId", "ParentId")
VALUES
	(CONF_Cl_ID, 'Conference','Conference Event Type', CONF_EVENT_TYPE_ID, NULL),
	(KEYNOTE_Cl_ID, 'Keynote','Keynote', CONF_EVENT_TYPE_ID, NULL),
	(WORKSHOP_Cl_ID, 'Workshop','Practical workshop', CONF_EVENT_TYPE_ID, NULL),
	(LECTURE_Cl_ID, 'Lecture','Lecture', CONF_EVENT_TYPE_ID, NULL),
	(BREAK_Cl_ID, 'Break','Break', CONF_EVENT_TYPE_ID, NULL),
	(AUDITORIUM_Cl_ID, 'Auditorium','Auditorium', CONF_ROOM_TYPE_ID, NULL),
	(CLASSROOM_Cl_ID, 'Classroom','Classroom', CONF_ROOM_TYPE_ID, NULL);


SELECT uuid_generate_v4() INTO TTU_MEKTORY_ID;
SELECT uuid_generate_v4() INTO EVENT1;
SELECT uuid_generate_v4() INTO EVENT2;
SELECT uuid_generate_v4() INTO EVENT3;
SELECT uuid_generate_v4() INTO EVENT4;
SELECT uuid_generate_v4() INTO EVENT5;
SELECT uuid_generate_v4() INTO EVENT6;
SELECT uuid_generate_v4() INTO EVENT7;

INSERT INTO "Event"
	("Id", "EventTypeId", "ParentId")
VALUES
	(TTU_MEKTORY_ID, CONF_Cl_ID, NULL),
	(EVENT1, KEYNOTE_Cl_ID, TTU_MEKTORY_ID),
	(EVENT2, LECTURE_Cl_ID, TTU_MEKTORY_ID),
	(EVENT3, LECTURE_Cl_ID, TTU_MEKTORY_ID),
	(EVENT4, LECTURE_Cl_ID, TTU_MEKTORY_ID),
	(EVENT5, BREAK_Cl_ID, TTU_MEKTORY_ID),
	(EVENT6, LECTURE_Cl_ID, TTU_MEKTORY_ID),
	(EVENT7, BREAK_Cl_ID, TTU_MEKTORY_ID);

SELECT uuid_generate_v4() INTO EVENT_LOCATION_ID;

INSERT INTO "EventLocation"
	("Id", "EventId", "Country", "City", "FullAddress", "Place")
VALUES
	(EVENT_LOCATION_ID, TTU_MEKTORY_ID, 'Estonia', 'Tallinn', 'Raja 15, 12618 Tallinn, Estonia', 'TTU Mektory');

SELECT uuid_generate_v4() INTO EVENT_ROOM1;

INSERT INTO "EventRoom"
	("Id", "EventId", "Name", "Description", "EventRoomTypeId","ExtraInfo")
VALUES
	(EVENT_ROOM1, TTU_MEKTORY_ID, 'Big Hall', 'Mektory auditiorum', AUDITORIUM_Cl_ID, NULL);

SELECT uuid_generate_v4() INTO PRESENTER1;
SELECT uuid_generate_v4() INTO PRESENTER2;
SELECT uuid_generate_v4() INTO PRESENTER3;
SELECT uuid_generate_v4() INTO PRESENTER4;
SELECT uuid_generate_v4() INTO PRESENTER5;
SELECT uuid_generate_v4() INTO PRESENTER6;
SELECT uuid_generate_v4() INTO PRESENTER7;
SELECT uuid_generate_v4() INTO PRESENTER8;
SELECT uuid_generate_v4() INTO PRESENTER9;
SELECT uuid_generate_v4() INTO PRESENTER10;
SELECT uuid_generate_v4() INTO PRESENTER11;
SELECT uuid_generate_v4() INTO PRESENTER12;
SELECT uuid_generate_v4() INTO PRESENTER13;
SELECT uuid_generate_v4() INTO PRESENTER14;
SELECT uuid_generate_v4() INTO PRESENTER15;

INSERT INTO "EventPresenter"
	("Id", "EventId", "Name", "Description", "ExtraInfo")
VALUES
	(PRESENTER1, TTU_MEKTORY_ID, 'Avionica', 'Avionica builds transponders for drones to help their operators to comply with regulations and enhance safety in aviation.', ''),
	(PRESENTER2, TTU_MEKTORY_ID, 'CADirect', 'CADirect saves engineers 10-15% in BOM processing and CAD design time by integrating supplier and reseller product data directly to BOM and CAD software.', ''),
	(PRESENTER3, TTU_MEKTORY_ID, 'HappyMe', '', ''),
	(PRESENTER4, TTU_MEKTORY_ID, 'Medical Diagnostics', '', ''),
	(PRESENTER5, TTU_MEKTORY_ID, 'Meetallica', 'Website to find other musicians to their band.', ''),
	(PRESENTER6, TTU_MEKTORY_ID, 'Moday', 'A modular USB hub.', ''),
	(PRESENTER7, TTU_MEKTORY_ID, 'MoleQL', ' Augmented reality along with 3D animation technology will show how molecules and atoms interact and merge into new compounds right on the screenof your mobile device!', ''),
	(PRESENTER8, TTU_MEKTORY_ID, 'Music Wall Street', 'MWS takes crowdfunding to a new level by bringing together (i) investor-minded fans and (ii) artists or organizers of creative projects.', ''),
	(PRESENTER9, TTU_MEKTORY_ID, 'PacKing', '', ''),
	(PRESENTER10, TTU_MEKTORY_ID, 'PRAP', 'Production assistant app that will put an end to messy riders, tech lists, call sheets and other piles of chaotic notes that is the necessary everyday (d)evil of live events and music production related people.', ''),
	(PRESENTER11, TTU_MEKTORY_ID, 'RailWatch OÜ', '', ''),
	(PRESENTER12, TTU_MEKTORY_ID, 'Reflector Buddy', '', ''),
	(PRESENTER13, TTU_MEKTORY_ID, 'TarkEnergia', 'Tark Energia idee on serverikeskuste asemel toota nutikad radiaatorid, mis võimaldavad hajutada serverid majapidamiste vahel ning kasutada eralduvat soojust kodude kütmiseks.', ''),
	(PRESENTER14, TTU_MEKTORY_ID, 'TitanGrid', 'Our mission is to reduce risk in organizations by making reconnaissance more difficult for cyber attackers.', ''),
	(PRESENTER15, TTU_MEKTORY_ID, 'Vulcan Technologies', '', '');

SELECT uuid_generate_v4() INTO EVENT_CONTACT1;

INSERT INTO "EventContact"
	("Id", "EventId", "Website", "Email", "PhoneNumber", "ExtraInfo")
VALUES
	(EVENT_CONTACT1, TTU_MEKTORY_ID, 'http://ttu.ee/startup', 'anu.oks@ttu.ee', '+372 56603344', '');

SELECT uuid_generate_v4() INTO EVENT_DATA1;
SELECT uuid_generate_v4() INTO EVENT_DATA2;
SELECT uuid_generate_v4() INTO EVENT_DATA3;
SELECT uuid_generate_v4() INTO EVENT_DATA4;
SELECT uuid_generate_v4() INTO EVENT_DATA5;
SELECT uuid_generate_v4() INTO EVENT_DATA6;
SELECT uuid_generate_v4() INTO EVENT_DATA7;
SELECT uuid_generate_v4() INTO EVENT_DATA8;

INSERT INTO "EventDetails"
	("Id", "EventId", "Name", "Description", "EventRoomId", "PresenterId", "StartsAt", "EndsAt")
VALUES
	(EVENT_DATA1, TTU_MEKTORY_ID, 'TTU Mektory Start-up Competition 2016', '', 
		NULL, NULL, '2016-12-05 00:00:00', '2016-12-07 23:55:00'),
	(EVENT_DATA2, EVENT1, 'Opening Speeches and Greetings', '',
		EVENT_ROOM1, null, '2016-12-07 18:00:00', '2016-12-07 18:15:00'),
	(EVENT_DATA3, EVENT2, 'First round for startup pitches', '',
		EVENT_ROOM1, null, '2016-12-07 18:15:00', '2016-12-07 19:00:00'),
	(EVENT_DATA4, EVENT3, 'Inspiration speech', '',
		EVENT_ROOM1, null, '2016-12-07 19:00:00', '2016-12-07 19:15:00'),
	(EVENT_DATA5, EVENT4, 'Second round for startup pitches', '',
		EVENT_ROOM1, null, '2016-12-07 19:15:00', '2016-12-07 20:05:00'),
	(EVENT_DATA6, EVENT5, 'Coffe Break and Jury Discussion', '',
		EVENT_ROOM1, null, '2016-12-07 20:05:00', '2016-12-07 20:20:00'),
	(EVENT_DATA7, EVENT6, 'Award Ceremony', '',
		EVENT_ROOM1, null, '2016-12-07 20:20:00', '2016-12-07 20:35:00'),
	(EVENT_DATA8, EVENT7, 'Cake&Coffee and networking', '',
		EVENT_ROOM1, null, '2016-12-07 20:35:00', '2016-12-07 21:00:00');

--END CHANGES

RAISE NOTICE 'Transaction OK.';

ELSE

	RAISE EXCEPTION 'WRONG DB VERSION %', OLDVERSION;

END IF;

EXCEPTION WHEN OTHERS THEN
	RAISE NOTICE 'Transaction is rolled back Error: %, State: %', SQLERRM, SQLSTATE;


END$$;
