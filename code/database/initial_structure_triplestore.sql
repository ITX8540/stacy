--NOT used currently - it was initial version.
/*

--SQL for DB stacy_triplestore (kind a RDF / triplestore model to be dynamic so much as possible)

--this solution supports very dynamic datasets but requires more complex code in our application.

--can be done in different ways:
--  1.every conference has own table (main types and overall structure are in common tables - currently under type)
--     + secure and independent, easily maintenaible
--     - takes a little bit time to implement new tables, connections
--  2. all conference info to one table, then need to add conference ID column / row?

DROP TABLE type;
DROP TABLE topconf2016;

CREATE TABLE type (
	id int,
	name hstore,
	value text,
	parent_id text[];

INSERT INTO type
	(id, name, value, parent_id)
VALUES
	(1, 'address', '{country, city, long_address}', ''), --value hstore consists usually predefined fields for this type
	(2, 'event', '{title, summary, presenter, startAs, endsAt, room}', ''),
	(3, 'keynote', '', '2'),
	(4, 'lecture', '', '2'),
	(5, 'workshop', '{requirements}', '2');

--
CREATE TABLE topconf2016 (
	id int,
	name text,
	value text;

INSERT INTO topconf2016
	(id, name, value)
VALUES
(1, 'type', 'conference'),
(1, 'name', 'Topconf 2016'),
(1, 'description', 'Front End, Big Data, Project Management, Product Management, Java & The JVM, Reactive Architectures, Sustainable Development, Ops4Dev'),

(2, 'type', 'address'),
(2, 'long_address', 'Viru väljak 4, 10111 Tallinn, Estonia'),
(2, 'place', 'Sokos Hotel Viru'),
(2, 'city', 'Tallinn'),
(2, 'country', 'Estonia'),

(3, 'type', 'contact'),
(3, 'phone', ''),
(3, 'mail', ''),
(3, 'website', 'http://topconf.com/tallinn-2016/'),
(3, 'twitter', '@TopconfEE'),
(3, 'hashtag', '#TopconfEE'),

(50, 'type', 'room'),
(50, 'name', 'Grande 1'),
(50, 'description', 'Bigger room.'),

(51, 'type', 'room'),
(51, 'name', 'Bolero 1'),
(51, 'description', 'Smaller room.'),

(100, 'type', 'event'),
--(100, 'subtype', 'keynote'), -- or subtype under classifier
(100, 'title', 'Opening Keynote: True Agile is what you are'),
(100, 'summary', 'We can see Agile literally on every continent. It is not a hype anymore, it has matured.'),
(100, 'startsAt', '2016-11-16 09:20:00')
(100, 'endsAt', '2016-11-16 10:00:00')
(100, 'tags', '{JVM, HotSpot, source code, GC, JLS}'), --think better solution. hstore? array?
(100, 'room', '50'),
(100, 'presenter', '200'),
(200, 'type', 'presenter'),
(200, 'name', 'Arie van Bennekum'),
(200, 'description', 'Co-author of the Agile Manifesto'),

(101, 'type', 'event'),
(101, 'title', 'The Mysteries Lie In Our Heads, Not In The JVM'),
(101, 'summary', 'Every once in a while each engineer faces a problem novel to them. Oftentimes, a quick internet search yields a solution.'),
(101, 'startsAt', '2016-11-16 10:20:00')
(101, 'endsAt', '2016-11-16 11:00:00')
(101, 'tags', '{JVM, HotSpot, source code, GC, JLS}'),
(101, 'room', '50'),
(101, 'presenter', '201'),
(201, 'type', 'presenter'),
(201, 'name', 'Gleb Smirnov'),
(201, 'description', 'Senior Software Engineer - Plumbr'),

(102, 'type', 'event'),
(102, 'title', 'Modern Java Component Design with Spring 4.3'),
(102, 'summary', 'Spring programming and configuration model has a strong design philosophy with respect to application components and configuration artifacts.'),
(102, 'startsAt', '2016-11-16 12:00:00')
(102, 'endsAt', '2016-11-16 12:40:00')
(102, 'tags', '{Spring, Java}'),
(102, 'room', '50'),
(102, 'presenter', '202'),
(202, 'type', 'presenter'),
(202, 'name', 'Juergen Hoeller'),
(202, 'description', 'Principal Engineer - Pivotal'),

(103, 'type', 'event'),
(103, 'title', 'MyoJS – From Gesture to Code'),
(103, 'summary', 'The Myo armband measures the electrical activity from your muscles to detect what gesture your hand is making.'),
(103, 'startsAt', '2016-11-16 10:20:00')
(103, 'endsAt', '2016-11-16 12:00:00')
(103, 'tags', '{history of the gesture control, myo: the armband technology, web}'),
(103, 'room', '51'),
(103, 'presenter', '203'),
(203, 'type', 'presenter'),
(203, 'name', 'Alex Canessa'),
(203, 'description', 'Senior Full Stack Developer at Digital Detox'),

(104, 'type', 'event'),
(104, 'title', 'Web Performance in the Age of HTTP2'),
(104, 'summary', 'Web performance optimisation has been gaining ground and is slowly getting more of its deserved recognition.'),
(104, 'startsAt', '2016-11-16 12:00:00')
(104, 'endsAt', '2016-11-16 12:40:00')
(104, 'tags', '{web performance, optimisation, http2, best practices, front end}'),
(104, 'room', '51'),
(104, 'presenter', '204'),
(204, 'type', 'presenter'),
(204, 'name', 'Holger Bartel'),
(204, 'description', 'Front end developer');

(105, 'type', 'event'),
(105, 'title', 'Longer event for testing2'),
(105, 'summary', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),
(105, 'startsAt', '2016-11-16 12:00:00')
(105, 'endsAt', '2016-11-16 23:40:00')
(105, 'tags', '{web, AI}'),
(105, 'room', '51'),
(105, 'presenter', '203'),

(106, 'type', 'event'),
(106, 'title', 'Another longer event for testing.'),
(106, 'summary', 'Aenean sed sagittis metus. Morbi porta lorem ut elit iaculis elementum. '),
(106, 'startsAt', '2016-11-16 16:00:00')
(106, 'endsAt', '2016-11-16 19:00:00')
(106, 'tags', '{security}'),
(106, 'room', '50'),
(106, 'presenter', '201');
/*
(105, 'type', 'event'),
(105, 'title', ''),
(105, 'summary', ''),
(105, 'startsAt', '2016-11-16 10:20:00')
(105, 'endsAt', '2016-11-16 12:00:00')
(105, 'tags', '{}'),
(105, 'room', '50'),
(105, 'presenter', ''),
(205, 'type', 'presenter'),
(205, 'name', ''),
(205, 'description', ''),
*/

*/