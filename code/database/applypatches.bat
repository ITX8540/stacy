@echo off

set PGPASSWORD=stacy
set ENCODING=utf-8
set PGCLIENTENCODING=utf-8
chcp 65001

for /f "delims=?" %%a IN ('dir /on /b /s "patches\*.sql"') do call psql -q --host=localhost --dbname="stacy" --username="stacy" --file="%%a"

pause