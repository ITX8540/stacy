
SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;


CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;



COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';



CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;



COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';



CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;




COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';




CREATE EXTENSION IF NOT EXISTS ltree WITH SCHEMA public;

COMMENT ON EXTENSION ltree IS 'data type for hierarchical tree-like structures';


CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


CREATE EXTENSION IF NOT EXISTS tsearch2 WITH SCHEMA public;


CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


CREATE EXTENSION IF NOT EXISTS "pg_trgm" WITH SCHEMA public;


COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;


--
-- TOC entry 2872 (class 2616 OID 24587)
-- Name: _uuid_ops; Type: OPERATOR CLASS; Schema: public; Owner: postgres
--

CREATE OPERATOR CLASS _uuid_ops
    DEFAULT FOR TYPE uuid[] USING gin AS
    STORAGE uuid ,
    OPERATOR 1 &&(anyarray,anyarray) ,
    OPERATOR 2 @>(anyarray,anyarray) ,
    OPERATOR 3 <@(anyarray,anyarray) ,
    OPERATOR 4 =(anyarray,anyarray) ,
    FUNCTION 1 (uuid[], uuid[]) uuid_cmp(uuid,uuid) ,
    FUNCTION 2 (uuid[], uuid[]) ginarrayextract(anyarray,internal,internal) ,
    FUNCTION 3 (uuid[], uuid[]) ginqueryarrayextract(anyarray,internal,smallint,internal,internal,internal,internal) ,
    FUNCTION 4 (uuid[], uuid[]) ginarrayconsistent(internal,smallint,anyarray,integer,internal,internal,internal,internal);


ALTER OPERATOR CLASS public._uuid_ops USING gin OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;



CREATE TABLE "Configuration" (
    "Key" character varying(100),
    "Value" text
);


ALTER TABLE public."Configuration" OWNER TO stacy;

CREATE TABLE "ClassificationType"(
    "Id" uuid NOT NULL,
    "Name" text,
    "Description" text,
    "ParentId" uuid
);

ALTER TABLE public."ClassificationType" OWNER TO stacy;

ALTER TABLE ONLY "ClassificationType"
    ADD CONSTRAINT "ClassificationType_pkey" PRIMARY KEY ("Id");

ALTER TABLE ONLY "ClassificationType"
    ADD CONSTRAINT "ClassificationType_ParentId_fkey" FOREIGN KEY ("ParentId") REFERENCES "ClassificationType"("Id");

CREATE TABLE "Classification"(
    "Id" uuid NOT NULL,
    "Name" text,
    "Description" text,
    "ClassificationTypeId" uuid NOT NULL,
    "ParentId" uuid
);

ALTER TABLE public."Classification" OWNER TO stacy;

ALTER TABLE ONLY "Classification"
    ADD CONSTRAINT "Classification_pkey" PRIMARY KEY ("Id");

ALTER TABLE ONLY "Classification"
    ADD CONSTRAINT "Classification_ClassificationTypeId_fkey" FOREIGN KEY ("ClassificationTypeId") REFERENCES "ClassificationType"("Id");

ALTER TABLE ONLY "Classification"
    ADD CONSTRAINT "Classification_ParentId_fkey" FOREIGN KEY ("ParentId") REFERENCES "Classification"("Id");


CREATE TABLE "Event" (
    "Id" uuid NOT NULL,
    "EventTypeId" uuid NOT NULL,
    "ParentId" uuid);

ALTER TABLE public."Event" OWNER TO stacy;

ALTER TABLE ONLY "Event"
    ADD CONSTRAINT "Event_pkey" PRIMARY KEY ("Id");

ALTER TABLE ONLY "Event"
    ADD CONSTRAINT "Event_ParentId_fkey" FOREIGN KEY ("ParentId") REFERENCES "Event"("Id");

ALTER TABLE ONLY "Event"
    ADD CONSTRAINT "Event_EventTypeId_fkey" FOREIGN KEY ("EventTypeId") REFERENCES "Classification"("Id");

CREATE TABLE "EventLocation" (
	"Id" uuid NOT NULL,
	"EventId" uuid NOT NULL,
	"Country" text,
	"City" text,
	"FullAddress" text,
	"Place" text,
	"ExtraInfo" hstore);

ALTER TABLE public."EventLocation" OWNER TO stacy;

ALTER TABLE ONLY "EventLocation"
    ADD CONSTRAINT "EventLocation_pkey" PRIMARY KEY ("Id");

ALTER TABLE ONLY "EventLocation"
    ADD CONSTRAINT "EventLocation_EventId_fkey" FOREIGN KEY ("EventId") REFERENCES "Event"("Id");


CREATE TABLE "EventRoom" (
	"Id" uuid NOT NULL,
	"EventId" uuid NOT NULL,
	"Name" text,
	"Description" text,
    "EventRoomTypeId" uuid NOT NULL,
	"ExtraInfo" hstore);

ALTER TABLE public."EventRoom" OWNER TO stacy;

ALTER TABLE ONLY "EventRoom"
    ADD CONSTRAINT "EventRoom_pkey" PRIMARY KEY ("Id");

ALTER TABLE ONLY "EventRoom"
    ADD CONSTRAINT "EventRoom_EventId_fkey" FOREIGN KEY ("EventId") REFERENCES "Event"("Id");

ALTER TABLE ONLY "EventRoom"
    ADD CONSTRAINT "EventRoom_EventRoomTypeId_fkey" FOREIGN KEY ("EventRoomTypeId") REFERENCES "Classification"("Id");


CREATE TABLE "EventContact" (
	"Id" uuid NOT NULL,
	"EventId" uuid NOT NULL,
	"Website" text,
	"Email" text,
	"PhoneNumber" text,
	"ExtraInfo" hstore);

ALTER TABLE public."EventContact" OWNER TO stacy;

ALTER TABLE ONLY "EventContact"
    ADD CONSTRAINT "EventContact_pkey" PRIMARY KEY ("Id");

ALTER TABLE ONLY "EventContact"
    ADD CONSTRAINT "EventContact_EventId_fkey" FOREIGN KEY ("EventId") REFERENCES "Event"("Id");


CREATE TABLE "EventPresenter" (
	"Id" uuid NOT NULL,
	"EventId" uuid NOT NULL,
	"Name" text,
	"Description" text,
	"ExtraInfo" hstore);

ALTER TABLE public."EventPresenter" OWNER TO stacy;

ALTER TABLE ONLY "EventPresenter"
    ADD CONSTRAINT "EventPresenter_pkey" PRIMARY KEY ("Id");

ALTER TABLE ONLY "EventPresenter"
    ADD CONSTRAINT "EventPresenter_EventId_fkey" FOREIGN KEY ("EventId") REFERENCES "Event"("Id");


CREATE TABLE "EventDetails" (
	"Id" uuid NOT NULL,
	"EventId" uuid NOT NULL,
	"Name" text,
	"Description" text,
	"Summary" text,
	"EventRoomId" uuid,
	"PresenterId" uuid,
	"StartsAt" timestamp with time zone,
	"EndsAt" timestamp with time zone,
	"Tags" text[],
	"ExtraInfo" hstore);

ALTER TABLE public."EventDetails" OWNER TO stacy;

ALTER TABLE ONLY "EventDetails"
    ADD CONSTRAINT "EventDetails_pkey" PRIMARY KEY ("Id");

ALTER TABLE ONLY "EventDetails"
    ADD CONSTRAINT "EventDetails_EventId_fkey" FOREIGN KEY ("EventId") REFERENCES "Event"("Id");

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM stacy;
GRANT ALL ON SCHEMA public TO stacy;
GRANT ALL ON SCHEMA public TO PUBLIC;

INSERT INTO "Configuration" ("Key", "Value") VALUES ('DatabaseSchemaVersion', '2016.09.27.01');