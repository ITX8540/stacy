--SQL for DB stacy (almost relational data model)

DROP TABLE conference;
DROP TABLE conference_location;
DROP TABLE conference_room;
DROP TABLE conference_contact;
DROP TABLE conference_presenter;
DROP TABLE conference_event;

--it's example dataset for initial prototype
--foreign keys and types/classifiers will be added later when this data model will taken in use

CREATE TABLE conference (
	id serial,
	name text,
	description text,
	startsAt date,
	endsAt date,
	extra_info hstore);

INSERT INTO conference
	(id, name, description, startsAt, endsAt)
VALUES
	(1, 'Topconf 2016', 'IT conference in Baltics.', '2016-11-16', '2016-11-17');


CREATE TABLE conference_location (
	id serial,
	conference_id int,
	country text,
	city text,
	long_address text,
	place text,
	extra_info hstore);

INSERT INTO conference_location
	(id, conference_id, country, city, long_address, place)
VALUES
	(1, 1, 'Estonia', 'Tallinn', 'Viru väljak 4, 10111 Tallinn, Estonia', 'Sokos Hotel Viru');


CREATE TABLE conference_room (
	id serial,
	conference_id int,
	name text,
	description text,
	extra_info hstore);

INSERT INTO conference_room
	(id, conference_id, name, description)
VALUES
	(1, 1, 'Grande 1', 'Bigger room'),
	(2, 1, 'Bolero 1', 'Smaller room');


CREATE TABLE conference_contact (
	id serial,
	conference_id int,
	website text,
	mail text,
	phone text,
	extra_info hstore);

INSERT INTO conference_contact
	(id, conference_id, website, mail, phone, extra_info)
VALUES
	(1, 1, 'http://topconf.com/tallinn-2016/', '', '', '"twitter" => "@TopconfEE", "hashtag" => "#TopconfEE"');


CREATE TABLE conference_presenter (
	id serial,
	conference_id int,
	name text,
	description text,
	extra_info hstore);

INSERT INTO conference_presenter
	(id, conference_id, name, description, extra_info)
VALUES
	(1, 1, 'Arie van Bennekum', 'Co-author of the Agile Manifesto', ''),
	(2, 1, 'Gleb Smirnov', 'Senior Software Engineer - Plumbr', ''),
	(3, 1, 'Juergen Hoeller', 'Principal Engineer - Pivotal', ''),
	(4, 1, 'Alex Canessa', 'Senior Full Stack Developer at Digital Detox', '"website" => "www.detox.com/alex"'),
	(5, 1, 'Holger Bartel', 'Front end developer', '');


CREATE TABLE conference_event (
	id serial,
	conference_id int,
	title text,
	summary text,
	type text,
	room int,
	presenter int,
	startsAt timestamp,
	endsAt timestamp,
	tags text[],
	extra_info hstore);

INSERT INTO conference_event
	(id, conference_id, title, summary,
		type, room, presenter, startsAt, endsAt,
		tags, extra_info)
VALUES
	(1, 1, 'Opening Keynote: True Agile is what you are', 'We can see Agile literally on every continent. It is not a hype anymore, it has matured.',
		'keynote', 1, 1, '2016-11-16 09:20:00', '2016-11-16 10:00:00',
		'{JVM, HotSpot, source code, GC, JLS}', ''),
	(2, 1, 'The Mysteries Lie In Our Heads, Not In The JVM', 'Every once in a while each engineer faces a problem novel to them. Oftentimes, a quick internet search yields a solution.',
		'workshop', 1, 2, '2016-11-16 10:20:00', '2016-11-16 11:00:00',
		'{JVM, HotSpot, source code, GC, JLS}', '"requirements" => "bring laptop with you"'),
	(3, 1, 'Modern Java Component Design with Spring 4.3', 'Springs programming and configuration model has a strong design philosophy with respect to application components and configuration artifacts.',
		'lecture', 1, 3, '2016-11-16 12:00:00', '2016-11-16 12:40:00',
		'{Spring, Java}', ''),
	(4, 1, 'MyoJS – From Gesture to Code', 'The Myo armband measures the electrical activity from your muscles to detect what gesture your hand is making.',
		'lecture', 2, 4, '2016-11-16 10:20:00', '2016-11-16 12:00:00',
		'{history of the gesture control, myo: the armband technology, web}', ''),
	(5, 1, 'Web Performance in the Age of HTTP2', 'Web performance optimisation has been gaining ground and is slowly getting more of its deserved recognition.',
		'lecture', 2, 5, '2016-11-16 12:00:00', '2016-11-16 12:40:00',
		'{web performance, optimisation, best practices, front end}', '');

