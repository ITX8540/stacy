@echo off

SET /P ANSWER=Do you want to drop database "stacy" and recreate it from scratch (Y/N)?
if /i {%ANSWER%}=={y} (goto :yes)
if /i {%ANSWER%}=={yes} (goto :yes)
goto :no
:yes

set PGPASSWORD=stacy
set ENCODING=utf-8
set PGCLIENTENCODING=utf-8
chcp 65001

echo Dropping database stacy

psql -q --host=localhost --dbname="postgres" --username="stacy" -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'stacy' AND pid <> pg_backend_pid();"
psql -q --host=localhost --dbname="postgres" --username="stacy" -c "DROP DATABASE stacy;" 
psql -q --host=localhost --dbname="postgres" --username="stacy" -c "CREATE DATABASE stacy WITH OWNER = stacy ENCODING = 'UTF8' TABLESPACE = pg_default  LC_COLLATE = 'Estonian_Estonia.1257'  LC_CTYPE = 'Estonian_Estonia.1257'   CONNECTION LIMIT = -1;" 
psql -q --host=localhost --dbname="stacy" --username="stacy" --file "init.sql" 


for /f "delims=?" %%a IN ('dir /on /b /s "patches\*.sql"') do call psql -q --host=localhost --dbname="stacy" --username="stacy" --file="%%a"

pause

:no
echo Exiting!
exit /b 1 
