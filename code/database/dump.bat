set PGPASSWORD=stacy
"pg_dump.exe" --verbose --host localhost --port 5432 --username "stacy" --role "stacy" --no-password  --format plain --schema-only --encoding UTF8 --file "stacy_new.sql" "stacy"

java -jar apgdiff-2.4.jar stacy.sql stacy_new.sql > stacy_diff.sql
@echo off

move stacy_new.sql stacy.sql
pause