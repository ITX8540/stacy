<?php
session_start();
$_SESSION['id'] = $_GET['id'];
$db = pg_connect("host=localhost port=5432 dbname=vhost52321p0 user=vhost52321p0 password=As6WH6X"); 
$result = pg_query($db, "SELECT * FROM conference where id=".$_GET['id']);
$row = pg_fetch_all($result);
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="http://stacy.neway.ee/img/shareimage.jpg">
    <meta property="og:url" content="http://stacy.neway.ee">
    <meta property="og:title" content="Stacy - Virtual assistant for conferences">
    <meta property="og:site_name" content="Stacy - Virtual assistant for conferences">
    <meta property="og:type" content="website">

    <title>Stacy</title>

    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/owl.carousel.css">
    <link rel="stylesheet" href="/css/owl.theme.css">
    <link rel="stylesheet" href="/css/nivo-lightbox/nivo-lightbox.css">
    <link rel="stylesheet" href="/css/nivo-lightbox/nivo-lightbox-theme.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/style.css">


    <script src="/js/modernizr.custom.js"></script>

</head>

<body>

    <a href="#header" id="back-to-top" class="top"><i class="fa fa-chevron-up"></i></a>

    <section id="header" class="">
        <div class="col-md-11 col-xs-10">
            <a href="/"><span id="logo"><img src="/img/logo.svg"></span></a>
        </div>
                        
    </section>
    <section>
    	<div class="container">
    		
    		<div class="events">
    			<?php 
                    
    				foreach ($row as $key => $value) {
				?>
					<h1><?php echo $value['name']; ?></h1>
                    <a href="/change/contact">Muuda kontakt infot</a>
                    <a href="/change/event">Muuda ürituse infot</a>
                    <a href="/change/location">Muuda asukoha infot</a>
                    <a href="/change/presenter">Muuda esinejate infot</a>
                    <a href="/change/room">Muuda ruumi infot</a>
    			<?php
    				}
    			?>
    		</div>
    	</div>
    </section>

<div class=""></div>
</body>
</html>