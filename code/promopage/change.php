<?php
session_start();
$table = $_GET['change_temp'];
$db = pg_connect("host=localhost port=5432 dbname=vhost52321p0 user=vhost52321p0 password=As6WH6X"); 

?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="http://stacy.neway.ee/img/shareimage.jpg">
    <meta property="og:url" content="http://stacy.neway.ee">
    <meta property="og:title" content="Stacy - Virtual assistant for conferences">
    <meta property="og:site_name" content="Stacy - Virtual assistant for conferences">
    <meta property="og:type" content="website">

    <title>Stacy</title>

    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/owl.carousel.css">
    <link rel="stylesheet" href="/css/owl.theme.css">
    <link rel="stylesheet" href="/css/nivo-lightbox/nivo-lightbox.css">
    <link rel="stylesheet" href="/css/nivo-lightbox/nivo-lightbox-theme.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/style.css">

    <script src="/js/jquery-1.11.2.min.js"></script>
    <script src="/js/modernizr.custom.js"></script>

</head>

<body>

    <a href="#header" id="back-to-top" class="top"><i class="fa fa-chevron-up"></i></a>

    <section id="header" class="">
        <div class="col-md-11 col-xs-10">
            <a href="/"><span id="logo"><img src="/img/logo.svg"></span></a>
        </div>
                        
    </section>
    <?php if ($table === "contact"):
        $result = pg_query($db, "SELECT * FROM conference_".$table.",conference where conference.id=".$_SESSION['id']." and conference_id=".$_SESSION['id']);
        $row = pg_fetch_all($result);
        $result2 = pg_query($db, "SELECT extra_info as extra FROM conference_".$table." where conference_id=".$_SESSION['id']);
        $row2= pg_fetch_all($result2);
        $extra = $row2[0]['extra'];
        $items = json_decode($extra);
    ?>
    <section>
        <div class="container">
            
            <div class="events">
                <?php 
                    foreach ($row as $key => $value) {
                ?>
                    <div class="form">
                    <h1><?php echo $value['name']; ?></h1>
                    <form class="eventc">
                    <input type="hidden" name="id" value="<?php echo $value['id'];?>">
                    <div class="form">
                        <div class="form_item">
                            <label for="phone">Phone</label>
                            <input type="text" id="phone" name="phone" value="<?php echo $value['phone']; ?>">
                        </div>
                        <div class="form_item">
                            <label for="mail">E-mail</label>
                            <input type="text" id="mail" name="mail" value="<?php echo $value['mail']; ?>">
                        </div>
                        <div class="form_item">
                            <label for="website">Website</label>
                            <input type="text" id="website" name="website" value="<?php echo $value['website']; ?>">
                        </div>
                        <?php /*foreach ($items as $val => $vals) {
                           ?>
                            <div class="form_item">
                                <input type="text" name="extra[<?php echo $val?>][label]" id="<?php echo $val?>" value="<?php echo $val?>">
                                <input type="text" name="extra[<?php echo $val?>][value]" id="<?php echo $val?>" value="<?php echo $vals; ?>">
                            </div>
                           <?php
                        }*/
                        ?>
                    </div>
                    <div class="form_submit">
                            <button class="submit" data-id="<?php echo $key;?>">Update</button>
                        </div>
                    </form>
                    </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>

     <script type="text/javascript">
        $('.form_submit button').click(function(e){
            var params = {
                    type: 'post',
                    url: '/service/contact'
                };
            params.data = new FormData($(".eventc").get(0))
            params.cache = false;
            params.contentType = false;
            params.processData = false;
            $.ajax(params).done(function(data){
                console.log(data)
            });
            e.preventDefault();
            return false;
        })
    </script>
    <?php endif;?>
    <?php if ($table === "event"):
        $result = pg_query($db, "SELECT * FROM conference_".$table.",conference where conference.id=".$_SESSION['id']." and conference_id=".$_SESSION['id']);
        $row = pg_fetch_all($result);
        
        $result2 = pg_query($db, "SELECT extra_info as extra FROM conference_".$table." where conference_id=".$_SESSION['id']);
        $row2= pg_fetch_all($result2);
        $result3 = pg_query($db, "SELECT array_to_json(tags) AS tags FROM conference_".$table." where conference_id=".$_SESSION['id']);
        $row3= pg_fetch_all($result3);
        $extra = $row2;
        $tags = $row3;

    ?>
    <section>
        <div class="container">
            
            <div class="events">
                    <h1><?php echo $row[0]['name']; ?></h1>
                <?php 
                    foreach ($row as $key => $value) {
                        $tagitem = json_decode($tags[$key]['tags']);
                        $items = json_decode($extra[$key]['extra']);
                ?>
                    <div class="form">
                        <form  class="form<?php echo $key;?>" method="POST">
                        <input type="hidden" name="id" value="<?php echo $value['id'];?>">
                        <div class="form_item">
                            <label for="title<?php echo $key;?>">Title</label>
                            <input type="text" id="title<?php echo $key;?>" name="item[<?php echo $value['id'];?>][title]" value="<?php echo $value['title']; ?>">
                        </div>
                        <div class="form_item">
                            <label for="summary<?php echo $key;?>">Summary</label>
                            <textarea type="text" id="summary<?php echo $key;?>" name="item[<?php echo $value['id'];?>][summary]" value=""><?php echo $value['summary']; ?></textarea>
                        </div>
                        <div class="form_item">
                            <label for="type<?php echo $key;?>">Type</label>
                            <input type="text" id="type<?php echo $key;?>" name="item[<?php echo $value['id'];?>][type]" value="<?php echo $value['type']; ?>">
                        </div>
                        <div class="form_item">
                            <label for="room<?php echo $key;?>">Room</label>
                            <input type="text" id="room<?php echo $key;?>" name="item[<?php echo $value['id'];?>][room]" value="<?php echo $value['room']; ?>">
                        </div>
                        <div class="form_item">
                            <label for="presenter<?php echo $key;?>">Presenter</label>
                            <input type="text" id="presenter<?php echo $key;?>" name="item[<?php echo $value['id'];?>][presenter]" value="<?php echo $value['presenter']; ?>">
                        </div>
                        <div class="form_item">
                            <label for="startsat<?php echo $key;?>">Start at</label>
                            <input type="text" id="startsat<?php echo $key;?>" name="item[<?php echo $value['id'];?>][startsat]" value="<?php echo $value['startsat']; ?>">
                        </div>
                        <div class="form_item">
                            <label for="endsat<?php echo $key;?>">Ends at</label>
                            <input type="text" id="endsat<?php echo $key;?>" name="item[<?php echo $value['id'];?>][endsat]" value="<?php echo $value['endsat']; ?>">
                        </div>
                        <div class="tags">
                        <span class="tags_title"><b>Tags</b></span>
                        <?php foreach ($tagitem as $tag_key => $tag) {
                           ?>
                            <div class="form_item">
                                <input type="text" name="item[<?php echo $value['id'];?>][tags][<?php echo $tag_key?>]" id="tag<?php echo $tag_key?>" value="<?php echo $tag; ?>">
                            </div>
                           <?php
                        }
                        ?>
                        </div>
                        <span class="tags_title"><b>Extra info</b></span>
                        <?php foreach ($items as $val => $vals) {
                           ?>
                            <div class="form_item extra">
                               <label for="<?php echo $val?>"><?php echo $val?></label>
                                <input type="text" name="item[<?php echo $value['id'];?>][extra][<?php echo $val?>][label]" id="<?php echo $val?>" value="<?php echo $val; ?>">
                                <input type="text" name="item[<?php echo $value['id'];?>][extra][<?php echo $val?>][val]" id="<?php echo $val?>" value="<?php echo $vals; ?>">
                            </div>
                           <?php
                        }
                        ?>
                        <div class="form_submit">
                            <button class="submit" data-id="<?php echo $key;?>">Update</button>
                        </div>
                        </form>
                    </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $('.form_submit button').click(function(e){
            var params = {
                    type: 'post',
                    url: '/service/event'
                };
            params.data = new FormData($(".form"+$(this).data('id')).get(0))
            params.cache = false;
            params.contentType = false;
            params.processData = false;
            $.ajax(params).done(function(data){
                console.log(data)
            });
            e.preventDefault();
            return false;
        })
    </script>
    <?php endif;?>
    <?php if ($table === "location"):
        $result = pg_query($db, "SELECT * FROM conference_".$table.",conference where conference.id=".$_SESSION['id']." and conference_id=".$_SESSION['id']);
        $row = pg_fetch_all($result);
        
        $result2 = pg_query($db, "SELECT extra_info as extra FROM conference_".$table." where conference_id=".$_SESSION['id']);
        $row2= pg_fetch_all($result2);        
        $extra = $row2;

    ?>
    <section>
    	<div class="container">
    		
    		<div class="events">
					<h1><?php echo $row[0]['name']; ?></h1>
                <?php 
                    foreach ($row as $key => $value) {
                        $tagitem = json_decode($tags[$key]['tags']);
                        $items = json_decode($extra[$key]['extra']);
                ?>
                    <div class="form">
                        <form  class="form<?php echo $key;?>" method="POST">
                        <input type="hidden" name="id" value="<?php echo $value['id'];?>">
                        <div class="form_item">
                            <label for="title<?php echo $key;?>">Country</label>
                            <input type="text" id="title<?php echo $key;?>" name="country" value="<?php echo $value['country']; ?>">
                        </div>
                        <div class="form_item">
                            <label for="summary<?php echo $key;?>">City</label>
                            <textarea type="text" id="summary<?php echo $key;?>" name="city" value=""><?php echo $value['city']; ?></textarea>
                        </div>
                        <div class="form_item">
                            <label for="type<?php echo $key;?>">Address</label>
                            <input type="text" id="type<?php echo $key;?>" name="long_address" value="<?php echo $value['long_address']; ?>">
                        </div>
                        <div class="form_item">
                            <label for="room<?php echo $key;?>">Place</label>
                            <input type="text" id="room<?php echo $key;?>" name="place" value="<?php echo $value['place']; ?>">
                        </div>
                        
                        <div class="form_submit">
                            <button class="submit" data-id="<?php echo $key;?>">Update</button>
                        </div>
                        </form>
                    </div>
    			<?php
    				}
    			?>
    		</div>
    	</div>
    </section>
    <script type="text/javascript">
        $('.form_submit button').click(function(e){
            var params = {
                    type: 'post',
                    url: '/service/location'
                };
            params.data = new FormData($(".form"+$(this).data('id')).get(0))
            params.cache = false;
            params.contentType = false;
            params.processData = false;
            $.ajax(params).done(function(data){
                console.log(data)
            });
            e.preventDefault();
            return false;
        })
    </script>
    <?php endif;?>
    <?php if ($table === "presenter"):
        $result = pg_query($db, "SELECT * FROM conference_".$table.",conference where conference.id=".$_SESSION['id']." and conference_id=".$_SESSION['id']);
        $row = pg_fetch_all($result);
        
        $result2 = pg_query($db, "SELECT extra_info as extra FROM conference_".$table." where conference_id=".$_SESSION['id']);
        $row2= pg_fetch_all($result2);
        
        $extra = $row2;

    ?>
    <section>
        <div class="container">
            
            <div class="events">
                    <h1><?php echo $row[0]['name']; ?></h1>
                <?php 
                    foreach ($row as $key => $value) {
                        $tagitem = json_decode($tags[$key]['tags']);
                        $items = json_decode($extra[$key]['extra']);
                ?>
                    <div class="form">
                        <form  class="form<?php echo $key;?>" method="POST">
                        <input type="hidden" name="id" value="<?php echo $value['id'];?>">
                        <div class="form_item">
                            <label for="title<?php echo $key;?>">Name</label>
                            <input type="text" id="title<?php echo $key;?>" name="item[<?php echo $value['id'];?>][name]" value="<?php echo $value['name']; ?>">
                        </div>
                        <div class="form_item">
                            <label for="summary<?php echo $key;?>">Description</label>
                            <textarea type="text" id="summary<?php echo $key;?>" name="item[<?php echo $value['id'];?>][description]" value=""><?php echo $value['description']; ?></textarea>
                        </div>
                        
                       
                        <div class="form_submit">
                            <button class="submit" data-id="<?php echo $key;?>">Update</button>
                        </div>
                        </form>
                    </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $('.form_submit button').click(function(e){
            var params = {
                    type: 'post',
                    url: '/service/presenter'
                };
            params.data = new FormData($(".form"+$(this).data('id')).get(0))
            params.cache = false;
            params.contentType = false;
            params.processData = false;
            $.ajax(params).done(function(data){
                console.log(data)
            });
            e.preventDefault();
            return false;
        })
    </script>
    <?php endif;?>
 <?php if ($table === "room"):
        $result = pg_query($db, "SELECT conference_".$table.".name as title , conference_".$table.".description, conference.name FROM conference_".$table.",conference where conference.id=".$_SESSION['id']." and   conference_id=".$_SESSION['id']);
        $row = pg_fetch_all($result);
        
        $result2 = pg_query($db, "SELECT extra_info as extra FROM conference_".$table." where conference_id=".$_SESSION['id']);
        $row2= pg_fetch_all($result2);

        $extra = $row2;

    ?>
    <section>
        <div class="container">
            
            <div class="events">
                    <h1><?php echo $row[0]['name']; ?></h1>
                <?php 
                    foreach ($row as $key => $value) {
                        $tagitem = json_decode($tags[$key]['tags']);
                        $items = json_decode($extra[$key]['extra']);
                ?>
                    <div class="form">
                        <form  class="form<?php echo $key;?>" method="POST">
                        <input type="hidden" name="id" value="<?php echo $value['id'];?>">
                        <div class="form_item">
                            <label for="title<?php echo $key;?>">Name</label>
                            <input type="text" id="title<?php echo $key;?>" name="item[<?php echo $value['id'];?>][name]" value="<?php echo $value['title']; ?>">
                        </div>
                        <div class="form_item">
                            <label for="summary<?php echo $key;?>">Description</label>
                            <textarea type="text" id="summary<?php echo $key;?>" name="item[<?php echo $value['id'];?>][description]" value=""><?php echo $value['description']; ?></textarea>
                        </div>
                        
                       
                        <div class="form_submit">
                            <button class="submit" data-id="<?php echo $key;?>">Update</button>
                        </div>
                        </form>
                    </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $('.form_submit button').click(function(e){
            var params = {
                    type: 'post',
                    url: '/service/room'
                };
            params.data = new FormData($(".form"+$(this).data('id')).get(0))
            params.cache = false;
            params.contentType = false;
            params.processData = false;
            $.ajax(params).done(function(data){
                console.log(data)
            });
            e.preventDefault();
            return false;
        })
    </script>
    <?php endif;?>

<div class=""></div>
<script type="text/javascript">
equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(function() {
  equalheight('.form');
});


$(window).resize(function(){
  equalheight('.form');
});

</script>
</body>
</html>