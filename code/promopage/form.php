<?php 

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = strip_tags(trim($_POST["name"]));
        $name = str_replace(array("\r","\n"),array(" "," "),$name);
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
        $phone = strip_tags(trim($_POST["phone"]));
        $event = strip_tags(trim($_POST["event"]));
        
        $message = trim($_POST["extra"]);

        if ( empty($name) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            http_response_code(400);
            echo "Oops! There was a problem with your submission. Please complete the form and try again.";
            exit;
        }

        $recipient = "info@stacy.ee";

        $subject = "Subscribe Stacy.ee";

        $email_content = "Name: $name\n";
        $email_content .= "Email: $email\n";
        if($phone != ""){
            $email_content .= "Phone: $phone\n";
            
        }
        if($event != ""){
            $email_content .= "Event: $event\n";
            
        }
        $email_content .= "\Extra information:\n$message\n";

        $email_headers = "From: $name <$email>\n\r";

        if (mail($recipient, $subject, $email_content, $email_headers)) {
            $servername = "localhost";
            $username = "vhost52321s0";
            $password = "SImh3MOX6w";
            $dbname = "vhost52321s0";
            try {
                $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $sql = "INSERT INTO `vhost52321s0`.`subscribes` (`name`, `email`, `phone`, `event`, `extra`) VALUES ('$name', '$email', '$phone', '$event', '$message');";
                $conn->exec($sql);
                }
            catch(PDOException $e)
                {
                echo $sql . "<br>" . $e->getMessage();
                }

            $conn = null;
            http_response_code(200);
            echo "Thank You! Your message has been sent.";
        } else {
            http_response_code(500);
            echo "Oops! Something went wrong and we couldn't send your message.";
        }
        $recipient = "$email";

        $subject = "Subscribe Stacy.ee";

        $email_content = "Hi $name\n\nThank you for being interested in making the conference experience awesome.\n\nBest regards\nTeam Stacy\ninfo@stacy.ee";

        $email_headers = "From: Stacy <info@stacy.ee>\n\r";

        if (mail($recipient, $subject, $email_content, $email_headers)) {

        }

    } else {
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }

?>
