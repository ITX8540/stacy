﻿namespace Stacy.LoadTests
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var benchmarkRunner = new StacyBenchmarkRunner();
            benchmarkRunner.Run();
        }
    }
}