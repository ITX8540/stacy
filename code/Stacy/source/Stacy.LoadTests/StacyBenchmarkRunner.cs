﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BenchmarkDotNet.Running;
using MediatR;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using Stacy.Domain.Commands;
using Stacy.Infrastructure.Handlers.Requests;
using Stacy.LoadTests.Benchmarks;
using Stacy.Webhooks.Infrastructure;

namespace Stacy.LoadTests
{
    public class StacyBenchmarkRunner
    {
        // TODO: Find a way to DI into BenchmarkRunner.Run.
        public static Container GlobalContainer { get; private set; }

        static StacyBenchmarkRunner()
        {
            Configure();
        }        

        public void Run()
        {
            var benchmarks = new[]
            {
                //typeof(HelloWorldBenchmark),
                typeof(FbWebhookControllerBenchmark),
                //typeof(ApiAiWebhookControllerBenchmark)
            };

            foreach (var benchmark in benchmarks)
            {
                var summary = BenchmarkRunner.Run(benchmark);                
            }
        }

        private static void Configure()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();

            var assemblies = GetAssemblies().ToArray();
            container.RegisterSingleton<IMediator, Mediator>();
            container.Register(typeof(IRequestHandler<,>), assemblies);
            container.Register(typeof(IAsyncRequestHandler<,>), assemblies);
            container.RegisterCollection(typeof(INotificationHandler<>), assemblies);
            container.RegisterCollection(typeof(INotificationHandler<>), assemblies);
            container.RegisterSingleton(new SingleInstanceFactory(container.GetInstance));
            container.RegisterSingleton(new MultiInstanceFactory(container.GetAllInstances));
            container.RegisterSingleton<IMemoryCacher, MemoryCacher>();

            GlobalContainer = container;
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            yield return typeof(IMediator).GetTypeInfo().Assembly;
            yield return typeof(GetGoogleMapsLinkMessage).GetTypeInfo().Assembly;
            yield return typeof(GoogleMapsRequestHandler).GetTypeInfo().Assembly;
        }
    }
}
