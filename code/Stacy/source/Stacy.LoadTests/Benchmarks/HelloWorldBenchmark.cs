﻿using System.Threading;
using BenchmarkDotNet.Attributes;

namespace Stacy.LoadTests.Benchmarks
{
    public class HelloWorldBenchmark : BenchmarkBase
    {
        [Benchmark]
        public void Run()
        {
            Thread.Sleep(10);
        }
    }
}
