﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Columns;
using BenchmarkDotNet.Attributes.Exporters;
using MediatR;
using Stacy.Core.DTOs.Facebook;
using Stacy.Webhooks.Controllers;
using Stacy.Webhooks.Infrastructure;

namespace Stacy.LoadTests.Benchmarks
{
    [MinColumn, MaxColumn]
    [MarkdownExporter]
    public class FbWebhookControllerBenchmark : BenchmarkBase
    {
        private const int NumParallel = 5;
        private readonly FbWebhookController _controller;

        public FbWebhookControllerBenchmark()
        {
            _controller = new FbWebhookController(
                Container.GetInstance<IMediator>(),
                Container.GetInstance<IMemoryCacher>())
            {
                Request = new HttpRequestMessage
                {
                    Content = new StringContent("", Encoding.UTF8, "application/json")              
                }
            };
        }


        // Smalltalk.
        [Benchmark]
        public Task SayHello() => RunParallel(
            () => _controller.Post(PrepareDefaultCallback(msg: "hello")), NumParallel);

        // TODO: Commented out because these tests failed on JWIN due to exception. 
        // TODO: Exception source unknown. Need to check log.
        //// Google Map.
        //[Benchmark]
        //public Task AskEventLocation() => RunParallel(
        //    () => _controller.Post(PrepareDefaultCallback(msg: "where is the event located?")), NumParallel);

        //// Event.
        //[Benchmark]
        //public Task AskEvent() => RunParallel(
        //    () => _controller.Post(PrepareDefaultCallback(msg: "what event?")), NumParallel);


        private static FbCallback PrepareDefaultCallback(string msg)
        {
            return new FbCallback
            {
                Object = "page",
                Entry = new[]
                {
                    new FbEntry
                    {
                        Id = "395504737504160",
                        Time = 1480937007975,
                        Messaging = new[]
                        {
                            new FbMessaging
                            {
                                Timestamp = 1480937007949,
                                Sender = new FbSender
                                {
                                    Id = "1252500021480732"
                                },
                                Recipient = new FbRecipient
                                {
                                    Id = "395504737504160"
                                },
                                Message = new FbMessage
                                {
                                    MessageId = "mid.1480937007949:d76eef1f30",
                                    Seq = 151,
                                    Text = msg
                                }
                            }
                        }
                    }
                }
            };
        }
    }
}
