﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Results;
using BenchmarkDotNet.Attributes;
using Stacy.Core.DTOs.ApiAi;
using Stacy.Webhooks.Controllers;

namespace Stacy.LoadTests.Benchmarks
{
    public class ApiAiWebhookControllerBenchmark : BenchmarkBase
    {
        private readonly ApiAiWebhookController _controller = new ApiAiWebhookController();


        [Benchmark]
        public Task<JsonResult<ApiAiResponse>> Post() => _controller.Post(PrepareDefaultRequest());


        private static ApiAiRequest PrepareDefaultRequest()
        {
            var request = new ApiAiRequest
            {
                Id = Guid.NewGuid(),
                SessionId = Guid.NewGuid(),
                Timestamp = DateTime.Today,
                Status = new ApiAiStatus { Code = 0 },
                Result = new ApiAiResult
                {
                    ResolvedQuery = "1",
                    Action = "",
                    Parameters = new Dictionary<string, string>(),
                    Contexts = new ApiAiContext[0],
                    Metadata = new ApiAiMetadata
                    {
                        IntentId = Guid.NewGuid(),
                        WebhookUsed = "true",
                        IntentName = "DefaultFallbackIntent"
                    },
                    Fulfillment = new ApiAiFulfillment
                    {
                        Speech = "Sorry, I'm afraid I don't follow you."                        
                    },
                    Score = 1
                }
            };
            return request;
        }  

    }
}
