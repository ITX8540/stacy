﻿using SimpleInjector;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Stacy.LoadTests.Benchmarks
{
    public abstract class BenchmarkBase
    {
        protected BenchmarkBase()
        {
            Container = StacyBenchmarkRunner.GlobalContainer;
        }

        protected Container Container { get; }

        protected async Task RunParallel(Func<Task> action, int numParallel = 1)
        {
            await Task.WhenAll(Enumerable.Range(0, numParallel).Select(_ => action()));
        }
    }
}
