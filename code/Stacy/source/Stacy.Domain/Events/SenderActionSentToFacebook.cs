﻿using System;
using MediatR;
using Stacy.Core.Events;

namespace Stacy.Domain.Events
{
    public class SenderActionSentToFacebook : INotification, IEvent
    {
        public readonly Guid RequestId;

        public SenderActionSentToFacebook(Guid requestId)
        {
            Id = Guid.NewGuid();
            Version = 0;
            TimeStamp = DateTimeOffset.Now;
            RequestId = requestId;
        }

        public Guid Id { get; set; }
        public int Version { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
    }
}