﻿using System;
using MediatR;
using Stacy.Core.DTOs.Facebook;
using Stacy.Core.Events;

namespace Stacy.Domain.Events
{
    public class FacebookCallbackReceived : INotification, IEvent
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
        public DateTimeOffset TimeStamp { get; set; }

        public Guid RequestId { get;}

        public FbCallback Callback { get; }

        public FacebookCallbackReceived(Guid requestId, FbCallback callback, int version, DateTimeOffset timestamp)
        {
            Id = Guid.NewGuid();
            RequestId = requestId;
            Callback = callback;
            Version = version;
            TimeStamp = timestamp;
        }
    }
}
