﻿using System;
using Stacy.Core.Events;

namespace Stacy.Domain.Events
{
    public class MessageReceived : IEvent
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
        public DateTimeOffset TimeStamp { get; set; }

        public string Sender { get; set; }

        public string MessageContent { get; set; }

        public string ActionResolved { get; set; }

        public MessageReceived(Guid id, DateTimeOffset timeStamp, string sender, string message, string action)
        {
            Id = id;
            TimeStamp = timeStamp;
            Sender = sender;
            MessageContent = message;
            ActionResolved = action;
        }
    }
}
