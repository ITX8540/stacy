﻿using System;
using ApiAiSDK.Model;
using MediatR;
using Stacy.Core.DTOs.Facebook;
using Stacy.Core.Events;

namespace Stacy.Domain.Events
{
    public class MessageProcessed : INotification, IEvent
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
        public DateTimeOffset TimeStamp { get; set; }

        public string ApiSessionId { get; set; }

        public string UserId { get; set; }

        public AIResponse AiResponse { get; set; }
        public FbMessaging IncomingMessage { get; set; }

        public Guid RequestId { get; set; }
        public MessageProcessed(Guid requestId, string userId, AIResponse aiResponse, FbMessaging messaging, DateTimeOffset timeStamp)
        {
            Id = Guid.NewGuid();
            Version = 0;
            RequestId = requestId;
            ApiSessionId = aiResponse != null ? aiResponse.SessionId : string.Empty;
            UserId = userId;
            AiResponse = aiResponse;
            IncomingMessage = messaging;
        }

    }
}