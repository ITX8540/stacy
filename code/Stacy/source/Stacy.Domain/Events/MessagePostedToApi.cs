﻿using System;
using System.Net.Http;
using MediatR;
using Stacy.Core.Events;

namespace Stacy.Domain.Events
{
    public class MessagePostedToApi<T> : INotification, IEvent where T : class
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
        public DateTimeOffset TimeStamp { get; set; }

        public T MessagePayload { get; set; }

        public HttpResponseMessage HttpResponse { get; set; }

        public MessagePostedToApi(T messagePayload, HttpResponseMessage response, DateTimeOffset timeStamp)
        {
            MessagePayload = messagePayload;
            HttpResponse = response;
            TimeStamp = timeStamp;
            Version = 0;
            Id = Guid.NewGuid();
        }
    }
}
