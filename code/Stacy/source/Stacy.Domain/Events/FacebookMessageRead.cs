﻿using System;
using MediatR;
using Stacy.Core.Events;

namespace Stacy.Domain.Events
{
    public class FacebookMessageRead : INotification, IEvent
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        public string UserId { get; set; }
        public long? FacebookMessageWatermark { get; set; }

        public FacebookMessageRead(string userId, long? waterMark)
        {
            Id = Guid.NewGuid();
            UserId = userId;
            FacebookMessageWatermark = waterMark;
            TimeStamp = DateTimeOffset.Now; ;
            Version = 0;
        }
    }
}