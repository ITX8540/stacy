﻿using System;
using MediatR;
using Stacy.Core.Events;

namespace Stacy.Domain.Events
{
    public class FacebookMessageDelivered : INotification, IEvent
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        public string UserId { get; set; }
        public string[] MessageIds { get; set; }
        public long? FacebookMessageWatermark { get; set; }

        public FacebookMessageDelivered(string userId, string[] messageIds, long? waterMark)
        {
            Id = Guid.NewGuid();
            UserId = userId;
            MessageIds = messageIds;
            FacebookMessageWatermark = waterMark;
            TimeStamp = DateTimeOffset.Now;
            Version = 0;
        }
    }
}