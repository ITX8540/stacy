﻿using System;
using MediatR;
using Stacy.Core.Events;

namespace Stacy.Domain.Events
{
    public class TypingOnActionSent : INotification, IEvent
    {
        public Guid Id { get; set; }

        public int Version { get; set; }

        public DateTimeOffset TimeStamp { get; set; }

        public readonly Guid RequestId;

        public readonly string ReceipientId;

        public TypingOnActionSent(Guid requestId, string receipientId)
        {
            Id = Guid.NewGuid();
            Version = 0;
            RequestId = requestId;
            ReceipientId = receipientId;
            TimeStamp = DateTimeOffset.Now;
        }

        
    }
}