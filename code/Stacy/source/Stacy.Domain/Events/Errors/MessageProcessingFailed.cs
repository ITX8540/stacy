﻿using System;
using MediatR;
using Stacy.Core.Events;

namespace Stacy.Domain.Events.Errors
{
    public class MessageProcessingFailed : INotification, IEvent
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
        public DateTimeOffset TimeStamp { get; set; }

        public readonly Guid RequestId;

        public readonly string ReceipientId;

        public string Error { get; set; }

        public MessageProcessingFailed(Guid requestId, string receipientId, string error)
        {
            RequestId = requestId;
            ReceipientId = receipientId;
            Error = error;
            Id = Guid.NewGuid();
            Version = 0;
            TimeStamp = DateTimeOffset.Now;
        }
    }
}
