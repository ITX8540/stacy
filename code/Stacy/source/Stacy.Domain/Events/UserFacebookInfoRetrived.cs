﻿using System;
using MediatR;

namespace Stacy.Domain.Events
{
    public class UserFacebookInfoRetrived : INotification
    {
        public readonly string UserId;
        public readonly string UserImageId;
        public readonly string FirstName;
        public readonly string LastName;
        public readonly string FullName;
        public readonly Guid RequestId;
        public readonly DateTimeOffset TimeStamp;

        public UserFacebookInfoRetrived(string userId, string userImageId, string firstName, string lastName, Guid requestId, DateTimeOffset timeStamp)
        {
            UserId = userId;
            UserImageId = userImageId;
            FirstName = firstName;
            LastName = lastName;
            RequestId = requestId;
            TimeStamp = timeStamp;
            FullName = $"{FirstName} {FullName}";
        }
    }
}