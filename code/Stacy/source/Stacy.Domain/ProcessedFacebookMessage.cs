﻿using System;
using ApiAiSDK.Model;
using Stacy.Core.DTOs.Facebook;

namespace Stacy.Domain
{
    public class ProcessedFacebookMessage
    {
        public readonly AIResponse AiResponse;
        public readonly FbMessaging Messaging;
        public readonly Guid RequestId;

        public ProcessedFacebookMessage(Guid requestId, AIResponse aiResponse, FbMessaging messaging)
        {
            AiResponse = aiResponse;
            Messaging = messaging;
            RequestId = requestId;
        }
    }
}
