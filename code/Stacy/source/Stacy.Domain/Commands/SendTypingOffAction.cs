﻿using System;
using MediatR;
using Stacy.Domain.Events;

namespace Stacy.Domain.Commands
{
    public class SendTypingOffAction : IRequest<TypingOffActionSent>
    {
        public readonly Guid RequestId;
        public readonly DateTimeOffset TimeStamp;
        public readonly string ReceipientId;

        public SendTypingOffAction(Guid requestId, string receipientId)
        {
            RequestId = requestId;
            ReceipientId = receipientId;
            TimeStamp = DateTimeOffset.Now;
        }
    }
}