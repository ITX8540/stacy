﻿using MediatR;
using Stacy.Core.DTOs.Facebook;

namespace Stacy.Domain.Commands
{
    public class GetNextEventsMessage : IRequest<FbMessaging>
    {
        public readonly ProcessedFacebookMessage Message;

        public GetNextEventsMessage(ProcessedFacebookMessage message)
        {
            Message = message;
        }
    }
}