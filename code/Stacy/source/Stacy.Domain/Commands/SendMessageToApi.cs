﻿using MediatR;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain.Events;

namespace Stacy.Domain.Commands
{
    public class SendMessageToApi<T> : IRequest<MessagePostedToApi<FbMessaging>> where T : class 
    {
        public readonly T Messaging;
        public readonly string Endpoint;

        public SendMessageToApi(T messaging, string endpoint)
        {
            Messaging = messaging;
            Endpoint = endpoint;
        }
    }
}
