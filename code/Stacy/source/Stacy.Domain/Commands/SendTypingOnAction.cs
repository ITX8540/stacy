﻿using System;
using MediatR;
using Stacy.Domain.Events;

namespace Stacy.Domain.Commands
{
    public class SendTypingOnAction : IRequest<TypingOnActionSent>
    {
        public readonly Guid RequestId;
        public readonly DateTimeOffset TimeStamp;
        public readonly string ReceipientId;

        public SendTypingOnAction(Guid requestId, string receipientId)
        {
            TimeStamp = DateTimeOffset.Now;
            RequestId = requestId;
            ReceipientId = receipientId;
        }
    }
}
