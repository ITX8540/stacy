﻿using System;
using MediatR;
using Stacy.Domain.Events;

namespace Stacy.Domain.Commands
{
    public class ProcessReceivedPostBack<T> : IRequest<string> where T : class
    {
        public readonly T IncomingPostBack;
        public readonly string ApiAiSessionId;
        public readonly Guid RequestId;
        public ProcessReceivedPostBack(Guid requestId, T incomingMessage, string apiAiSessionId)
        {
            IncomingPostBack = incomingMessage;
            ApiAiSessionId = apiAiSessionId;
            RequestId = requestId;
        }
    }
}