﻿using MediatR;
using Stacy.Core.DTOs.Facebook;

namespace Stacy.Domain.Commands
{
    public class GetFbMessageForEventsOfPresenter : IRequest<FbMessaging>
    {
        public readonly ProcessedFacebookMessage Message;

        public GetFbMessageForEventsOfPresenter(ProcessedFacebookMessage message)
        {
            Message = message;
        }
    }
}