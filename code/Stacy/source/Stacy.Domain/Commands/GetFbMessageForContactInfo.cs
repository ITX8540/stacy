﻿using MediatR;
using Stacy.Core.DTOs.Facebook;

namespace Stacy.Domain.Commands
{
    public class GetFbMessageForContactInfo : IRequest<FbMessaging>
    {
        public readonly ProcessedFacebookMessage IncomingMessage;

        public GetFbMessageForContactInfo(ProcessedFacebookMessage incomingMessage)
        {
            IncomingMessage = incomingMessage;
        }
    }
}