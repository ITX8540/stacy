﻿using MediatR;
using Stacy.Core.DTOs.Facebook;

namespace Stacy.Domain.Commands
{
    public class GetGoogleMapsLinkMessage : IRequest<FbMessaging>
    {
        public ProcessedFacebookMessage Message;
        public GetGoogleMapsLinkMessage(ProcessedFacebookMessage message)
        {
            Message = message;
        }

        
    }
}