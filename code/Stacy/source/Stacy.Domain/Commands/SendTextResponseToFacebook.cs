﻿using System;
using MediatR;
using Stacy.Core.DTOs.Facebook;

namespace Stacy.Domain.Commands
{
    public class SendTextResponseToFacebook : IRequest<FbMessaging>
    {
        public readonly string Reponse;
        public readonly string RecepientId;
        public readonly Guid RequestId;

        public SendTextResponseToFacebook(Guid requestId, string recepientId, string reponse)
        {
            Reponse = reponse;
            RequestId = requestId;
            RecepientId = recepientId;
        }
    }
}