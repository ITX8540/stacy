﻿using System;
using MediatR;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain.Events;

namespace Stacy.Domain.Commands
{
    public class SendSenderActionToFacebook : IRequest<SenderActionSentToFacebook>
    {
        public readonly Guid RequestId;
        public readonly FbAction Action;
        public readonly DateTimeOffset TimeStamp;

        public SendSenderActionToFacebook(Guid requestId, FbAction action)
        {
            Action = action;
            RequestId = requestId;
            TimeStamp = DateTimeOffset.Now;
        }
    }
}