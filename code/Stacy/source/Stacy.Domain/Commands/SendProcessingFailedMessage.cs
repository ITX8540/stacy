﻿using System;
using MediatR;

namespace Stacy.Domain.Commands
{
    public class SendProcessingFailedMessage : IRequest<Unit>
    {
        public readonly Guid Id;

        public readonly DateTimeOffset TimeStamp;

        public readonly Guid RequestId;

        public readonly string ReceipientId;

        public SendProcessingFailedMessage(Guid requestId, string receipientId)
        {
            Id = Guid.NewGuid();
            TimeStamp = DateTimeOffset.Now;
            RequestId = requestId;
            ReceipientId = receipientId;
        }
    }
}
