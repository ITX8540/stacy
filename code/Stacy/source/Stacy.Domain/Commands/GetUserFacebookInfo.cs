﻿using System;
using MediatR;
using Stacy.Domain.Events;

namespace Stacy.Domain.Commands
{
    public class GetUserFacebookInfo : IRequest<UserFacebookInfoRetrived>
    {
        public readonly string UserId;
        public readonly string FacebookPageToken;
        public readonly Guid RequestId;
        public readonly DateTimeOffset TimeStamp;
        public GetUserFacebookInfo(string userId, string facebookPageToken, Guid requestId)
        {
            UserId = userId;
            FacebookPageToken = facebookPageToken;
            RequestId = requestId;
            TimeStamp = DateTimeOffset.Now;
        }
    }
}
