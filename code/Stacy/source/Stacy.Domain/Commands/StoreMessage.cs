﻿using MediatR;
using Stacy.Core.DTOs.ApiAi;

namespace Stacy.Domain.Commands
{
    public class StoreMessage : IRequest<bool>
    {
        public StoreMessage(ApiAiRequest request)
        {
            Request = request;
        }

        public ApiAiRequest Request { get; set; }
    }
}