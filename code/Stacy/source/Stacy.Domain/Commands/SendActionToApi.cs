﻿using System;
using MediatR;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain.Events;

namespace Stacy.Domain.Commands
{
    public class SendActionToApi<T> : IRequest<MessagePostedToApi<FbAction>> where T : class
    {
        public readonly T Action;
        public readonly string Endpoint;
        public readonly Guid RequestId;

        public SendActionToApi(Guid requestId, T action, string endpoint)
        {
            Action = action;
            Endpoint = endpoint;
            RequestId = requestId;
        }
    }
}