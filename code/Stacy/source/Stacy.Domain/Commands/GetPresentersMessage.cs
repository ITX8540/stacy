﻿using MediatR;
using Stacy.Core.DTOs.Facebook;

namespace Stacy.Domain.Commands
{
    public class GetPresentersMessage : IRequest<FbMessaging>
    {
        public readonly ProcessedFacebookMessage Message;
        public GetPresentersMessage(ProcessedFacebookMessage message)
        {
            Message = message;
        }
    }
}