﻿using System;
using MediatR;
using Stacy.Domain.Events;

namespace Stacy.Domain.Commands
{
    public class ProcessReceivedMessage<T> : IRequest<MessageProcessed>, INotification where T : class
    {
        public T IncomingMessage { get; }

        public string ApiAiSessionId { get; }

        public Guid RequestId { get; set; }

        public DateTimeOffset TimeStamp { get; set; }
        public string UserId { get; set; }

        public ProcessReceivedMessage(T incomingMessage, string userId, string apiAiSessionId, Guid requestId)
        {
            IncomingMessage = incomingMessage;
            UserId = userId;
            ApiAiSessionId = apiAiSessionId;
            RequestId = requestId;
            TimeStamp = DateTimeOffset.Now;
        }
    }
}
