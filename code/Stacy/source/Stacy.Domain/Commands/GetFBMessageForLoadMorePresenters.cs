﻿using MediatR;
using Stacy.Core.DTOs.Facebook;

namespace Stacy.Domain.Commands
{
    public class GetFBMessageForLoadMorePresenters : IRequest<FbMessaging>
    {
        public readonly ProcessedFacebookMessage Message;

        public GetFBMessageForLoadMorePresenters(ProcessedFacebookMessage message)
        {
            Message = message;
        }
    }
}