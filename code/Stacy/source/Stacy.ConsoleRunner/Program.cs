﻿using System;
using ApiAiSDK;
using ApiAiSDK.Model;
using Stacy.Core;
using Stacy.DataAccess.Infrastructure;
using System.Linq;
using Stacy.DataAccess.Entities;
using System.Collections.Generic;
using Stacy.Core.DTOs.Facebook;

namespace Stacy.ConsoleRunner
{
    class Program
    {
        static string _sessionId = string.Empty;
        static bool _quitFlag = false;
        private const string QuitKeyword = "quit;";
        private static AIDataService _dataService;

        static void Main(string[] args)
        {

            Console.WriteLine($"Press Ctrl+C or write {QuitKeyword} to exit");
            while (!_quitFlag)
            {

                var keyInfo = Console.ReadKey();
                _quitFlag = keyInfo.Key == ConsoleKey.C
                         && keyInfo.Modifiers == ConsoleModifiers.Control;
                try
                {
                    string input = string.Empty;
                    string responseMessage = SendMessageToApiAiAndReturnFulfilment(keyInfo.KeyChar, out input);
                    _quitFlag = input.ToLower().Trim().Equals(QuitKeyword);
                    Console.WriteLine(responseMessage);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }


            }
        }
        private static string SendMessageToApiAiAndReturnFulfilment(char keyChar, out string inputMessage)
        {
            var message = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(keyChar.ToString()))
            {
                message = keyChar.ToString() + message;

            }
            inputMessage = message;
            CreateDataService();

            var request = new AIRequest(message);

            
            var response = _dataService.Request(request);

            string responseText = "";
            if (response.Result.Action.ToString().Equals("findLocation", StringComparison.OrdinalIgnoreCase))
            {
                using (var db = new StacyDatabase())
                {
                    var eventDetails =
                        db.GetTable<EventLocation>()    
                            .ToList();
                    responseText = eventDetails.ToString();
                }
            }
            else
            {
                responseText = response.Result.Fulfillment.Speech + "(" + response.Result.Action.ToString() + ")";
            }
            _sessionId = response.SessionId;
            return responseText;
        }

        private static AIResponse MakeRequest(AIDataService service, AIRequest request)
        {
            var aiResponse = service.Request(request);

            return aiResponse;
        }

        private static void CreateDataService()
        {
            if (_dataService != null)
            {
                return;
            }
            var config = new AIConfiguration(StacyConfiguration.ApiAiToken, SupportedLanguage.English);
            config.DebugLog = true;
            if (string.IsNullOrWhiteSpace(_sessionId))
            {
                config.SessionId = _sessionId;
            }
            _dataService = new AIDataService(config);
            
        }
    }
}
