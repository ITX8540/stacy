﻿using System;
using ApiAiSDK;
using ApiAiSDK.Model;
using FluentAssertions;
using Stacy.Core;
using Xunit;

namespace Stacy.Tests
{
    public class StacyTests
    {

        [Fact]
        public void Create_Stacy_Api_Ai_Service()
        {
            var stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken);
            stacyApiAiService.Should().NotBeNull();
            stacyApiAiService.Token.Should().NotBeNullOrWhiteSpace();
            stacyApiAiService.Token.Should().BeEquivalentTo(StacyConfiguration.ApiAiToken);

        }

        [Fact]
        public void Start_Stacy_Api_Service_with_token_and_sessionId()
        {
            var sessionId = Guid.NewGuid().ToString();
            StacyApiAiService stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken, sessionId);

            stacyApiAiService.Token.Should().NotBeNullOrWhiteSpace().And.BeEquivalentTo(StacyConfiguration.ApiAiToken);
            stacyApiAiService.SessionId.Should().NotBeNullOrWhiteSpace().And.BeEquivalentTo(sessionId);
            
        }

        [Fact]
        public void Start_Stacy_Api_Service_in_Debug_mode()
        {
            var stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken);
            stacyApiAiService.Should().NotBeNull();
            stacyApiAiService.Token.Should().NotBeNullOrWhiteSpace();
            stacyApiAiService.Token.Should().BeEquivalentTo(StacyConfiguration.ApiAiToken);
            stacyApiAiService.SessionId.Should().BeNullOrEmpty();
            stacyApiAiService.DebugMode.Should().BeFalse();

            var sessionId = Guid.NewGuid().ToString();
            stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken, sessionId);
            stacyApiAiService.Token.Should().NotBeNullOrWhiteSpace().And.BeEquivalentTo(StacyConfiguration.ApiAiToken);
            stacyApiAiService.SessionId.Should().NotBeNullOrWhiteSpace().And.BeEquivalentTo(sessionId);
            stacyApiAiService.DebugMode.Should().BeFalse();


            var debugMode = true;
            stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken,debugMode);
            stacyApiAiService.Should().NotBeNull();
            stacyApiAiService.Token.Should().NotBeNullOrWhiteSpace();
            stacyApiAiService.Token.Should().BeEquivalentTo(StacyConfiguration.ApiAiToken);
            stacyApiAiService.SessionId.Should().BeNullOrEmpty();
            stacyApiAiService.DebugMode.Should().BeTrue();

            sessionId = Guid.NewGuid().ToString();
            stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken, sessionId,debugMode);
            stacyApiAiService.Token.Should().NotBeNullOrWhiteSpace().And.BeEquivalentTo(StacyConfiguration.ApiAiToken);
            stacyApiAiService.SessionId.Should().NotBeNullOrWhiteSpace().And.BeEquivalentTo(sessionId);
            stacyApiAiService.DebugMode.Should().BeTrue();

            debugMode = false;
            stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken, debugMode);
            stacyApiAiService.Should().NotBeNull();
            stacyApiAiService.Token.Should().NotBeNullOrWhiteSpace();
            stacyApiAiService.Token.Should().BeEquivalentTo(StacyConfiguration.ApiAiToken);
            stacyApiAiService.SessionId.Should().BeNullOrEmpty();
            stacyApiAiService.DebugMode.Should().BeFalse();

            sessionId = Guid.NewGuid().ToString();
            stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken, sessionId, debugMode);
            stacyApiAiService.Token.Should().NotBeNullOrWhiteSpace().And.BeEquivalentTo(StacyConfiguration.ApiAiToken);
            stacyApiAiService.SessionId.Should().NotBeNullOrWhiteSpace().And.BeEquivalentTo(sessionId);
            stacyApiAiService.DebugMode.Should().BeFalse();
        }

        [Fact]
        public void Start_ApiAi_Serivice()
        {
            var stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken, true);
            stacyApiAiService.SessionId.Should().BeNullOrWhiteSpace();

            stacyApiAiService.StartDataService();
            stacyApiAiService.SessionId.Should().NotBeNullOrWhiteSpace();

            var sessionId = Guid.NewGuid().ToString();
            stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken,sessionId);
            stacyApiAiService.SessionId.Should().NotBeNullOrWhiteSpace();
            stacyApiAiService.SessionId.Should().BeEquivalentTo(sessionId);
            
            
        }

        [Fact]
        public void Start_ApiAi_Serivice_in_debug_mode()
        {
            var stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken, true);
            stacyApiAiService.Configuration.Should().BeNull();
            stacyApiAiService.StartDataService();
            stacyApiAiService.Configuration.Should().NotBeNull();
            stacyApiAiService.Configuration.DebugLog.Should().BeTrue();
        }

        [Fact]
        public void Start_ApiAi_Service_with_SessionId()
        {
            var sessionId = Guid.NewGuid().ToString();
            var stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken, sessionId);
            stacyApiAiService.SessionId.Should().NotBeNullOrWhiteSpace();
            stacyApiAiService.StartDataService();
            stacyApiAiService.SessionId.Should().BeEquivalentTo(sessionId);
            stacyApiAiService.Configuration.SessionId.Should().BeEquivalentTo(sessionId);
        }
        [Fact]
        public void Send_Message_To_Stacy_ApiAi_Service()
        {
            string message = "Hi";
            var stacyApiAiService = new StacyApiAiService(StacyConfiguration.ApiAiToken, true);
            stacyApiAiService.SessionId.Should().BeNullOrWhiteSpace();
            stacyApiAiService.StartDataService();
            var response = stacyApiAiService.Send(message);
            response.Should().NotBeNull();
        }
				
    }

    public class StacyApiAiService
    {
        public readonly string Token;
        public AIConfiguration Configuration { get; private set; }
        private string _sessionId;

        public string SessionId
        {
            get
            {
                if (_aiDataService != null)
                {
                    return _aiDataService.SessionId;
                }
                    return _sessionId;
            }
        }

        private bool _debugMode;

        public bool DebugMode
        {
            get { return _debugMode; }
        }

        private AIDataService _aiDataService;
        public StacyApiAiService(string token)
        {
            Token = token;
        }

        public void StartDataService()
        {
            if (_aiDataService != null)
            {
                return;
            }
            Configuration = new AIConfiguration(Token, SupportedLanguage.English);
            Configuration.DebugLog = DebugMode;
            if (!string.IsNullOrWhiteSpace(SessionId))
            {
                Configuration.SessionId = SessionId;
            }
            _aiDataService= new AIDataService(Configuration);
        }


        public StacyApiAiService(string token, string sessionId):this(token)
        {
            _sessionId = sessionId;

        }

        public StacyApiAiService(string token, bool debugMode):this(token)
        {
            _debugMode = debugMode;
        }

        public StacyApiAiService(string token, string sessionId, bool debugMode):this(token,sessionId)
        {
            _debugMode = debugMode;
        }

        public AIResponse Send(string message)
        {
			var requet = new AIRequest(message);
			return _aiDataService.Request(requet);
        }
    }
}
