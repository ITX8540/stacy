﻿using System.Linq;
using FluentAssertions;
using Stacy.Core;
using Stacy.DataAccess.Entities;
using Stacy.DataAccess.Infrastructure;
using Xunit;

namespace Stacy.Tests.DataAccessTests
{
    public class EventRepositoryTests
    {
        [Fact]
        public void Create_Event_Repo()
        {
            using (var db = new StacyDatabase())
            {
                //act
                var repo = new EventRepository(db);
                
                //assess
                repo.Should().NotBeNull();
            }
        }

        [Fact]
        public void get_all_parent_events()
        {
            using (var db = new StacyDatabase())
            {
                //arrange
                var events = db.GetTable<Event>().Where(x => !x.ParentId.HasValue).ToList();
                var eventCount = events.Count;

                //act
                var repo = new EventRepository(db);
                var parentEvents = repo.Get(x => !x.ParentId.HasValue).ToList();
                
                //assess
                parentEvents.Count.ShouldBeEquivalentTo(eventCount);
                foreach (var parentEvent in parentEvents)
                {
                    var @event = events.FirstOrDefault(x => x.Id == parentEvent.Id);
                    @event.Should().NotBeNull();
                }
            }
        }

        //[Fact]
        public void get_all_events_with_parent()
        {
            using (var db = new StacyDatabase())
            {
                //arrange
                var events = db.GetTable<Event>().Where(x => x.ParentId.HasValue).ToList();
                events.Any(x => !x.ParentId.HasValue).Should().BeFalse();
                var eventCount = events.Count;

                //act
                var repo = new EventRepository(db);
                var parentEvents = repo.Get(x => x.ParentId.HasValue).ToList();

                //assess
                parentEvents.Any(x => !x.ParentId.HasValue).Should().BeFalse();
                parentEvents.Count.ShouldBeEquivalentTo(eventCount);
                foreach (var parentEvent in parentEvents)
                {
                    var @event = events.FirstOrDefault(x => x.Id == parentEvent.Id);
                    @event.Should().NotBeNull();
                }
            }
        }

        [Fact]
        public void event_has_associated_data()
        {
            using (var db = new StacyDatabase())
            {
                //arrange
                var tempfirstParentEvent = db.GetTable<Event>().FirstOrDefault(x => x.ParentId == null);
                tempfirstParentEvent.Should().NotBeNull();
                var rooms = db.GetTable<EventRoom>().Where(x => x.EventId == tempfirstParentEvent.Id).ToList();
                var presenters = db.GetTable<EventPresenter>().Where(x => x.EventId == tempfirstParentEvent.Id).ToList();
                var details = db.GetTable<EventDetail>().Where(x => x.EventId == tempfirstParentEvent.Id).ToList();
                var locations = db.GetTable<EventLocation>().Where(x => x.EventId == tempfirstParentEvent.Id).ToList();
                var eventType = db.GetTable<Classification>().FirstOrDefault(x => x.Id == tempfirstParentEvent.EventTypeId);
                var contacts = db.GetTable<EventContact>().Where(x => x.EventId == tempfirstParentEvent.Id).ToList();
                var children = db.GetTable<Event>().Where(x => x.ParentId == tempfirstParentEvent.Id).ToList();

                //act
                var repo = new EventRepository(db);
                var parentEvent = repo.GetById(tempfirstParentEvent.Id);

                //assess
                parentEvent.EventTypeId.Should().Be(tempfirstParentEvent.EventTypeId);
                eventType.Should().NotBeNull();
                parentEvent.EventType.Id.Should().Be(eventType.Id);
                parentEvent.Children.Should().HaveSameCount(children);
                foreach (var eventChild in parentEvent.Children)
                {
                    children.Any(x => x.Id == eventChild.Id).Should().BeTrue();
                }
                foreach (var eventContact in parentEvent.EventContacts)
                {
                    contacts.Any(x => x.Id == eventContact.Id).Should().BeTrue();
                }
                foreach (var eventLocation in parentEvent.EventLocations)
                {
                    locations.Any(x => x.Id == eventLocation.Id).Should().BeTrue();
                }
                foreach (var eventDetail in parentEvent.EventDetails)
                {
                    details.Any(x => x.Id == eventDetail.Id).Should().BeTrue();
                }
                foreach (var eventPresenter in parentEvent.EventPresenters)
                {
                    presenters.Any(x => x.Id == eventPresenter.Id).Should().BeTrue();
                }
                foreach (var eventRoom in parentEvent.EventRooms)
                {
                    rooms.Any(x => x.Id == eventRoom.Id).Should().BeTrue();
                }
            }
        }

        //[Fact]
        public void repo_creates_gets_updates_deletes_event()
        {
            using (var db = new StacyDatabase())
            {
                var typeId =
                    db.GetTable<Classification>()
                        .Where(
                            x =>
                                x.Name == StacyContstants.EventTypes.Conference &&
                                x.ClassificationTypeIdfkey.Name == StacyContstants.ClassificationTypes.EventType).Select(x => x.Id).FirstOrDefault();
                var newEvent = new Event()
                {
                    EventTypeId = typeId,
                   
                };
                
                var repo = new EventRepository(db);
                var eventCount = repo.Get().Count();

                repo.Insert(newEvent);
                var newEventCount = repo.Get().Count();

                newEventCount.Should().BeGreaterThan(eventCount);
                var expected = eventCount + 1;
                newEventCount.Should().Be(expected);
                var newEventInDb = repo.Get(x => x.EventTypeId == typeId && !x.ParentId.HasValue && !x.Children.Any())
                    .FirstOrDefault();

                newEventInDb.Should().NotBeNull();
                var newEventId = newEventInDb.Id;

                var newTypeId =
                    db.GetTable<Classification>()
                        .Where(
                            x =>
                                x.Name == StacyContstants.EventTypes.Keynote &&
                                x.ClassificationTypeIdfkey.Name == StacyContstants.ClassificationTypes.EventType).Select(x => x.Id).FirstOrDefault();

                newEventInDb.EventTypeId = newTypeId;

                repo.Update(newEventInDb);

                var newEventInDb2 = repo.GetById(newEventId);
                newEventInDb2.Should().NotBeNull();
                newEventInDb2.EventTypeId.Should().Be(newTypeId);

                repo.Delete(newEventInDb2);
                var finalCount = repo.Get().Count();
                finalCount.Should().Be(eventCount);


            }
        }
    }
}
