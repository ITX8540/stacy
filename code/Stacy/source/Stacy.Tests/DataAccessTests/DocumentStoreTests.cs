﻿using System;
using System.Linq;
using FluentAssertions;
using Marten;
using Stacy.Core;
using Stacy.Domain.Events;
using Xunit;

namespace Stacy.Tests.DataAccessTests
{
    public class DocumentStoreTests
    {
        [Fact]
        public void Store_event_to_documentstore()
        {
            var store = DocumentStore.For(StacyConfiguration.StacyDocumentStoreConnectionString);

            using (var session = store.LightweightSession())
            {
                var existing = session
                    .Query<MessageReceived>().Where(x => x.Sender.Equals("1112") && x.MessageContent.Equals("Hello") ).FirstOrDefault();

                if (existing == null)
                {
                    var messageReceived = new MessageReceived(Guid.NewGuid(), DateTimeOffset.Now, "1112","Hello","smalltalk");
                    session.Store<MessageReceived>(messageReceived);
                    session.SaveChanges();
                }
            }

            using (var session = store.OpenSession())
            {
                var existing = session
                    .Query<MessageReceived>().Where(x => x.Sender.Equals("1112") && x.MessageContent.Equals("Hello")).FirstOrDefault();
                var messageReceivedEvent = existing;
                messageReceivedEvent.Should().NotBeNull();
                messageReceivedEvent.Should().BeOfType<MessageReceived>();
                messageReceivedEvent.Id.Should().NotBeEmpty();
                messageReceivedEvent.Sender.Should().Be("1112");
                messageReceivedEvent.MessageContent.Should().Be("Hello");

            }
        }
    }
}
