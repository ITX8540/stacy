﻿using System.Linq;
using FluentAssertions;
using LinqToDB;
using Stacy.DataAccess.Entities;
using Stacy.DataAccess.Infrastructure;
using Xunit;

namespace Stacy.Tests.DataAccessTests
{
    public class StacyDatabaseTests
    {
        [Fact]
        public void Load_data_from_stacy_db()
        {
            using (var db = new StacyDatabase())
            {
                var eventDetails =
                    db.GetTable<EventDetail>()
                        .LoadWith(x => x.Event)
                        .LoadWith(x => x.Event.EventType)
                        .ToList();

                eventDetails.Should().NotBeNullOrEmpty();
            }
        }

        [Fact]
        public void RepoTest()
        {
            using (var db = new StacyDatabase())
            {
                var eventDetailsRepo = new GenericRepository<EventDetail>(db);
                var data = eventDetailsRepo.Get();
                var topconf = eventDetailsRepo.Get(x => x.Name.Contains("Topconf"));
                topconf.Should().NotBeNull();
                topconf.FirstOrDefault().Name.Should().ContainEquivalentOf("Topconf");

                data.Should().NotBeNull();

                var firstDetailId = data.First().Id;
                var first = eventDetailsRepo.GetById(firstDetailId);
                first.Should().NotBeNull();

                var uOW = new UnitOfWork(db);
                var eventDetails = uOW.EventDetailRepository.GetById(firstDetailId);

                eventDetails.Should().NotBeNull().And.Subject.As<EventDetail>().Name.ShouldBeEquivalentTo(first.Name);
            }
        }

        [Fact]
        public void test_unit_of_work()
        {
            var uow = new UnitOfWork();

            var events = uow.FullEventInfoRepository.Get();

            events.Should().NotBeNull();
        }
    }
}
