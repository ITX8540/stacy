﻿using ApiAiSDK;
using ApiAiSDK.Model;
using FluentAssertions;
using Stacy.Core;
using Xunit;

namespace Stacy.Tests.AiTests
{
    public class ApiAiSdkTests
    {
        [Fact]
        public void TestTextRequest()
        {
            var config = new AIConfiguration(StacyConfiguration.ApiAiToken, SupportedLanguage.English);

            var apiAi = new ApiAi(config);

            var response = apiAi.TextRequest("hello");

            response.Should().NotBeNull();
            response.Result.Action.Should().Be("smalltalk.greetings");
            response.Result.Fulfillment.Speech.Should().NotBeNullOrWhiteSpace();
        }

        [Fact]
        public void TextRequestWithDataService()
        {
            var dataService = CreateDataService();

            var request = new AIRequest("Hello");

            var response = dataService.Request(request);
            
            response.Should().NotBeNull();
            response.Result.Action.Should().Be("smalltalk.greetings");
            response.Result.Fulfillment.Speech.Should().NotBeNullOrWhiteSpace();
            response.SessionId.Should().NotBeNullOrEmpty();
        }

        private AIDataService CreateDataService()
        {
            var config = new AIConfiguration(StacyConfiguration.ApiAiToken, SupportedLanguage.English);
            config.DebugLog = true;
            var dataService = new AIDataService(config);
            return dataService;
        }

        private AIResponse MakeRequest(AIDataService service, AIRequest request)
        {
            var aiResponse = service.Request(request);
            aiResponse.Should().NotBeNull();
            aiResponse.IsError.Should().BeTrue();
            aiResponse.Id.Should().NotBeNullOrWhiteSpace();
            aiResponse.Result.Should().NotBeNull();

            return aiResponse;
        }
    }
}
