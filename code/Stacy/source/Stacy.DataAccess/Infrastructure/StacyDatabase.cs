﻿using LinqToDB.Data;

namespace Stacy.DataAccess.Infrastructure
{
    public class StacyDatabase : DataConnection
    {
        private static bool TraceEnabled = false;

        public StacyDatabase(bool traceEnabled = false)
            : base("StacyDatabase")
        {
            if (traceEnabled)
            {
                
            }   
        }
        public StacyDatabase(string providerName, string connectionString, bool traceSql = false) 
            :base(providerName, connectionString)
        {
            if (traceSql)
            {
                EnableLogging();
            }
        }

        public static void Trace(TraceInfo traceInfo)
        {

            string commText = string.Format(traceInfo.Command.CommandText);

            foreach (var parameter in traceInfo.Command.Parameters)
            {
                var param = parameter as System.Data.Common.DbParameter;
                if (param != null)
                {
                    commText = commText.Replace(":" + param.ParameterName, "'" + param.Value.ToString() + "'");
                }
            }

            System.Diagnostics.Debug.WriteLine(commText);
        }
        public static void EnableLogging()
        {
            //Npgsql.Logging.NpgsqlLogManager.Provider = new DiagnosticLoggingProvider(Npgsql.Logging.NpgsqlLogLevel.Trace,
            //        true, false);
            TurnTraceSwitchOn(System.Diagnostics.TraceLevel.Verbose);
            OnTrace += OnDbTrace;

        }

        public static void OnDbTrace(TraceInfo traceInfo)
        {
            System.Diagnostics.Debug.WriteLine(traceInfo.SqlText);
        }
    }
}
