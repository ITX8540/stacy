﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqToDB;
using Stacy.DataAccess.Entities;

namespace Stacy.DataAccess.Infrastructure
{
    public class EventRepository : Repository<Event>
    {
        public EventRepository(StacyDatabase database) : base(database)
        {
            LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
        }


        public override IEnumerable<Event> Get(Expression<Func<Event, bool>> filter = null, Func<IQueryable<Event>, IOrderedQueryable<Event>> orderBy = null)
        {
            Table
                .LoadWith(x => x.Children)
                .LoadWith(x => x.EventContacts)
                .LoadWith(x => x.EventDetails)
                .LoadWith(x => x.EventLocations)
                .LoadWith(x => x.EventRooms)
                .LoadWith(x => x.EventType)
                .LoadWith(x => x.EventPresenters);
            IQueryable<Event> query = null;

            if (filter != null)
            {
                query = Table.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            if (query != null)
            {
                return query.ToList();
            }
            return Table.ToList();
        }

        public override Event GetById(Guid id)
        {
            Table
                .LoadWith(x => x.Children)
                .LoadWith(x => x.EventContacts)
                .LoadWith(x => x.EventDetails)
                .LoadWith(x => x.EventLocations)
                .LoadWith(x => x.EventRooms)
                .LoadWith(x => x.EventType)
                .LoadWith(x => x.EventPresenters);
            return Table.Find(id);
        }

        public override void Insert(Event entity)
        {
            Database.Insert(entity);
        }

        public override void Delete(Event entity)
        {
            Table.Delete(x => x.Id == entity.Id);
        }

        public override void Update(Event entity)
        {
            Table.Update(x => new Event {EventTypeId = entity.EventTypeId, ParentId = entity.ParentId});
        }
    }
}
