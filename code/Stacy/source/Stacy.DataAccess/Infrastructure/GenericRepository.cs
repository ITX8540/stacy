﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using LinqToDB;
using Stacy.DataAccess.Entities;

namespace Stacy.DataAccess.Infrastructure
{
    public class GenericRepository<TEntity> : Repository<TEntity> where TEntity : class , IEntity
    {
        public GenericRepository(StacyDatabase database) : base(database)
        {
        }
        public override IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {

            IQueryable<TEntity> query = null;

            if (filter != null)
            {
                query = Table.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            if (query != null)
            {
                return query.ToList();
            }
            return Table.ToList();
        }

        public override TEntity GetById(Guid id)
        {
            return Table.FirstOrDefault(x => x.Id == id);
        }

        public override void Insert(TEntity entity)
        {
            Database.Insert(entity);
        }

        public override void Delete(TEntity entity)
        {
            Database.BeginTransaction(IsolationLevel.ReadCommitted);
            try
            {
                Database.Delete(entity);
                Database.CommitTransaction();
            }
            catch (Exception)
            {
                Database.RollbackTransaction();
                throw;
            }
        }

        public override void Update(TEntity entity)
        {
            Database.BeginTransaction(IsolationLevel.ReadCommitted);
            try
            {
                Database.Update(entity);
                Database.CommitTransaction();
            }
            catch (Exception)
            {
                Database.RollbackTransaction();
                throw;
            }
        }
    }
}
