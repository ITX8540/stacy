﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqToDB;
using Stacy.DataAccess.Entities;

namespace Stacy.DataAccess.Infrastructure
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class ,IEntity
    {
        internal StacyDatabase Database;
        internal ITable<TEntity> Table;
        protected Repository(StacyDatabase database)
        {
            Database = database;
            Table = Database.GetTable<TEntity>();
        }

        public abstract IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null);

        public abstract TEntity GetById(Guid id);

        public abstract void Insert(TEntity entity);

        public abstract void Delete(TEntity entity);

        public abstract void Update(TEntity entity);
    }
}