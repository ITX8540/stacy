﻿using System;
using System.Data;
using Stacy.DataAccess.Entities;

namespace Stacy.DataAccess.Infrastructure
{
    
    public class UnitOfWork : IDisposable
    {
        private readonly StacyDatabase _database;

        private EventRepository _fullEventInfoRepository;
        private GenericRepository<Event> _eventRepository;
        private GenericRepository<EventDetail> _eventDetailRepository;
        private GenericRepository<EventRoom> _eventRoomRepository;
        private GenericRepository<EventContact> _eventContactRepository;
        private GenericRepository<EventPresenter> _eventPresenterRepository;
        private GenericRepository<EventLocation> _eventLocationRepository;
        private GenericRepository<Classification> _classificationRepository;
        private GenericRepository<ClassificationType> _classificationTypeRepository;

        public UnitOfWork()
        {
            LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
            _database = new StacyDatabase();
            _database.BeginTransaction(IsolationLevel.ReadUncommitted);
        }
        public UnitOfWork(StacyDatabase database)
        {
            LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
            _database = database;
            _database.BeginTransaction(IsolationLevel.ReadUncommitted);
        }
        public GenericRepository<Event> EventRepository
        {
            get { return _eventRepository ?? (_eventRepository = new GenericRepository<Event>(_database)); }
        }

        public GenericRepository<EventDetail> EventDetailRepository
        {
            get { return _eventDetailRepository ?? (_eventDetailRepository = new GenericRepository<EventDetail>(_database)); }
        }

        public GenericRepository<EventRoom> EventRoomRepository
        {
            get { return _eventRoomRepository ?? (_eventRoomRepository = new GenericRepository<EventRoom>(_database)); }
        }

        public GenericRepository<EventContact> EventContactRepository
        {
            get { return _eventContactRepository ?? (_eventContactRepository = new GenericRepository<EventContact>(_database)); }
        }

        public GenericRepository<EventPresenter> EventPresenterRepository
        {
            get { return _eventPresenterRepository ?? (_eventPresenterRepository = new GenericRepository<EventPresenter>(_database)); }
        }

        public GenericRepository<EventLocation> EventLocationRepository
        {
            get { return _eventLocationRepository ?? (_eventLocationRepository = new GenericRepository<EventLocation>(_database)); }
        }

        public GenericRepository<Classification> ClassificationRepository
        {
            get { return _classificationRepository ?? (_classificationRepository = new GenericRepository<Classification>(_database)); }
        }

        public GenericRepository<ClassificationType> ClassificationTypeRepository
        {
            get { return _classificationTypeRepository ?? (_classificationTypeRepository = new GenericRepository<ClassificationType>(_database)); }
        }

        public EventRepository FullEventInfoRepository
        {
            get { return _fullEventInfoRepository ?? (_fullEventInfoRepository = new EventRepository(database:_database)); }
        }

        public void Save()
        {
            try
            {
                _database.CommitTransaction();
            }
            catch (Exception)
            {
                _database.RollbackTransaction();
                throw;
            }
        }
        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _database.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
