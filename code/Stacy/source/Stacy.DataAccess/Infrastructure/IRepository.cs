﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Stacy.DataAccess.Entities;

namespace Stacy.DataAccess.Infrastructure
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null);
        TEntity GetById(Guid id);

        void Insert(TEntity entity);
        void Delete(TEntity entity);

        void Update(TEntity entity);
    }
}
