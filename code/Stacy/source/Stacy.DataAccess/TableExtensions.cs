﻿using System;
using System.Linq;
using LinqToDB;
using Stacy.DataAccess.Entities;

namespace Stacy.DataAccess
{
    public static class TableExtensions
    {
        public static Event Find(this ITable<Event> table, Guid id )
        {
            return table.FirstOrDefault(x => x.Id == id);
        }
    }
}
