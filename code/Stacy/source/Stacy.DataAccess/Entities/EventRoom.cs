﻿using System;
using LinqToDB.Mapping;

namespace Stacy.DataAccess.Entities
{
    [Table(Schema = "public", Name = "EventRoom")]
    public class EventRoom : IEntity
    {
        [PrimaryKey, NotNull]
        public Guid Id { get; set; } // uuid
        [Column, NotNull]
        public Guid EventId { get; set; } // uuid
        [Column, Nullable]
        public string Name { get; set; } // text
        [Column, Nullable]
        public string Description { get; set; } // text
        [Column, NotNull]
        public Guid EventRoomTypeId { get; set; } // uuid
        [Column, Nullable]
        public object ExtraInfo { get; set; } // USER-DEFINED

        #region Associations

        /// <summary>
        /// EventRoom_EventId_fkey
        /// </summary>
        [Association(ThisKey = "EventId", OtherKey = "Id", CanBeNull = false, KeyName = "EventRoom_EventId_fkey", BackReferenceName = "EventRoomEventIdfkeys")]
        public Event Event { get; set; }

        /// <summary>
        /// EventRoom_EventRoomTypeId_fkey
        /// </summary>
        [Association(ThisKey = "EventRoomTypeId", OtherKey = "Id", CanBeNull = false, KeyName = "EventRoom_EventRoomTypeId_fkey", BackReferenceName = "EventRoomEventRoomTypeIdfkeys")]
        public Classification EventRoomType { get; set; }

        #endregion
    }
}