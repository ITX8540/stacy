﻿using System;

namespace Stacy.DataAccess.Entities
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}