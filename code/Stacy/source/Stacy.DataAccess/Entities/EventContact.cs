﻿using System;
using LinqToDB.Mapping;

namespace Stacy.DataAccess.Entities
{
    [Table(Schema = "public", Name = "EventContact")]
    public class EventContact : IEntity
    {
        [PrimaryKey, NotNull]
        public Guid Id { get; set; } // uuid
        [Column, NotNull]
        public Guid EventId { get; set; } // uuid
        [Column, Nullable]
        public string Website { get; set; } // text
        [Column, Nullable]
        public string Email { get; set; } // text
        [Column, Nullable]
        public string PhoneNumber { get; set; } // text
        [Column, Nullable]
        public object ExtraInfo { get; set; } // USER-DEFINED

        #region Associations

        /// <summary>
        /// EventContact_EventId_fkey
        /// </summary>
        [Association(ThisKey = "EventId", OtherKey = "Id", CanBeNull = false, KeyName = "EventContact_EventId_fkey", BackReferenceName = "EventContactEventIdfkeys")]
        public Event Event { get; set; }

        #endregion
    }
}