﻿using System;
using System.Collections.Generic;
using LinqToDB.Mapping;

namespace Stacy.DataAccess.Entities
{
    [Table(Schema = "public", Name = "Classification")]
    public class Classification : IEntity
    {
        [PrimaryKey, NotNull]
        public Guid Id { get; set; } // uuid
        [Column, Nullable]
        public string Name { get; set; } // text
        [Column, Nullable]
        public string Description { get; set; } // text
        [Column, NotNull]
        public Guid ClassificationTypeId { get; set; } // uuid
        [Column, Nullable]
        public Guid? ParentId { get; set; } // uuid

        #region Associations

        /// <summary>
        /// Classification_ClassificationTypeId_fkey
        /// </summary>
        [Association(ThisKey = "ClassificationTypeId", OtherKey = "Id", CanBeNull = false, KeyName = "Classification_ClassificationTypeId_fkey", BackReferenceName = "ClassificationClassificationTypeIdfkeys")]
        public ClassificationType ClassificationTypeIdfkey { get; set; }

        /// <summary>
        /// Classification_ParentId_fkey
        /// </summary>
        [Association(ThisKey = "ParentId", OtherKey = "Id", CanBeNull = true, KeyName = "Classification_ParentId_fkey", BackReferenceName = "Classification_ParentId_fkey_BackReferences")]
        public Classification ParentIdfkey { get; set; }

        /// <summary>
        /// Event_EventTypeId_fkey_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "EventTypeId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<Event> EventEventTypeIdfkeys { get; set; }

        /// <summary>
        /// Classification_ParentId_fkey_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "ParentId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<Classification> Classification_ParentId_fkey_BackReferences { get; set; }

        /// <summary>
        /// EventRoom_EventRoomTypeId_fkey_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "EventRoomTypeId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<EventRoom> EventRoomEventRoomTypeIdfkeys { get; set; }

        #endregion
    }
}