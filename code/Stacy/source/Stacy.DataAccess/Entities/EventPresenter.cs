﻿using System;
using LinqToDB.Mapping;

namespace Stacy.DataAccess.Entities
{
    [Table(Schema = "public", Name = "EventPresenter")]
    public class EventPresenter : IEntity
    {
        [PrimaryKey, NotNull]
        public Guid Id { get; set; } // uuid
        [Column, NotNull]
        public Guid EventId { get; set; } // uuid
        [Column, Nullable]
        public string Name { get; set; } // text
        [Column, Nullable]
        public string Description { get; set; } // text
        [Column, Nullable]
        public object ExtraInfo { get; set; } // USER-DEFINED

        #region Associations

        /// <summary>
        /// EventPresenter_EventId_fkey
        /// </summary>
        [Association(ThisKey = "EventId", OtherKey = "Id", CanBeNull = false, KeyName = "EventPresenter_EventId_fkey", BackReferenceName = "EventPresenterEventIdfkeys")]
        public Event Event { get; set; }

        #endregion
    }
}