﻿using System;
using LinqToDB.Mapping;

namespace Stacy.DataAccess.Entities
{
    [Table(Schema = "public", Name = "EventLocation")]
    public class EventLocation : IEntity
    {
        [PrimaryKey, NotNull]
        public Guid Id { get; set; } // uuid
        [Column, NotNull]
        public Guid EventId { get; set; } // uuid
        [Column, Nullable]
        public string Country { get; set; } // text
        [Column, Nullable]
        public string City { get; set; } // text
        [Column, Nullable]
        public string FullAddress { get; set; } // text
        [Column, Nullable]
        public string Place { get; set; } // text
        [Column, Nullable]
        public object ExtraInfo { get; set; } // USER-DEFINED

        #region Associations

        /// <summary>
        /// EventLocation_EventId_fkey
        /// </summary>
        [Association(ThisKey = "EventId", OtherKey = "Id", CanBeNull = false, KeyName = "EventLocation_EventId_fkey", BackReferenceName = "EventLocationEventIdfkeys")]
        public Event Event { get; set; }

        #endregion
    }
}