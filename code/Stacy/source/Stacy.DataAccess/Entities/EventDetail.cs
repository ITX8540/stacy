﻿using System;
using LinqToDB.Mapping;

namespace Stacy.DataAccess.Entities
{
    [Table(Schema = "public", Name = "EventDetails")]
    public class EventDetail : IEntity
    {
        [PrimaryKey, NotNull]
        public Guid Id { get; set; } // uuid
        [Column, NotNull]
        public Guid EventId { get; set; } // uuid
        [Column, Nullable]
        public string Name { get; set; } // text
        [Column, Nullable]
        public string Description { get; set; } // text
        [Column, Nullable]
        public string Summary { get; set; } // text
        [Column, Nullable]
        public Guid? EventRoomId { get; set; } // uuid
        [Column, Nullable]
        public Guid? PresenterId { get; set; } // uuid
        [Column, Nullable]
        public DateTime? StartsAt { get; set; } // date
        [Column, Nullable]
        public DateTime? EndsAt { get; set; } // date
        [Column, Nullable]
        public object Tags { get; set; } // ARRAY
        [Column, Nullable]
        public object ExtraInfo { get; set; } // USER-DEFINED

        #region Associations

        /// <summary>
        /// EventDetails_EventId_fkey
        /// </summary>
        [Association(ThisKey = "EventId", OtherKey = "Id", CanBeNull = false, KeyName = "EventDetails_EventId_fkey", BackReferenceName = "EventDetailsEventIdfkeys")]
        public Event Event { get; set; }

        #endregion
    }
}