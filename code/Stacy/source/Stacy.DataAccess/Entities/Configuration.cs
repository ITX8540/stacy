﻿using LinqToDB.Mapping;

namespace Stacy.DataAccess.Entities
{
    [Table(Schema = "public", Name = "Configuration")]
    public class Configuration
    {
        [Column, Nullable]
        public string Key { get; set; } // character varying(100)
        [Column, Nullable]
        public string Value { get; set; } // text
    }
}