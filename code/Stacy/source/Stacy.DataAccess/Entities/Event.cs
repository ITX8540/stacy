﻿using System;
using System.Collections.Generic;
using LinqToDB.Mapping;

namespace Stacy.DataAccess.Entities
{
    [Table(Schema = "public", Name = "Event")]
    public class Event : IEntity
    {
        [PrimaryKey, NotNull]
        public Guid Id { get; set; } // uuid
        [Column, NotNull]
        public Guid EventTypeId { get; set; } // uuid
        [Column, Nullable]
        public Guid? ParentId { get; set; } // uuid

        #region Associations

        /// <summary>
        /// Event_ParentId_fkey
        /// </summary>
        [Association(ThisKey = "ParentId", OtherKey = "Id", CanBeNull = true, KeyName = "Event_ParentId_fkey", BackReferenceName = "Event_ParentId_fkey_BackReferences")]
        public Event Parent { get; set; }

        /// <summary>
        /// Event_EventTypeId_fkey
        /// </summary>
        [Association(ThisKey = "EventTypeId", OtherKey = "Id", CanBeNull = false, KeyName = "Event_EventTypeId_fkey", BackReferenceName = "EventEventTypeIdfkeys")]
        public Classification EventType { get; set; }

        /// <summary>
        /// Event_ParentId_fkey_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "ParentId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<Event> Children { get; set; }

        /// <summary>
        /// EventLocation_EventId_fkey_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "EventId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<EventLocation> EventLocations { get; set; }

        /// <summary>
        /// EventRoom_EventId_fkey_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "EventId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<EventRoom> EventRooms { get; set; }

        /// <summary>
        /// EventDetails_EventId_fkey_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "EventId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<EventDetail> EventDetails { get; set; }

        /// <summary>
        /// EventContact_EventId_fkey_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "EventId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<EventContact> EventContacts { get; set; }

        /// <summary>
        /// EventPresenter_EventId_fkey_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "EventId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<EventPresenter> EventPresenters { get; set; }

        #endregion
    }
}