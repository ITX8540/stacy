﻿using System;
using System.Collections.Generic;
using LinqToDB.Mapping;

namespace Stacy.DataAccess.Entities
{
    [Table(Schema = "public", Name = "ClassificationType")]
    public class ClassificationType : IEntity
    {
        [PrimaryKey, NotNull]
        public Guid Id { get; set; } // uuid
        [Column, Nullable]
        public string Name { get; set; } // text
        [Column, Nullable]
        public string Description { get; set; } // text
        [Column, Nullable]
        public Guid? ParentId { get; set; } // uuid

        #region Associations

        /// <summary>
        /// ClassificationType_ParentId_fkey
        /// </summary>
        [Association(ThisKey = "ParentId", OtherKey = "Id", CanBeNull = true, KeyName = "ClassificationType_ParentId_fkey", BackReferenceName = "ClassificationType_ParentId_fkey_BackReferences")]
        public ClassificationType ParentIdfkey { get; set; }

        /// <summary>
        /// ClassificationType_ParentId_fkey_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "ParentId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<ClassificationType> Children { get; set; }

        /// <summary>
        /// Classification_ClassificationTypeId_fkey_BackReference
        /// </summary>
        [Association(ThisKey = "Id", OtherKey = "ClassificationTypeId", CanBeNull = true, IsBackReference = true)]
        public IEnumerable<Classification> Classifications { get; set; }

        #endregion
    }
}