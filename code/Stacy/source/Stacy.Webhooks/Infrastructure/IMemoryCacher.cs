﻿using System;

namespace Stacy.Webhooks.Infrastructure
{
    public interface IMemoryCacher
    {
        bool Add(string key, object value, DateTimeOffset absExpiration);
        void Delete(string key);
        object GetValue(string key);
    }
}