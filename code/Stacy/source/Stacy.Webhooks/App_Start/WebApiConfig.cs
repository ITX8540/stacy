﻿using System.Web.Http;

namespace Stacy.Webhooks
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Write web api requests/responses to trace.
            //config.EnableSystemDiagnosticsTracing();

            // Web API routes.
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
