﻿using System;
using System.Threading.Tasks;
using Stacy.Core.DTOs.ApiAi;

namespace Stacy.Webhooks.Models
{
    public class ApiAiRequestMatch
    {
        public ApiAiRequestMatch(
            Func<ApiAiRequest, bool> predicate, 
            Func<ApiAiRequest, Task<ApiAiResponse>> map)
        {
            Predicate = predicate;
            MapAsync = map;
        }

        public ApiAiRequestMatch(
            Func<ApiAiRequest, bool> predicate,
            Func<ApiAiRequest, ApiAiResponse> map)
        {
            Predicate = predicate;
            MapAsync = async input => map(input);
        }

        public Func<ApiAiRequest, bool> Predicate { get; }
        public Func<ApiAiRequest, Task<ApiAiResponse>> MapAsync { get; }
    }
}