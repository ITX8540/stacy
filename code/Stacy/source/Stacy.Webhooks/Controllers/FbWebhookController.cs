﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using MediatR;
using Newtonsoft.Json;
using Stacy.Core;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain.Commands;
using Stacy.Domain.Events;
using Stacy.Webhooks.Infrastructure;

namespace Stacy.Webhooks.Controllers
{
    public class FbWebhookController : ApiController
    {
        private const string ValidationToken = "test";


        private readonly IMediator _mediator;
        private readonly IMemoryCacher _memoryCacher;
        public FbWebhookController(IMediator mediator, IMemoryCacher memoryCacher)
        {
            _mediator = mediator;
            _memoryCacher = memoryCacher;
        }

        public long Get()
        {
            var queryParams = Request.GetQueryNameValuePairs()
                .ToDictionary(x => x.Key, x => x.Value);

            string mode, verifyToken;
            queryParams.TryGetValue("hub.mode", out mode);
            queryParams.TryGetValue("hub.verify_token", out verifyToken);

            if (mode == "subscribe" && verifyToken == ValidationToken)
                return long.Parse(queryParams["hub.challenge"]);

            throw new HttpResponseException(HttpStatusCode.Forbidden);
        }


        public async Task Post(FbCallback data)
        {
            try
            {
                Log.TraceEntry(data);
                var requestId = Guid.NewGuid();
                var requestTime = DateTimeOffset.Now;

                await _mediator.Publish(new FacebookCallbackReceived(requestId, data, 0, requestTime));
                // Make sure this is a page subscription.
                if (data.Object == "page")
                {
                    // Iterate over each entry.
                    // There may be multiple if batched.
                    foreach (var entry in data.Entry)
                    {
                        // Iterate over each messaging @event.
                        foreach (var messagingEvent in entry.Messaging)
                        {
                            var getUserFacebookInfo = new GetUserFacebookInfo(messagingEvent.Sender.Id,
                                StacyConfiguration.FbPageAccessToken, requestId);
                            var userInfo = await _mediator.Send(getUserFacebookInfo);

                            if (messagingEvent.Message != null)
                            {
                                string sessionId = string.Empty;
                                var value = _memoryCacher.GetValue(messagingEvent.Sender.Id);
                                if (value != null)
                                {
                                    sessionId = value.ToString();
                                }


                                var response =
                                    await
                                        _mediator.Send(new ProcessReceivedMessage<FbMessaging>(messagingEvent,
                                            userInfo.UserId, sessionId, requestId));

                                sessionId = response.ApiSessionId;

                                await _mediator.Publish(response);

                                _memoryCacher.Add(messagingEvent.Sender.Id, sessionId, DateTimeOffset.UtcNow.AddHours(1));
                                return;
                            }
                            if (messagingEvent.Postback != null)
                            {
                                string sessionId = string.Empty;
                                var value = _memoryCacher.GetValue(messagingEvent.Sender.Id);
                                if (value != null)
                                {
                                    sessionId = value.ToString();
                                }

                                sessionId =
                                    await
                                        _mediator.Send(new ProcessReceivedPostBack<FbMessaging>(requestId,messagingEvent,sessionId));
                                _memoryCacher.Add(messagingEvent.Sender.Id, sessionId, DateTimeOffset.UtcNow.AddHours(1));
                                return;
                            }
                            if (messagingEvent.Delivery != null)
                            {
                                var messageDelivered = new FacebookMessageDelivered(userInfo.UserId,messagingEvent.Delivery.MessageIds,messagingEvent.Delivery.Watermark);
                                await _mediator.Publish(messageDelivered);
                                return;
                            }
                            if (messagingEvent.Read != null)
                            {
                                var messageDelivered = new FacebookMessageRead(userInfo.UserId, messagingEvent.Read.Watermark);
                                await _mediator.Publish(messageDelivered);
                                return;
                            }
                            Log.Info("Error");
                            Log.Info(JsonConvert.SerializeObject(data));
                            throw new HttpResponseException(HttpStatusCode.OK);
                        }
                    }
                }
                Log.TraceExit();


                // An http code 200 must be sent within 20 seconds to let Facebook know we"ve 
                // successfully received the callback. Otherwise, the request will time out.
            }
            catch (Exception ex)
            {
                Log.TraceCatch(ex);
                //try
                //{
                //    var message = await _mediator.Send(new SendTextResponseToFacebook(data.Entry[0].Messaging[0].Sender.Id, ex.Message));
                //    await
                //        _mediator.Send(new SendMessageToApi<FbMessaging>(message, StacyConfiguration.FbMessageEndpoint));
                //}
                //catch (Exception exc)
                //{
                //    Log.TraceCatch(exc);
                //    throw;
                //}
                throw new HttpResponseException(HttpStatusCode.OK);

            }
        }
    }
}
