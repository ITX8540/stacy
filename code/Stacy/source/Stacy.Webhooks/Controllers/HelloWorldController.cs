﻿using System.Web.Http;

namespace Stacy.Webhooks.Controllers
{
    public class HelloWorldController : ApiController
    {        
        // GET api/HelloWorld
        public string Get()
        {
            return "hello world";
        }
    }
}
