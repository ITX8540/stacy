﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Stacy.Core;
using Stacy.Core.DTOs.ApiAi;
using Stacy.Core.DTOs.Facebook;
using Stacy.Core.DTOs.Google;
using Stacy.Webhooks.Models;
using Stacy.DataAccess.Infrastructure;
using Stacy.DataAccess.Entities;

namespace Stacy.Webhooks.Controllers
{
    public class ApiAiWebhookController : ApiController
    {
        private const string ResponseSource = "Stacy Web API";
        private const string ResponseDisplayText = "Display text from Stacy Web API";

        // Predicate + map pairs.
        private static readonly ApiAiRequestMatch[] ResponseMatches =
        {            
            new Models.ApiAiRequestMatch(
                // If input is asking for location.
                input => input.Result.Action.ToString().Equals(StacyContstants.ApiAiActions.FindLocation, StringComparison.OrdinalIgnoreCase),
                // Respond with google map link.
                async input => FBResponse(await GetFBMessageForGoogleMapsLinkAsync(input))),

            new ApiAiRequestMatch(
                // If input is "C#".
                input => input.Result.ResolvedQuery.Equals("C#", StringComparison.OrdinalIgnoreCase),
                // Respond with the following text.
                _ => TextResponse("... > Java")),

            new ApiAiRequestMatch(
                input => input.Result.Action.ToString().Equals(StacyContstants.ApiAiActions.NextEvents, StringComparison.OrdinalIgnoreCase),
                async input => FBResponse(await GetFBMessageForNextEvents())),
            
            new ApiAiRequestMatch(
                input => input.Result.Action.ToString().Equals(StacyContstants.ApiAiActions.FindPresenters, StringComparison.OrdinalIgnoreCase),
                async input => FBResponse(await GetFBMessageForPresenters())),

            new ApiAiRequestMatch(
                input => input.Result.Action.ToString().Equals(StacyContstants.ApiAiActions.ContactInfo, StringComparison.OrdinalIgnoreCase),
                async input => FBResponse(await GetFBMessageForContactInfo())),

            new ApiAiRequestMatch(
                input => input.Result.Action.ToString().Equals(StacyContstants.ApiAiActions.EventsOfPresenter, StringComparison.OrdinalIgnoreCase),
                async input => FBResponse(await GetFBMessageForEventsOfPresenter(input))),

            new ApiAiRequestMatch(
                // If action is respondPostbackText then respond hidden text which was inside json.
                input => input.Result.Action.ToString().Equals(StacyContstants.ApiAiActions.RespondPostbackText, StringComparison.OrdinalIgnoreCase),
                // Respond with the following text.
                async input => TextResponse(await GetPostBackMessage(input))),
            
            new ApiAiRequestMatch(
                // If input is integer.
                input => input.Result.ResolvedQuery.All(char.IsDigit),
                // Respond with the integer inc by 1.
                input => TextResponse((int.Parse(input.Result.ResolvedQuery) + 1).ToString()))

        };

        /// <summary>
        /// Webhook for receiving messages from api.ai.
        /// NB! Response must be sent within 5 seconds or the request will time out.
        /// </summary>
        public async Task<JsonResult<ApiAiResponse>> Post(ApiAiRequest request)
        {
            try
            {                
                Log.TraceEntry(request);

                ApiAiRequestMatch match = ResponseMatches.FirstOrDefault(m => m.Predicate(request));
                ApiAiResponse response = match != null 
                    ? await match.MapAsync(request) 
                    : TextResponse(request.Result.Fulfillment.Speech);

                Log.TraceExit(response);
                return Json(response);                
            }
            catch (Exception ex)
            {
                Log.TraceCatch(ex);
                throw;
            }
        }

        private static ApiAiResponse TextResponse(string text)
        {
            if (text.Length == 0) { text = "Sorry, I’m afraid I don’t follow you.."; }

            return new ApiAiResponse
            {
                DisplayText = ResponseDisplayText,
                Source = ResponseSource,
                Speech = text
            };
        }

        private static ApiAiResponse FBResponse(FbMessage message)
        {
            return new ApiAiResponse
            {
                DisplayText = ResponseDisplayText,
                Source = ResponseSource,
                Data = new Dictionary<string, object>
                {
                    ["facebook"] = message
                }
            };
        }

        

        
        private static async Task<FbMessage> GetFBMessageForNextEvents()
        {
            return FacebookExtensions.GetComposedFbMessage(GetEventMessages(true));
        }
        private static async Task<FbMessage> GetFBMessageForEventsOfPresenter(ApiAiRequest input)
        {
            dynamic json = JsonConvert.DeserializeObject(input.Result.ResolvedQuery);
            string presenterId = json.presenterId;

            return FacebookExtensions.GetComposedFbMessage(GetEventMessages(false, presenterId));
        }

        private static FbAttachmentPayloadElement[] GetEventMessages(Boolean onlyFutureOnes = false, string presenterId = null)
        {
            FbAttachmentPayloadElement[] eventMessages = null;

            using (var db = new StacyDatabase())
            {
                var presentersRepo = new GenericRepository<EventPresenter>(db);
                var roomsRepo = new GenericRepository<EventRoom>(db);
                var eventDetailsRepo = new GenericRepository<EventDetail>(db);
                var eventRepo = new EventRepository(db);


                List<Guid> eventIds;

                //TODO: review this logic to be more dynamic query
                if (presenterId != null && onlyFutureOnes == false)
                {
                    eventIds = eventDetailsRepo.Get(x => x.PresenterId == new Guid(presenterId)).Select(x => x.EventId).ToList();
                }
                else if (presenterId != null && onlyFutureOnes == true)
                {
                    eventIds = eventDetailsRepo.Get(x => x.PresenterId == new Guid(presenterId) && x.EndsAt >= DateTime.Now).Select(x => x.EventId).ToList();
                }
                else if (presenterId == null && onlyFutureOnes == true)
                {
                    eventIds = eventDetailsRepo.Get(x => x.EndsAt >= DateTime.Now).Select(x => x.EventId).ToList();
                } else
                {
                    eventIds = eventDetailsRepo.Get().Select(x => x.EventId).ToList();
                }
                
                var nextEventsData = eventRepo.Get(x => x.ParentId != null && eventIds.Contains(x.Id)).OrderBy(x => x.EventDetails.FirstOrDefault().StartsAt).ToList();

                eventMessages = new FbAttachmentPayloadElement[nextEventsData.Count()];
                foreach (var _event in nextEventsData)
                {
                    EventDetail eventDetail = _event.EventDetails.FirstOrDefault();
                    var eventPresenters = presentersRepo.Get(x => x.Id == eventDetail.PresenterId);
                    EventPresenter eventPresenter = eventPresenters.FirstOrDefault();
                    EventRoom eventRoom = roomsRepo.Get(x => x.Id == eventDetail.EventRoomId).FirstOrDefault();
                        
                    FbAttachmentPayloadElement eventMessage = ComposePayloadElementForEvent(_event, eventDetail, eventPresenter, eventRoom);

                    eventMessages[nextEventsData.IndexOf(_event)] = eventMessage;
                }
            }

            return eventMessages;
        }

        private static FbAttachmentPayloadElement ComposePayloadElementForEvent(Event _event, EventDetail _eventDetail, EventPresenter _eventPresenter, EventRoom _room)
        {
            string title = _eventDetail.Name;
            if (!string.IsNullOrEmpty(_eventDetail.Description))
            {
                title = title + " (" + _event.EventType.Description + ")";
            }

            string startsTime = _eventDetail.StartsAt.Value.ToShortTimeString();
            string endsTime = _eventDetail.EndsAt.Value.ToShortTimeString();
            string date = _eventDetail.StartsAt.Value.ToShortDateString();

            string roomNameText = "";
            if (_room != null) {
                string roomName = _room.Name;
                roomNameText = " Room: " + roomName + ".";
            }

            string subtitle = "Time: " + startsTime + "-" + endsTime + " on " + date + "." + roomNameText;

            List<FbButton> buttonsList = new List<FbButton>();

            if (!string.IsNullOrEmpty(_eventDetail.Description))
            {
                string buttonDescriptionTitle = "Description ?";
                string buttonDescriptionPayload = "{ \"postbackHiddenText\" : \"\" , \"responseText\" : \"Description: " + _eventDetail.Description + "\"}"; ;
                buttonsList.Add(new FbButton
                {
                    Type = "postback",
                    Title = buttonDescriptionTitle,
                    Payload = buttonDescriptionPayload
                });
            }

            if (_eventPresenter != null)
            {
                string buttonPresenterTitle = _eventPresenter.Name + " ?";
                string buttonPresenterPayload = "{ \"postbackHiddenText\" : \"\" , \"responseText\" : \"" + _eventPresenter.Name + " - " + _eventPresenter.Description + "\"}";
                buttonsList.Add(new FbButton
                {
                    Type = "postback",
                    Title = buttonPresenterTitle,
                    Payload = buttonPresenterPayload
                });
            }

            FbButton[] buttons = null;
            if (buttonsList.Any())
            {
                buttons = new FbButton[buttonsList.Count()];
                buttons = buttonsList.ToArray();
            }

            return new FbAttachmentPayloadElement
            {
                Title = title,
                Subtitle = subtitle,
                Buttons = buttons
            };
        }


        private static async Task<FbMessage> GetFBMessageForPresenters()
        {
            return FacebookExtensions.GetComposedFbMessage(GetPresentersMessages());
        }

        private static FbAttachmentPayloadElement[] GetPresentersMessages()
        {
            FbAttachmentPayloadElement[] presenterMessages = null;

            using (var db = new StacyDatabase())
            {
                var presentersRepo = new GenericRepository<EventPresenter>(db);
                var roomsRepo = new GenericRepository<EventRoom>(db);
                var eventDetailsRepo = new GenericRepository<EventDetail>(db);
                var eventRepo = new EventRepository(db);

                var presentersData = presentersRepo.Get().ToList();

                presenterMessages = new FbAttachmentPayloadElement[presentersData.Count()];
                foreach (var _presenter in presentersData)
                {
                    string title = _presenter.Name;
                    string subtitle = "";

                    if (!string.IsNullOrEmpty(_presenter.Description))
                    {
                        subtitle = _presenter.Description;
                    }

                   List<FbButton> buttonsList = new List<FbButton>();

                    if (presenterHasEvents(_presenter.Id))
                    {
                        string buttonEventsOfPresenter = "Which events ?";
                        string buttonEventsOfPresenterPayload = "{ \"postbackPresenterEvents\" : \"\" , \"presenterId\" : \"" + _presenter.Id + "\"}";
                        buttonsList.Add(new FbButton
                        {
                            Type = "postback",
                            Title = buttonEventsOfPresenter,
                            Payload = buttonEventsOfPresenterPayload
                        });
                    }

                    if (!string.IsNullOrEmpty(_presenter.Name))
                    {
                        string linkSearchTitle = "More info from web ?";
                        string searchUrl = "http://www.google.com/search?q=" + _presenter.Name.Replace(" ", "+");
                        buttonsList.Add(new FbButton
                        {
                            Type = "web_url",
                            Title = linkSearchTitle,
                            Url = searchUrl
                        });
                    }

                    FbButton[] buttons = null;
                    if (buttonsList.Any())
                    {
                        buttons = new FbButton[buttonsList.Count()];
                        buttons = buttonsList.ToArray();
                    }

                    FbAttachmentPayloadElement eventMessage = new FbAttachmentPayloadElement
                    {
                        Title = title,
                        Subtitle = subtitle,
                        Buttons = buttons
                    };
                    presenterMessages[presentersData.IndexOf(_presenter)] = eventMessage;
                }
            }

            return presenterMessages;
        }

        private static Boolean presenterHasEvents(Guid presenter_id)
        {
            using (var db = new StacyDatabase())
            {
                var eventDetailsRepo = new GenericRepository<EventDetail>(db);
                
                foreach (var _eventDetail in eventDetailsRepo.Get())
                {
                    if (_eventDetail.PresenterId == presenter_id) { return true; }
                }
            }

            return false;
        }

        private static async Task<FbMessage> GetFBMessageForContactInfo()
        {
            using (var db = new StacyDatabase())
            {
                var eventsRepo = new EventRepository(db);
                var contactsRepo = new GenericRepository<EventContact>(db);
                var conference = eventsRepo.Get(x => x.ParentId == null).FirstOrDefault();
                var contactInfo = contactsRepo.Get(x => x.EventId == conference.Id).FirstOrDefault();


                string title = conference.EventDetails.FirstOrDefault().Name;
                string subtitle = "Phone: " + contactInfo.PhoneNumber + ". E-mail: " + contactInfo.Email;

                string linkSearchTitle = "Website";
                string searchUrl = contactInfo.Website;

                FbAttachmentPayloadElement contactInfoPayload = new FbAttachmentPayloadElement
                {
                    Title = title,
                    Subtitle = subtitle,
                    Buttons = new[]
                                {
                                    new FbButton
                                    {
                                        Type = "web_url",
                                        Title = linkSearchTitle,
                                        Url = searchUrl
                                    }
                                }
                };
                return FacebookExtensions.GetComposedFbMessage(new[] { contactInfoPayload });
            }
        }



        private static async Task<String> GetPostBackMessage(ApiAiRequest input)
        {
            dynamic json = JsonConvert.DeserializeObject(input.Result.ResolvedQuery);
            string responseText = json.responseText;
            return responseText;
        }

        private static async Task<FbMessage> GetFBMessageForGoogleMapsLinkAsync(ApiAiRequest input)
        {
            const int mapPreviewZoom = 17;

            var parameters = input.Result.Contexts[0].Parameters;
            string address;

            using (var db = new StacyDatabase())
            {
                var eventsRepo = new GenericRepository<Event>(db);
                var locationsRepo = new GenericRepository<EventLocation>(db);
                var conferenceId = eventsRepo.Get(x => x.ParentId == null).FirstOrDefault().Id;
                address = locationsRepo.Get(x => x.EventId == conferenceId).FirstOrDefault().FullAddress;
            }

            var location = await GetGoogleMapsLocationForAddressAsync(address);

            return FacebookExtensions.GetComposedFbMessage(new[]
                        {
                            new FbAttachmentPayloadElement
                            {
                                Title = parameters["eventName"],
                                Subtitle = address,
                                ImageUrl = $"https://maps.googleapis.com/maps/api/staticmap?size=764x400&center={location.Lat},{location.Lng}&zoom={mapPreviewZoom}&markers={location.Lat},{location.Lng}",
                                ItemUrl = $"https://www.google.com/maps/place/{HttpUtility.UrlEncode(address)}",
                            }
                        });
        }

        private static async Task<GoogleMapsLocation> GetGoogleMapsLocationForAddressAsync(string address)
        {
            using (var client = new HttpClient())
            {
                string resultString =  await client.GetStringAsync(
                    $"https://maps.googleapis.com/maps/api/geocode/json?address={HttpUtility.UrlEncode(address)}");
                return JsonConvert.DeserializeObject<GoogleMapsResponse>(resultString).Results[0].Geometry.Location;
            }
        }
    }
}
