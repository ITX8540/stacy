﻿using System;
using System.Threading.Tasks;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain;

namespace Stacy.Infrastructure
{
    public class ApiAiRequestMatch
    {
        public ApiAiRequestMatch(
            Func<ProcessedFacebookMessage, bool> predicate,
            Func<ProcessedFacebookMessage, Task<FbMessaging>> map)
        {
            Predicate = predicate;
            MapAsync = map;
        }

        public ApiAiRequestMatch(
            Func<ProcessedFacebookMessage, bool> predicate,
            Func<ProcessedFacebookMessage, FbMessaging> map)
        {
            Predicate = predicate;
            MapAsync = async input => map(input);
        }

        public Func<ProcessedFacebookMessage, bool> Predicate { get; }
        public Func<ProcessedFacebookMessage, Task<FbMessaging>> MapAsync { get; }
    }
}
