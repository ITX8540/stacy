﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Marten;
using MediatR;
using Stacy.Core;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain.Commands;
using Stacy.Domain.Events;

namespace Stacy.Infrastructure.Handlers.Notifications
{
    public class FeedbackHandler : INotificationHandler<MessageProcessed>
    {
        private const int SecondsUntilFeedback = 5 * 60; // 5 min after last msg.

        // TODO: Use cancellation tokens instead.
        private static readonly ConcurrentDictionary<string, Dictionary<Guid, bool>> _pendingTaskCancellations =
            new ConcurrentDictionary<string, Dictionary<Guid, bool>>();

        private const string FeedbackMessage =
            "Thank you for using Stacy! Your feedback is highly appreciated and will help us to improve our ability to serve you and other users. To provide feedback, simply respond to this message. :)";

        private readonly IMediator _mediator;

        public FeedbackHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async void Handle(MessageProcessed notification)
        {
            Log.TraceEntry(notification);
            try
            {
                string senderId = notification.IncomingMessage.Sender.Id;

                // Load existing or create new feedback state for user.
                FeedbackContext ctx;
                var store = DocumentStore.For(StacyConfiguration.StacyDocumentStoreConnectionString);                
                using (var session = store.LightweightSession())
                {
                    ctx = await session.LoadAsync<FeedbackContext>(senderId);
                    if (ctx == null)
                    {
                        ctx = new FeedbackContext { Id = senderId };
                        session.Store(ctx);
                        await session.SaveChangesAsync();
                    }
                }

                // Only proceed if feedback request has not been sent to the user already.
                if (!ctx.Sent)
                {
                    // Get ahold of or create a new map of task cancellation tokens.
                    // Since we only want to send feedback request X time after LAST message from user,
                    // we need a way to cancel previous scheduled tasks for feedback request.
                    var userPendingTaskCancellations = _pendingTaskCancellations.GetOrAdd(senderId, new Dictionary<Guid, bool>());

                    // Cancel all existing pending tasks.
                    var keys = userPendingTaskCancellations.Keys.ToArray();
                    foreach (var key in keys)
                        userPendingTaskCancellations[key] = true;

                    // Schedule a new task.
                    Guid taskId = Guid.NewGuid();
                    userPendingTaskCancellations.Add(taskId, false);
                    Log.Info("Scheduling feedback task ...");
                    Task.Run(() => ScheduleSendFbMessageAsync(senderId, taskId));
                }
            }
            catch (Exception ex)
            {
                Log.TraceCatch(ex);
                throw;
            }
            Log.TraceExit();
        }

        private async Task ScheduleSendFbMessageAsync(string senderId, Guid taskId)
        {
            Log.TraceEntry(new { senderId, taskId});
            try
            {
                await Task.Delay(SecondsUntilFeedback * 1000);

                var userPendingTaskCancellations = _pendingTaskCancellations[senderId];

                bool cancel = userPendingTaskCancellations[taskId];
                userPendingTaskCancellations.Remove(taskId);

                if (!cancel)
                {
                    var store = DocumentStore.For(StacyConfiguration.StacyDocumentStoreConnectionString);
                    using (var session = store.LightweightSession())
                    {
                        FeedbackContext ctx = await session.LoadAsync<FeedbackContext>(senderId);
                        if (!ctx.Sent)
                        {
                            var result = await _mediator.Send(
                                new SendTextResponseToFacebook(Guid.NewGuid(), ctx.Id, FeedbackMessage));
                            var sendToApiRequest = new SendMessageToApi<FbMessaging>(result,
                                StacyConfiguration.FbMessageEndpoint);
                            await _mediator.Send(sendToApiRequest);

                            ctx.Sent = true;

                            session.Store(ctx);
                            await session.SaveChangesAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.TraceCatch(ex);
                throw;
            }
            Log.TraceExit();
        }        
    }

    public class FeedbackContext
    {
        public string Id { get; set; } // Facebook user page id.
        public bool Sent { get; set; }
    }
}
