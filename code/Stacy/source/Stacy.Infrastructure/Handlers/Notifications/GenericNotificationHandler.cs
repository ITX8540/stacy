﻿using Marten;
using MediatR;
using Stacy.Core;
using Stacy.Domain.Events;

namespace Stacy.Infrastructure.Handlers.Notifications
{
    public class GenericNotificationHandler : INotificationHandler<FacebookMessageRead>
        , INotificationHandler<FacebookMessageDelivered>
        , INotificationHandler<FacebookCallbackReceived>
        , INotificationHandler<MessageProcessed>
    {
        private readonly DocumentStore _documentStore;
        public GenericNotificationHandler()
        {
            _documentStore = DocumentStore.For(StacyConfiguration.StacyDocumentStoreConnectionString);
        }
        public async void Handle(FacebookMessageRead notification)
        {
           
            using (var session = _documentStore.LightweightSession())
            {
                session.Store(notification);
                await session.SaveChangesAsync();
            }
        }

        public async void Handle(FacebookMessageDelivered notification)
        {
            using (var session = _documentStore.LightweightSession())
            {
                session.Store(notification);
                await session.SaveChangesAsync();
            }
        }

        public async void Handle(FacebookCallbackReceived notification)
        {
            using (var session = _documentStore.LightweightSession())
            {
                session.Store(notification);
                await session.SaveChangesAsync();
            }
        }

        public async void Handle(MessageProcessed notification)
        {
            using (var session = _documentStore.LightweightSession())
            {
                session.Store(notification);
                await session.SaveChangesAsync();
            }
        }
    }
}
