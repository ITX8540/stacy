﻿using System;
using Marten;
using MediatR;
using Stacy.Core;
using Stacy.Domain.Commands;
using Stacy.Domain.Events.Errors;

namespace Stacy.Infrastructure.Handlers.Notifications
{
    public class MessageProcessingFailedHandler : INotificationHandler<MessageProcessingFailed>
    {
        private readonly IMediator _mediator;

        public MessageProcessingFailedHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async void Handle(MessageProcessingFailed notification)
        {
            var store = DocumentStore.For(StacyConfiguration.StacyDocumentStoreConnectionString);
            bool doNotSendMessage = false;
            using (var session = store.LightweightSession())
            {
                doNotSendMessage = await session.Query<MessageProcessingFailed>().AnyAsync(
                                x => 
                                    x.ReceipientId == notification.ReceipientId 
                                    && x.TimeStamp > DateTimeOffset.Now.AddSeconds(-10));
                session.Store(notification);
                await session.SaveChangesAsync();
            }
            if (!doNotSendMessage)
            {
                await  _mediator.Send(new SendProcessingFailedMessage(notification.RequestId,notification.ReceipientId));
            }
        }
    }
}
