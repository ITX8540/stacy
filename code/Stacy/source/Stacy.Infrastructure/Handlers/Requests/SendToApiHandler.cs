﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using MediatR;
using Stacy.Core;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain.Commands;
using Stacy.Domain.Events;

namespace Stacy.Infrastructure.Handlers.Requests
{
    public class SendToApiHandler : IAsyncRequestHandler<SendMessageToApi<FbMessaging>, MessagePostedToApi<FbMessaging>>
                                    , IAsyncRequestHandler<SendActionToApi<FbAction>, MessagePostedToApi<FbAction>>
    {
        private readonly IMediator _mediator;

        public SendToApiHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<MessagePostedToApi<FbAction>> Handle(SendActionToApi<FbAction> message)
        {
            using (var client = new HttpClient())
            {
                Log.TraceEntry();

                HttpResponseMessage response = await client.PostAsJsonAsync(message.Endpoint, message.Action);
                var messagePosted = new MessagePostedToApi<FbAction>(message.Action, response, DateTimeOffset.Now);
                LogResponseMessage(response);

                Log.TraceExit();
                return messagePosted;

            }
        }

        public async Task<MessagePostedToApi<FbMessaging>> Handle(SendMessageToApi<FbMessaging> message)
        {
            using (var client = new HttpClient())
            {
                Log.TraceEntry();

                HttpResponseMessage response = await client.PostAsJsonAsync(message.Endpoint,message.Messaging);
                var messagePosted = new MessagePostedToApi<FbMessaging>(message.Messaging, response, DateTimeOffset.Now);
                LogResponseMessage(response);

                Log.TraceExit();
                return messagePosted;
                
            }
        }

        private void LogResponseMessage(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                Log.Info("Message sent");
            }
            else
            {                
                Log.Warning($"Message not sent: {response.ReasonPhrase}\n{response.RequestMessage}");
            }
        }
    }
}
