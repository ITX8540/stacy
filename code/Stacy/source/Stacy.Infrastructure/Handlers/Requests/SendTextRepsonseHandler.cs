﻿using System.Threading.Tasks;
using MediatR;
using Stacy.Core;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain.Commands;

namespace Stacy.Infrastructure.Handlers.Requests
{
    public class SendTextRepsonseHandler : IAsyncRequestHandler<SendTextResponseToFacebook, FbMessaging>
    {
        private readonly IMediator _mediator;
        public SendTextRepsonseHandler(IMediator mediator)
        {
            _mediator = mediator;
        }
        public async Task<FbMessaging> Handle(SendTextResponseToFacebook message)
        {
            Log.TraceEntry();
            var messaging = new FbMessaging
            {
                Recipient = new FbRecipient { Id = message.RecepientId },
                Message = new FbMessage { Text = message.Reponse }
            };

            return messaging;
        }
    }
}
