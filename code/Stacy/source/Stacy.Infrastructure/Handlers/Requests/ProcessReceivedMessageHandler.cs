﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Newtonsoft.Json;
using Stacy.Core;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain;
using Stacy.Domain.Commands;
using Stacy.Domain.Events;
using Stacy.Domain.Events.Errors;

namespace Stacy.Infrastructure.Handlers.Requests
{
    public class ProcessReceivedMessageHandler : IAsyncRequestHandler<ProcessReceivedMessage<FbMessaging>, MessageProcessed>, IAsyncRequestHandler<ProcessReceivedPostBack<FbMessaging>, string>
    {
        private readonly IMediator _mediator;
        private readonly ApiAiRequestMatch[] _responseMatches;
        public ProcessReceivedMessageHandler(IMediator mediator)
        {
            _mediator = mediator;
            _responseMatches = GetResponseMatches();
        }

        
        public async Task<MessageProcessed> Handle(ProcessReceivedMessage<FbMessaging> receivedMessage)
        {
            try
            {
                Log.TraceEntry();
                var senderId = receivedMessage.IncomingMessage.Sender.Id;
                var recipientId = receivedMessage.IncomingMessage.Recipient.Id;
                var timeOfMessage = receivedMessage.IncomingMessage.Timestamp;
                var message = receivedMessage.IncomingMessage.Message;

                Log.Info($"Received message for user {senderId} and page {recipientId} at {timeOfMessage} with message:");

                Log.Info(JsonConvert.SerializeObject(message));
                await _mediator.Send(new SendTypingOnAction(receivedMessage.RequestId, senderId));
                if (message.Text != null)
                {
                    var apiAiHandler = new AiApiHandler(message.Text.ToString(), receivedMessage.ApiAiSessionId);

                    var response = await apiAiHandler.GetResponseAsync();

                    if (!response.IsError)
                    {
                        var processedMessage = new ProcessedFacebookMessage(receivedMessage.RequestId,response, receivedMessage.IncomingMessage);
                        ApiAiRequestMatch match = _responseMatches.FirstOrDefault(m => m.Predicate(processedMessage));
                        if (match != null)
                        {
                            var messageResult = await match.MapAsync(processedMessage);
                            var sendToApiRequest = new SendMessageToApi<FbMessaging>(messageResult, StacyConfiguration.FbMessageEndpoint);
                            await _mediator.Send(sendToApiRequest);
                            return new MessageProcessed(receivedMessage.RequestId,receivedMessage.UserId,response,receivedMessage.IncomingMessage, receivedMessage.TimeStamp);
                        }
                        var responseText = response.Result.Fulfillment.Speech;
                        if (string.IsNullOrWhiteSpace(responseText))
                        {
                            responseText = "Can't help you with that! Keep to the script!";
                        }

                        await _mediator.Send(new SendTypingOffAction(receivedMessage.RequestId, senderId));

                        await SendTextResponseAsync(receivedMessage.RequestId,senderId, responseText);

                        return new MessageProcessed(receivedMessage.RequestId, string.Empty, response, receivedMessage.IncomingMessage, receivedMessage.TimeStamp);
                    }
                }

                // #100 Leaving it in for future expansions
                //if (message.Attachments != null && message.Attachments.Length > 0)
                //{
                //    await _mediator.Send(new SendTypingOffAction(receivedMessage.RequestId, senderId));
                //    //await SendTextResponseAsync(receivedMessage.RequestId,senderId, "Message with attachment received");
                //}
            }
            catch (Exception exception)
            {
                Log.TraceCatch(ex:exception);
                var failed = new MessageProcessingFailed(receivedMessage.RequestId, receivedMessage.IncomingMessage.Sender.Id, JsonConvert.SerializeObject(exception));
                await _mediator.Publish(failed);
            }
            Log.TraceExit();
            return new MessageProcessed(receivedMessage.RequestId, string.Empty, null, receivedMessage.IncomingMessage,receivedMessage.TimeStamp);
        }
        public async Task<string> Handle(ProcessReceivedPostBack<FbMessaging> message)
        {
            var postBackMessage = GetPostBackMessage(message.IncomingPostBack.Postback);
            var receipientId = message.IncomingPostBack.Sender.Id;
            if (string.IsNullOrEmpty(postBackMessage))
            {
                await _mediator.Send(new SendTypingOnAction(message.RequestId, receipientId));
                var postbackPayload = message.IncomingPostBack.Postback.Payload;
                var apiAiHandler = new AiApiHandler(postbackPayload, message.ApiAiSessionId);

                var response = await apiAiHandler.GetResponseAsync();
                if (!response.IsError)
                {
                    var processedMessage = new ProcessedFacebookMessage(message.RequestId, response, message.IncomingPostBack);
                    ApiAiRequestMatch match = _responseMatches.FirstOrDefault(m => m.Predicate(processedMessage));
                    if (match != null)
                    {
                        
                        var messageResult = await match.MapAsync(processedMessage);
                        await _mediator.Send(new SendTypingOffAction(message.RequestId, receipientId));
                        var sendToApiRequest = new SendMessageToApi<FbMessaging>(messageResult, StacyConfiguration.FbMessageEndpoint);

                        await _mediator.Send(sendToApiRequest);
                        
                        return message.ApiAiSessionId;
                    }
                }
            }
            await SendTextResponseAsync(message.RequestId,receipientId, postBackMessage);
            return message.ApiAiSessionId;

        }
        private async Task SendTextResponseAsync(Guid requestId,string senderId, string response)
        {
            var result = await _mediator.Send(new SendTextResponseToFacebook(requestId,senderId, response));
            var sendToApiRequest = new SendMessageToApi<FbMessaging>(result,StacyConfiguration.FbMessageEndpoint);
            await _mediator.Send(sendToApiRequest);
        }

        private ApiAiRequestMatch[] GetResponseMatches()
        {
            return new[]
            {
                new ApiAiRequestMatch(
                    input => input.AiResponse.Result.Action.Equals(StacyContstants.ApiAiActions.FindLocation, StringComparison.OrdinalIgnoreCase),
                    async input => await _mediator.Send(new GetGoogleMapsLinkMessage(input))),
                new ApiAiRequestMatch(input => input.AiResponse.Result.Action.Equals("C#", StringComparison.OrdinalIgnoreCase),
                    async input => await _mediator.Send(new SendTextResponseToFacebook(input.RequestId,input.Messaging.Sender.Id,"... > Java"))),
                new ApiAiRequestMatch(
                    input =>  input.AiResponse.Result.Action.Equals(StacyContstants.ApiAiActions.NextEvents, StringComparison.OrdinalIgnoreCase),
                    async input => await _mediator.Send(new GetNextEventsMessage(input))
                ),
                new ApiAiRequestMatch(
                    input =>  input.AiResponse.Result.Action.Equals(StacyContstants.ApiAiActions.FindPresenters, StringComparison.OrdinalIgnoreCase),
                    async input => await _mediator.Send(new GetPresentersMessage(input))
                ),
                new ApiAiRequestMatch(
                    input => input.AiResponse.Result.Action.Equals(StacyContstants.ApiAiActions.ContactInfo, StringComparison.OrdinalIgnoreCase),
                    async input => await _mediator.Send( new GetFbMessageForContactInfo(input))
                ),
                new ApiAiRequestMatch(
                    input => input.AiResponse.Result.Action.Equals(StacyContstants.ApiAiActions.EventsOfPresenter, StringComparison.OrdinalIgnoreCase),
                    async input => await _mediator.Send( new GetFbMessageForEventsOfPresenter(input))
                ),
                new ApiAiRequestMatch(
                    input => input.AiResponse.Result.Action.Equals(StacyContstants.ApiAiActions.LoadMorePresenters, StringComparison.OrdinalIgnoreCase),
                    async input => await _mediator.Send( new GetFBMessageForLoadMorePresenters(input))
                ),
                //  new ApiAiRequestMatch(
                //    // If action is respondPostbackText then respond hidden text which was inside json.
                //    input => input.AiResponse.Result.Action.Equals(StacyContstants.ApiAiActions.RespondPostbackText, StringComparison.OrdinalIgnoreCase),
                //    // Respond with the following text.
                //    async input => await _mediator.Send( new SendTextResponseToFacebook(input.Messaging.Sender.Id,GetPostBackMessage(input)))
                //),

                new ApiAiRequestMatch(
                
                    // If input is integer.
                    input => input.AiResponse.Result.ResolvedQuery.All(char.IsDigit),
                
                    // Respond with the integer inc by 1.
                    async input => await _mediator.Send( new SendTextResponseToFacebook(input.RequestId,input.Messaging.Sender.Id,(int.Parse(input.AiResponse.Result.ResolvedQuery) + 1).ToString())))

            };
        }

        private string GetPostBackMessage(FbPostback input)
        {
            dynamic json = JsonConvert.DeserializeObject(input.Payload);
            string responseText = json.responseText;
            return responseText;
        }

    }
}
