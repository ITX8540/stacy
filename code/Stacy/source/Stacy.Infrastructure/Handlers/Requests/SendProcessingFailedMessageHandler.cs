﻿using System.Threading.Tasks;
using Marten;
using MediatR;
using Stacy.Core;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain.Commands;

namespace Stacy.Infrastructure.Handlers.Requests
{
    public class SendProcessingFailedMessageHandler : IAsyncRequestHandler<SendProcessingFailedMessage, Unit>
    {
        private readonly IMediator _mediator;

        public SendProcessingFailedMessageHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Unit> Handle(SendProcessingFailedMessage message)
        {
            string response = "Sorry, there I couldn't handle your message! Try again.";
            var result = await _mediator.Send(new SendTextResponseToFacebook(message.RequestId, message.ReceipientId, response));
            var sendToApiRequest = new SendMessageToApi<FbMessaging>(result, StacyConfiguration.FbMessageEndpoint);
            await _mediator.Send(sendToApiRequest);
            
            return Unit.Value;
        }
    }
}
