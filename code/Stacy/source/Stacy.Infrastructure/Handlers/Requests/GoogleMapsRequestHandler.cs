﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using MediatR;
using Newtonsoft.Json;
using RestSharp.Extensions.MonoHttp;
using Stacy.Core.DTOs.Facebook;
using Stacy.Core.DTOs.Google;
using Stacy.DataAccess.Entities;
using Stacy.DataAccess.Infrastructure;
using Stacy.Domain.Commands;

namespace Stacy.Infrastructure.Handlers.Requests
{
    public class GoogleMapsRequestHandler : IAsyncRequestHandler<GetGoogleMapsLinkMessage, FbMessaging>
    {
        
        const int MapPreviewZoom = 17;
        
        public async Task<FbMessaging> Handle(GetGoogleMapsLinkMessage message)
        {
            string address;
            string conferenceName = string.Empty;
            using (var db = new StacyDatabase())
            {
                var eventsRepo = new GenericRepository<Event>(db);
                var eventdetailsRepo = new GenericRepository<EventDetail>(db);
                var locationsRepo = new GenericRepository<EventLocation>(db);
                var conferenceId = eventsRepo.Get(x => x.ParentId == null).FirstOrDefault().Id;
                var eventDetail = eventdetailsRepo.Get(x => x.EventId == conferenceId).FirstOrDefault();
                conferenceName = eventDetail.Name;
                address = locationsRepo.Get(x => x.EventId == conferenceId).FirstOrDefault().FullAddress;
            }
            var location = await GetGoogleMapsLocationForAddressAsync(address);
            var composedMessage = FacebookExtensions.GetComposedFbMessage(new[]
            {
                new FbAttachmentPayloadElement
                {
                    Title = conferenceName,
                    Subtitle = address,
                    ImageUrl =
                        $"https://maps.googleapis.com/maps/api/staticmap?size=764x400&center={location.Lat},{location.Lng}&zoom={MapPreviewZoom}&markers={location.Lat},{location.Lng}",
                    ItemUrl = $"https://www.google.com/maps/place/{HttpUtility.UrlEncode(address)}",
                }
            });
            var messaging = new FbMessaging
            {
                Recipient = new FbRecipient { Id = message.Message.Messaging.Sender.Id },
                Message = composedMessage
            };
            return messaging;
        }
        private async Task<GoogleMapsLocation> GetGoogleMapsLocationForAddressAsync(string address)
        {
            using (var client = new HttpClient())
            {
                string resultString = await client.GetStringAsync(
                    $"https://maps.googleapis.com/maps/api/geocode/json?address={HttpUtility.UrlEncode(address)}");
                return JsonConvert.DeserializeObject<GoogleMapsResponse>(resultString).Results[0].Geometry.Location;
            }
        }

        
    }
}