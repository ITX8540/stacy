﻿using System.Threading.Tasks;
using MediatR;
using Stacy.Core;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain.Commands;
using Stacy.Domain.Events;

namespace Stacy.Infrastructure.Handlers.Requests
{
    public class SendSenderActionToFacebookHandler : IAsyncRequestHandler<SendSenderActionToFacebook,SenderActionSentToFacebook>
                                                        , IAsyncRequestHandler<SendTypingOnAction,TypingOnActionSent>
                                                        , IAsyncRequestHandler<SendTypingOffAction, TypingOffActionSent>
    {
        private readonly IMediator _mediator;
        public SendSenderActionToFacebookHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<SenderActionSentToFacebook> Handle(SendSenderActionToFacebook message)
        {
            var sendActionToApi = new SendActionToApi<FbAction>(message.RequestId,message.Action, StacyConfiguration.FbMessageEndpoint);
            await _mediator.Send(sendActionToApi);
            return new SenderActionSentToFacebook(message.RequestId);
        }

        public async Task<TypingOnActionSent> Handle(SendTypingOnAction message)
        {
            var typingOnAction = new FbAction
            {
                Recipient = new FbRecipient
                {
                    Id = message.ReceipientId
                },
                SenderAction = StacyContstants.SenderActions.TypingOn
            };
            var request = new SendSenderActionToFacebook(message.RequestId,typingOnAction);
            await _mediator.Send(request);
            return new TypingOnActionSent(message.RequestId,message.ReceipientId);
        }

        public async Task<TypingOffActionSent> Handle(SendTypingOffAction message)
        {
            var typingOnAction = new FbAction
            {
                Recipient = new FbRecipient
                {
                    Id = message.ReceipientId
                },
                SenderAction = StacyContstants.SenderActions.TypingOff
            };
            var request = new SendSenderActionToFacebook(message.RequestId, typingOnAction);
            await _mediator.Send(request);
            return new TypingOffActionSent(message.RequestId, message.ReceipientId);
        }
    }
}
