﻿using System;
using System.Threading.Tasks;
using Marten;
using MediatR;
using Stacy.Core;
using Stacy.Domain.Commands;
using Stacy.Domain.Events;

namespace Stacy.Infrastructure.Handlers.Requests
{
    public class MessageHandler : IAsyncRequestHandler<StoreMessage, bool>
    {
        public async Task<bool> Handle(StoreMessage message)
        {
            var store = DocumentStore.For(StacyConfiguration.StacyDocumentStoreConnectionString);
            using (var session = store.OpenSession())
            {
                try
                {
                    var facebookSenderId = Guid.Empty.ToString();
                    if (message.Request.Result.Parameters != null && message.Request.Result.Parameters.ContainsKey("facebook_sender_id"))
                    {
                        facebookSenderId = message.Request.Result.Parameters["facebook_sender_id"];
                    }
                    var messageReceived = new MessageReceived(Guid.NewGuid(), DateTimeOffset.Now,facebookSenderId ,message.Request.Result.ResolvedQuery,message.Request.Result.Action);
                    session.Store(messageReceived);
                    await session.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    Log.TraceCatch(e);
                    return false;
                }
                return true;
            }
            
        }
    }
}