﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Newtonsoft.Json;
using Stacy.Core.DTOs.Facebook;
using Stacy.DataAccess.Entities;
using Stacy.DataAccess.Infrastructure;
using Stacy.Domain.Commands;

namespace Stacy.Infrastructure.Handlers.Requests
{
    public class GetEventsInfoMessageHandler : IAsyncRequestHandler<GetNextEventsMessage, FbMessaging>, IAsyncRequestHandler<GetFbMessageForEventsOfPresenter, FbMessaging>
    {
        public async Task<FbMessaging> Handle(GetNextEventsMessage message)
        {
            var fbAttachmentPayloadElements = GetEventMessages(true);
            
            var composedFbMessage = FacebookExtensions.GetComposedFbMessage(fbAttachmentPayloadElements);

            var messaging = new FbMessaging
            {
                Recipient = new FbRecipient { Id = message.Message.Messaging.Sender.Id },
                Message = composedFbMessage
            };
            return messaging;
        }

        private  FbAttachmentPayloadElement[] GetEventMessages(Boolean onlyFutureOnes = false,
            string presenterId = null)
        {
            FbAttachmentPayloadElement[] eventMessages = null;

            using (var db = new StacyDatabase())
            {
                var presentersRepo = new GenericRepository<EventPresenter>(db);
                var roomsRepo = new GenericRepository<EventRoom>(db);
                var eventDetailsRepo = new GenericRepository<EventDetail>(db);
                var eventRepo = new EventRepository(db);


                List<Guid> eventIds;

                //TODO: review this logic to be more dynamic query
                if (presenterId != null && onlyFutureOnes == false)
                {
                    eventIds =
                        eventDetailsRepo.Get(x => x.PresenterId == new Guid(presenterId))
                            .Select(x => x.EventId)
                            .ToList();
                }
                else if (presenterId != null)
                {
                    eventIds =
                        eventDetailsRepo.Get(x => x.PresenterId == new Guid(presenterId) && x.EndsAt >= DateTime.Now)
                            .Select(x => x.EventId)
                            .ToList();
                }
                else if (onlyFutureOnes)
                {
                    eventIds = eventDetailsRepo.Get(x => x.EndsAt >= DateTime.Now).Select(x => x.EventId).ToList();
                }
                else
                {
                    eventIds = eventDetailsRepo.Get().Select(x => x.EventId).ToList();
                }

                var nextEventsData =
                    eventRepo.Get(x => x.ParentId != null && eventIds.Contains(x.Id))
                        .OrderBy(x => x.EventDetails.FirstOrDefault().StartsAt)
                        .ToList();

                eventMessages = new FbAttachmentPayloadElement[nextEventsData.Count()];
                foreach (var _event in nextEventsData)
                {
                    EventDetail eventDetail = _event.EventDetails.FirstOrDefault();
                    var eventPresenters = presentersRepo.Get(x => x.Id == eventDetail.PresenterId);
                    EventPresenter eventPresenter = eventPresenters.FirstOrDefault();
                    EventRoom eventRoom = roomsRepo.Get(x => x.Id == eventDetail.EventRoomId).FirstOrDefault();

                    FbAttachmentPayloadElement eventMessage = ComposePayloadElementForEvent(_event, eventDetail,
                        eventPresenter, eventRoom);

                    eventMessages[nextEventsData.IndexOf(_event)] = eventMessage;
                }
            }
            
            return eventMessages;
            
        }

        private static FbAttachmentPayloadElement ComposePayloadElementForEvent(Event _event, EventDetail eventDetail, EventPresenter eventPresenter, EventRoom room)
        {
            string title = eventDetail.Name;
            if (!string.IsNullOrEmpty(eventDetail.Description))
            {
                title = title + " (" + _event.EventType.Description + ")";
            }

            string startsTime = eventDetail.StartsAt.Value.ToShortTimeString();
            string endsTime = eventDetail.EndsAt.Value.ToShortTimeString();
            string date = eventDetail.StartsAt.Value.ToShortDateString();

            string roomNameText = "";
            if (room != null)
            {
                string roomName = room.Name;
                roomNameText = " Room: " + roomName + ".";
            }

            string subtitle = "Time: " + startsTime + "-" + endsTime + " on " + date + "." + roomNameText;

            List<FbButton> buttonsList = new List<FbButton>();

            if (!string.IsNullOrEmpty(eventDetail.Description))
            {
                string buttonDescriptionTitle = "Description ?";
                string buttonDescriptionPayload = "{ \"postbackHiddenText\" : \"\" , \"responseText\" : \"Description: " + eventDetail.Description + "\"}"; ;
                buttonsList.Add(new FbButton
                {
                    Type = "postback",
                    Title = buttonDescriptionTitle,
                    Payload = buttonDescriptionPayload
                });
            }

            if (eventPresenter != null)
            {
                string buttonPresenterTitle = eventPresenter.Name + " ?";
                string buttonPresenterPayload = "{ \"postbackHiddenText\" : \"\" , \"responseText\" : \"" + eventPresenter.Name + " - " + eventPresenter.Description + "\"}";
                buttonsList.Add(new FbButton
                {
                    Type = "postback",
                    Title = buttonPresenterTitle,
                    Payload = buttonPresenterPayload
                });
            }

            FbButton[] buttons = null;
            if (buttonsList.Any())
            {
                buttons = new FbButton[buttonsList.Count()];
                buttons = buttonsList.ToArray();
            }

            return new FbAttachmentPayloadElement
            {
                Title = title,
                Subtitle = subtitle,
                Buttons = buttons
            };
        }

        public async Task<FbMessaging> Handle(GetFbMessageForEventsOfPresenter message)
        {
            dynamic json = JsonConvert.DeserializeObject(message.Message.AiResponse.Result.ResolvedQuery);
            string presenterId = json.presenterId;
            var fbAttachmentPayloadElements = GetEventMessages(false,presenterId);
            var composedFbMessage = FacebookExtensions.GetComposedFbMessage(fbAttachmentPayloadElements);

            var messaging = new FbMessaging
            {
                Recipient = new FbRecipient { Id = message.Message.Messaging.Sender.Id },
                Message = composedFbMessage
            };
            return messaging;
        }
    }
}