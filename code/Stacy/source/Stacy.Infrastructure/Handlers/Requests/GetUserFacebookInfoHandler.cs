﻿using System.Net.Http;
using System.Threading.Tasks;
using MediatR;
using Newtonsoft.Json;
using Stacy.Core;
using Stacy.Core.DTOs.Facebook;
using Stacy.Domain.Commands;
using Stacy.Domain.Events;

namespace Stacy.Infrastructure.Handlers.Requests
{
    public class GetUserFacebookInfoHandler : IAsyncRequestHandler<GetUserFacebookInfo,UserFacebookInfoRetrived>
    {
        public async Task<UserFacebookInfoRetrived> Handle(GetUserFacebookInfo message)
        {
            var userInfo = await GetInfoFromGraph(message.UserId, message.FacebookPageToken);
            if (userInfo != null)
            {
                return new UserFacebookInfoRetrived(message.UserId, userInfo.ProfileImageUrl, userInfo.FirstName, userInfo.LastName, message.RequestId, message.TimeStamp);
            }
            return new UserFacebookInfoRetrived(message.UserId,string.Empty,string.Empty,string.Empty,message.RequestId,message.TimeStamp);
        }

        private async Task<FacebookUserInfo> GetInfoFromGraph(string userId, string pageToken)
        {
            Log.TraceEntry();
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync($"https://graph.facebook.com/{StacyConfiguration.FacebookApiVersion}/{userId}?fields=first_name,last_name,profile_pic,locale,timezone,gender&access_token={pageToken}");
                
                if (response.IsSuccessStatusCode)
                {
                    Log.Info("Userinfo received");
                    var data = await response.Content.ReadAsStringAsync();
                    
                    Log.TraceExit();
                    var facebookUserInfo = JsonConvert.DeserializeObject<FacebookUserInfo>(data);
                    return facebookUserInfo;
                }
                
                Log.Info("Unable to get user info");
                Log.TraceExit();
                return null;


            }
        }
    }
}
