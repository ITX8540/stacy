﻿using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Stacy.Core.DTOs.Facebook;
using Stacy.DataAccess.Entities;
using Stacy.DataAccess.Infrastructure;
using Stacy.Domain.Commands;

namespace Stacy.Infrastructure.Handlers.Requests
{
    public class GetFbMessageForContactInfoHandler : IAsyncRequestHandler<GetFbMessageForContactInfo, FbMessaging>
    {
        public async Task<FbMessaging> Handle(GetFbMessageForContactInfo message)
        {
            using (var db = new StacyDatabase())
            {
                var eventsRepo = new EventRepository(db);
                var contactsRepo = new GenericRepository<EventContact>(db);
                var conference = eventsRepo.Get(x => x.ParentId == null).FirstOrDefault();
                var contactInfo = contactsRepo.Get(x => x.EventId == conference.Id).FirstOrDefault();


                string title = conference.EventDetails.FirstOrDefault().Name;
                string subtitle = "Phone: " + contactInfo.PhoneNumber + ". E-mail: " + contactInfo.Email;

                string linkSearchTitle = "Website";
                string searchUrl = contactInfo.Website;

                FbAttachmentPayloadElement contactInfoPayload = new FbAttachmentPayloadElement
                {
                    Title = title,
                    Subtitle = subtitle,
                    Buttons = new[]
                                {
                                    new FbButton
                                    {
                                        Type = "web_url",
                                        Title = linkSearchTitle,
                                        Url = searchUrl
                                    }
                                }
                };
                var composedMessage = FacebookExtensions.GetComposedFbMessage(new[] { contactInfoPayload });

                var messaging = new FbMessaging
                {
                    Recipient = new FbRecipient { Id = message.IncomingMessage.Messaging.Sender.Id },
                    Message = composedMessage
                };
                return messaging;
            }
        }
    }
}
