﻿using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Stacy.Core.DTOs.Facebook;
using Stacy.DataAccess.Entities;
using Stacy.DataAccess.Infrastructure;
using Stacy.Domain.Commands;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Stacy.Infrastructure.Handlers.Requests
{
    public  class GetPresentersMessageHandler : IAsyncRequestHandler<GetPresentersMessage, FbMessaging>, IAsyncRequestHandler<GetFBMessageForLoadMorePresenters, FbMessaging>
    {
        public async Task<FbMessaging> Handle(GetPresentersMessage message)
        {
            var fbAttachmentPayloadElements = GetPresentersMessages();

            var composedFbMessage = FacebookExtensions.GetComposedFbMessage(fbAttachmentPayloadElements);

            var messaging = new FbMessaging
            {
                Recipient = new FbRecipient { Id = message.Message.Messaging.Sender.Id },
                Message = composedFbMessage
            };
            return messaging;
        }

        private static FbAttachmentPayloadElement[] GetPresentersMessages(int startsAt = 0)
        {
            List<FbAttachmentPayloadElement> presenterMessages = new List<FbAttachmentPayloadElement>();

            using (var db = new StacyDatabase())
            {
                var presentersRepo = new GenericRepository<EventPresenter>(db);
                var presentersData = presentersRepo.Get().OrderBy(x => x.Name).ToList();
                
                foreach (var presenter in presentersData)
                {
                    string title = presenter.Name;
                    string subtitle = "";

                    if (!string.IsNullOrEmpty(presenter.Description))
                    {
                        subtitle = presenter.Description;
                    }

                    List<FbButton> buttonsList = new List<FbButton>();

                    if (presenterHasEvents(presenter.Id))
                    {
                        string buttonEventsOfPresenter = "Which events ?";
                        string buttonEventsOfPresenterPayload = "{ \"postbackPresenterEvents\" : \"\" , \"presenterId\" : \"" + presenter.Id + "\"}";
                        buttonsList.Add(new FbButton
                        {
                            Type = "postback",
                            Title = buttonEventsOfPresenter,
                            Payload = buttonEventsOfPresenterPayload
                        });
                    }

                    if (!string.IsNullOrEmpty(presenter.Name))
                    {
                        string linkSearchTitle = "More info from web";
                        string searchUrl = "http://www.google.com/search?q=" + presenter.Name.Replace(" ", "+");
                        buttonsList.Add(new FbButton
                        {
                            Type = "web_url",
                            Title = linkSearchTitle,
                            Url = searchUrl
                        });
                    }

                    FbButton[] buttons = null;
                    if (buttonsList.Any())
                    {
                        buttons = new FbButton[buttonsList.Count()];
                        buttons = buttonsList.ToArray();
                    }

                    FbAttachmentPayloadElement eventMessage = new FbAttachmentPayloadElement
                    {
                        Title = title,
                        Subtitle = subtitle,
                        Buttons = buttons
                    };
                    presenterMessages.Add(eventMessage);
                }
            }

            if (presenterMessages.Count > 10)
            {
                int size = 5;
                int loadUntil = startsAt + size;
                Boolean addLoadButton = true;

                if (loadUntil > presenterMessages.Count) { size = (presenterMessages.Count - startsAt); }
                if (loadUntil >= presenterMessages.Count) { addLoadButton = false; }

                presenterMessages = presenterMessages.GetRange(startsAt, size);

                if (addLoadButton)
                {
                    string buttonEventsOfPresenter = "Load more";
                    string buttonEventsOfPresenterPayload = "{ \"postbackLoadMorePresenters\" : \"\" , \"startingAt\" : \"" + loadUntil + "\"}";

                    FbAttachmentPayloadElement loadMessage = new FbAttachmentPayloadElement
                    {
                        Title = "There's even more of them.",
                        Subtitle = "",
                        Buttons = new[]
                                    {
                                            new FbButton
                                                {
                                                    Type = "postback",
                                                    Title = buttonEventsOfPresenter,
                                                    Payload = buttonEventsOfPresenterPayload
                                                }
                                        }
                    };

                    presenterMessages.Add(loadMessage);
                }
            }

            return presenterMessages.ToArray();
        }

        private static Boolean presenterHasEvents(Guid presenter_id)
        {
            using (var db = new StacyDatabase())
            {
                var eventDetailsRepo = new GenericRepository<EventDetail>(db);

                foreach (var _eventDetail in eventDetailsRepo.Get())
                {
                    if (_eventDetail.PresenterId == presenter_id) { return true; }
                }
            }

            return false;
        }

        public async Task<FbMessaging> Handle(GetFBMessageForLoadMorePresenters message)
        {
            dynamic json = JsonConvert.DeserializeObject(message.Message.AiResponse.Result.ResolvedQuery);
            var startingAt = json.startingAt;
            int skip = 3;
            bool isInt  = int.TryParse(startingAt.ToString(), out skip);

            var fbAttachmentPayloadElements = GetPresentersMessages(skip);

            var composedFbMessage = FacebookExtensions.GetComposedFbMessage(fbAttachmentPayloadElements);

            var messaging = new FbMessaging
            {
                Recipient = new FbRecipient { Id = message.Message.Messaging.Sender.Id },
                Message = composedFbMessage
            };
            return messaging;
        }

    }
}
