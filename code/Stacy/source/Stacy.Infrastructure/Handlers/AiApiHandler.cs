﻿using System;
using System.Threading.Tasks;
using ApiAiSDK;
using ApiAiSDK.Model;
using Stacy.Core;

namespace Stacy.Infrastructure.Handlers
{
    public class AiApiHandler
    {
        private AIDataService _dataService;
        public readonly string IncomingMessage;
        private readonly string _sessionId;

        public AiApiHandler(string incomingMessage, string sessionId)
        {
            IncomingMessage = incomingMessage;
            _sessionId = sessionId;
            _dataService = CreateApiAiDataService();
        }

        public Task<AIResponse> GetResponseAsync()
        {
            var request = new AIRequest(IncomingMessage);
            var tcs = new TaskCompletionSource<AIResponse>();
            try
            {
                tcs.SetResult(_dataService.Request(request));
            }
            catch (Exception ec)
            {
                tcs.SetException(ec);
                
            }
            return tcs.Task;
        }


        private AIDataService CreateApiAiDataService()
        {
            if (_dataService != null)
            {
                return _dataService;
            }
            var config = new AIConfiguration(StacyConfiguration.ApiAiToken, SupportedLanguage.English);
            config.DebugLog = true;

            if (!string.IsNullOrWhiteSpace(_sessionId))
            {
                config.SessionId = _sessionId;
            }
            return  _dataService = new AIDataService(config);
        }
    }
}
