﻿using System.Threading.Tasks;
using MediatR;

namespace Stacy.Infrastructure.CustomHandlers
{
    public class Pipeline<TRequest, TResponse>
    : IAsyncRequestHandler<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private readonly IAsyncRequestHandler<TRequest, TResponse> _inner;
        private readonly IPostRequestHandler<TRequest, TResponse>[] _postHandlers;

        public Pipeline(
            IAsyncRequestHandler<TRequest, TResponse> inner,
            IPostRequestHandler<TRequest, TResponse>[] postHandlers
            )
        {
            _inner = inner;
            _postHandlers = postHandlers;
        }

        public async Task<TResponse> Handle(TRequest message)
        {
            var response = await _inner.Handle(message);

            foreach (var postHandler in _postHandlers)
            {
                postHandler.Handle(message, response);
            }
            return response;
        }
    }
}
