﻿using MediatR;

namespace Stacy.Infrastructure.CustomHandlers
{
    public interface IPostRequestHandler<in TRequest, in TResponse> where TRequest : IRequest<TResponse>
    {
        void Handle(TRequest request, TResponse response);
    }
}
