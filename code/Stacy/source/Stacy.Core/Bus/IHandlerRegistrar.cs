﻿using Stacy.Core.Messages;

namespace Stacy.Core.Bus
{
    /// <summary>
    /// Handler Registrator Interface
    /// </summary>
    public interface IHandlerRegistrar
    {
        /// <summary>
        /// Register Message Handler
        /// </summary>
        /// <typeparam name="T"></typeparam>
        void RegisterHandler<T>() where T : IMessage;
    }
}
