﻿using System.Configuration;

namespace Stacy.Core
{
    public static class StacyConfiguration
    {
        public static string StacyDocumentStoreConnectionString => ConfigurationManager.AppSettings["Stacy.DocumentStore.ConnectionString"];

        public static string StacyEventStoreConnectionString => ConfigurationManager.AppSettings["Stacy.EventStore.ConnectionString"];

        public static string WitAiToken => ConfigurationManager.AppSettings["Stacy.WitAi.Token"];

        public static string ApiAiToken => ConfigurationManager.AppSettings["Stacy.ApiAi.Token"];

        public static string FbPageAccessToken => ConfigurationManager.AppSettings["Stacy.FB.PageAccessToken"];

        public static string FacebookApiVersion => ConfigurationManager.AppSettings["Stacy.Facebook.Api.Version"];

        public static string FbMessageEndpoint
            => $"https://graph.facebook.com/{FacebookApiVersion}/me/messages?access_token={FbPageAccessToken}";
    }
}
