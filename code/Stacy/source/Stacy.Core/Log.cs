﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using Newtonsoft.Json;

namespace Stacy.Core
{
    public static class Log
    {
        public static void TraceEntry(object arg = null,
            [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "")
            => TraceArg(arg, "Entry", filePath, lineNumber, memberName, val => Trace.WriteLine(val));        

        public static void Info(string message,
            [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "")
            => TraceArg(null, message, filePath, lineNumber, memberName, val => Trace.WriteLine(val));

        public static void Error(string message,
            [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "")
            => TraceArg(null, message, filePath, lineNumber, memberName, val => Trace.TraceError(val));

        public static void Warning(string message,
            [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "")
            => TraceArg(null, message, filePath, lineNumber, memberName, val => Trace.TraceWarning(val));

        public static void TraceCatch(Exception ex, 
            [CallerFilePath] string filePath = "", 
            [CallerLineNumber] int lineNumber = 0, 
            [CallerMemberName] string memberName = "")
            => TraceArg(ex, "Exception", filePath, lineNumber, memberName, val => Trace.TraceWarning(val));

        public static void TraceExit(object arg = null,
            [CallerFilePath] string filePath = "",
            [CallerLineNumber] int lineNumber = 0,
            [CallerMemberName] string memberName = "")
            => TraceArg(arg, "Exit", filePath, lineNumber, memberName, val => Trace.WriteLine(val));

        private static void TraceArg(object arg, string title, string filePath, int lineNumber, string memberName, Action<string> trace)
        {
            var sb = new StringBuilder();
            sb.AppendLine($"File: {filePath}");
            sb.AppendLine($"Method: {memberName} #{lineNumber}");
            sb.AppendLine();
            sb.AppendLine(title);
            if (arg != null)
            {
                sb.AppendLine();
                sb.AppendLine(JsonConvert.SerializeObject(arg));
            }
            trace(sb.ToString());
            Trace.WriteLine(sb.ToString());
        }
    }
}