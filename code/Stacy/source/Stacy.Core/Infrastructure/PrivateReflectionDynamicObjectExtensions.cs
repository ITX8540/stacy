﻿namespace Stacy.Core.Infrastructure
{

    /// <summary>
    /// PrivateReflection Extenion methods
    /// </summary>
    internal static class PrivateReflectionDynamicObjectExtensions
    {
        /// <summary>
        /// Get object as dynamic
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static dynamic AsDynamic(this object obj)
        {
            return PrivateReflectionDynamicObject.WrapObjectIfNeeded(obj);
        }
    }
}
