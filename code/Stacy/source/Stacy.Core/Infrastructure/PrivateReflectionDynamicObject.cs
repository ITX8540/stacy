﻿using System;
using System.Dynamic;
using System.Reflection;

namespace Stacy.Core.Infrastructure
{
    /// <summary>
    /// Helper object to handle Aggregates dynamically
    /// https://blogs.msdn.microsoft.com/davidebb/2010/01/18/use-c-4-0-dynamic-to-drastically-simplify-your-private-reflection-code/
    /// </summary>
    internal class PrivateReflectionDynamicObject : DynamicObject
    {
        /// <summary>
        /// The object created
        /// </summary>
        public object RealObject { get; set; }

        /// <summary>
        /// The members and types retreived for object
        /// </summary>
        private const BindingFlags ClassBindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

        /// <summary>
        /// Wrap the object in order to access the properties
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal static object WrapObjectIfNeeded(object obj)
        {
            if (obj == null || obj.GetType().IsPrimitive || obj is string)
            {
                return obj;
            }
            return new PrivateReflectionDynamicObject {RealObject = obj};
        }

        /// <summary>
        /// In order to call wrapping
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="arguments"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] arguments, out object result)
        {
            result = InvokeMemberOnType(RealObject.GetType(), RealObject, binder.Name, arguments);
            result = WrapObjectIfNeeded(result);
            return true;
        }

        /// <summary>
        /// Call member of request type from object
        /// </summary>
        /// <param name="type"></param>
        /// <param name="targetObject"></param>
        /// <param name="name"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        private static object InvokeMemberOnType(Type type, object targetObject, string name, object[] arguments)
        {
            var argumentTypes = new Type[arguments.Length];
            for (int i = 0; i < arguments.Length; i++)
            {
                argumentTypes[i] = arguments[i].GetType();
            }
            while (true)
            {
                var member = type.GetMethod(name,ClassBindingFlags,null, argumentTypes,null);
                if (member != null)
                {
                    return member.Invoke(targetObject, arguments);
                }

                if (type.BaseType == null)
                {
                    return null;
                }
                type = type.BaseType;
            }
        }
    }
}
