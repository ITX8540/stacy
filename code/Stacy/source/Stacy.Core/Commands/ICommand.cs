﻿using Stacy.Core.Messages;

namespace Stacy.Core.Commands
{
    /// <summary>
    /// Command invoked in the system
    /// </summary>
    public interface ICommand : IMessage
    {
         
    }
}