﻿namespace Stacy.Core.Commands
{
    /// <summary>
    /// Command Sender
    /// </summary>
    public interface ICommandSender
    {
        /// <summary>
        /// Send Command
        /// </summary>
        /// <typeparam name="T">CommandType</typeparam>
        /// <param name="command">Command issued</param>
        void Send<T>(T command) where T : ICommand;
    }
}