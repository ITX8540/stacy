﻿using Stacy.Core.Messages;

namespace Stacy.Core.Commands
{
    /// <summary>
    /// Command Handler for Command of type <see cref="ICommand"/> 
    /// </summary>
    /// <typeparam name="T">Command which will be handled by the Handler</typeparam>
    public interface ICommandHandler<in T> : IHandler<T> where T : ICommand
    {
         
    }
}