﻿namespace Stacy.Core
{
    public class StacyContstants
    {
        public static class ClassificationTypes
        {
            public static string EventType = "EventType";
            public static string RoomType = "RoomType";
        }

        public static class EventTypes
        {
            public static string Workshop = "Workshop";
            public static string Conference = "Conference";
            public static string Keynote = "Keynote";
        }

        public static class RoomTypes
        {
            public static string Classroom = "Classroom";
            public static string Auditorium = "Auditorium";
        }

        public static class ApiAiActions
        {
            public static string RespondPostbackText = "respondPostbackText";
            public static string NextEvents = "nextEvents";
            public static string FindLocation = "findlocation";
            public static string FindPresenters = "findPresenters";
            public static string ContactInfo = "findAdditionalInformation";
            public static string EventsOfPresenter = "findPresenterEvents";
            public static string LoadMorePresenters = "loadMorePresenters";
        }

        public static class SenderActions
        {
            public static string TypingOn = "typing_on";
            public static string TypingOff = "typing_off";
        }
    }
}
