﻿using System;

namespace Stacy.Core.Domain.Abstract
{
    public interface IAggregateRepository
    {
        void Save<T>(T aggregate, int? expectedVersion = null) where T : AggregateRoot;
        T Get<T>(Guid aggregateId) where T : AggregateRoot;
    }
}