﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stacy.Core.Events;

namespace Stacy.Core.Domain.Abstract
{
    interface IAggregate
    {
        IEnumerable<IEvent> FlushUnCommittedChanges();

        Guid Id { get; set; }

        int Version { get; set; }
    }
}
