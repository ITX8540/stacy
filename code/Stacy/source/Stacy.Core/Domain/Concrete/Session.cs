﻿using System;
using System.Collections.Generic;
using Stacy.Core.Domain.Abstract;
using Stacy.Core.Domain.Exceptions;

namespace Stacy.Core.Domain.Concrete
{
    public class Session : ISession
    {
        private readonly IAggregateRepository _aggregateRepository;
        private readonly Dictionary<Guid, AggregateDescriptor> _trackedAggregates;

        public Session(IAggregateRepository aggregateRepository)
        {
            if (aggregateRepository == null) throw new ArgumentNullException(nameof(aggregateRepository));

            _aggregateRepository = aggregateRepository;
            _trackedAggregates = new Dictionary<Guid, AggregateDescriptor>();
        }

        public void Add<T>(T aggregate) where T : AggregateRoot
        {
            if (!IsTracked(aggregate.Id))
            {
                _trackedAggregates.Add(aggregate.Id, new AggregateDescriptor {Aggregate = aggregate, Version = aggregate.Version});
            }
            else if (_trackedAggregates[aggregate.Id].Aggregate != aggregate)
            {
                throw new ConcurrencyException(aggregate.Id);
            }
        }

        

        public T Get<T>(Guid id, int? expectedVersion = null) where T : AggregateRoot
        {
            if (IsTracked(id))
            {
                var trackedAggregate = (T) _trackedAggregates[id].Aggregate;
                if (expectedVersion != null && trackedAggregate.Version != expectedVersion)
                {
                    throw new ConcurrencyException(trackedAggregate.Id);
                }
                return trackedAggregate;
            }

            var aggregate = _aggregateRepository.Get<T>(id);

            if (expectedVersion != null && aggregate.Version != expectedVersion)
            {
                throw new ConcurrencyException(id);
            }

            Add(aggregate);

            return aggregate;
        }

        private bool IsTracked(Guid aggregateId)
        {
            return _trackedAggregates.ContainsKey(aggregateId);
        }

        public void Commit()
        {
            foreach (var descriptor in _trackedAggregates.Values)
            {
                _aggregateRepository.Save(descriptor.Aggregate,descriptor.Version);
            }
            _trackedAggregates.Clear();
        }
    }
}
