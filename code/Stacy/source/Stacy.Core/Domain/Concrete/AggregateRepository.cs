﻿using System;
using System.Linq;
using Stacy.Core.Domain.Abstract;
using Stacy.Core.Domain.Exceptions;
using Stacy.Core.Domain.Factories;
using Stacy.Core.Events;

namespace Stacy.Core.Domain.Concrete
{
    public class AggregateRepository : IAggregateRepository
    {
        private readonly IEventStore _eventStore;
        private readonly IEventPublisher _publisher;

        public AggregateRepository(IEventStore eventStore)
        {
            if (_eventStore == null)
            {
                throw new ArgumentNullException(nameof(eventStore));
            }
            _eventStore = eventStore;
        }


        public void Save<T>(T aggregate, int? expectedVersion = null) where T : AggregateRoot
        {
            if (expectedVersion != null && _eventStore.Get<T>(aggregate.Id, expectedVersion.Value).Any())
            {
                throw new ConcurrencyException(aggregate.Id);
            }

            var changes = aggregate.FlushUnCommittedChanges();
            _eventStore.Save<T>(changes);
            if (_publisher != null)
            {
                foreach (IEvent @event in changes)
                {
                    _publisher.Publish(@event);
                }
            }
            else
            {
                if (_eventStore == null)
                {
                    throw new ArgumentNullException(nameof(_publisher));
                }
            }
        }

        public T Get<T>(Guid aggregateId) where T : AggregateRoot
        {
            return LoadAggregate<T>(aggregateId);
        }

        private T LoadAggregate<T>(Guid aggregateId) where T : AggregateRoot
        {
            var events = _eventStore.Get<T>(aggregateId,-1);

            if (!events.Any())
            {
                throw new AggregateNotFoundException(typeof(T),aggregateId);
            }

            var aggregate = AggregateFactory.CreateAggregate<T>();

            aggregate.LoadFromHistory(events);

            return aggregate;
        }
    }
}
