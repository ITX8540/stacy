using System;

namespace Stacy.Core.Domain.Exceptions
{
    public class AggregateNotFoundException : Exception
    {
        public AggregateNotFoundException(Type type, Guid aggregateId):base($"Aggregate {aggregateId} of type {type.FullName} was not found")
        {
            
        }
    }
}