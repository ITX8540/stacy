﻿using Stacy.Core.Domain.Abstract;
using Stacy.Core.Snapshots.Concrete;

namespace Stacy.Core.Snapshots.Abstract
{
    /// <summary>
    /// Abstraction of Snapshot
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SnapshotAggregateRoot<T> : AggregateRoot where T : Snapshot
    {
        /// <summary>
        /// Get Snapshot for Aggregate
        /// </summary>
        /// <returns></returns>
        public T GetSnapshot()
        {
            var snapshot = CreateSnapshot();
            snapshot.Id = Id;
            return snapshot;
        }

        /// <summary>
        /// Create snapshot
        /// </summary>
        /// <returns></returns>
        protected abstract T CreateSnapshot();

        /// <summary>
        /// Restore Snapshot for aggregate
        /// </summary>
        /// <param name="snapshot"></param>
        protected abstract void RestoreFromSnapshot(T snapshot);
    }
}
