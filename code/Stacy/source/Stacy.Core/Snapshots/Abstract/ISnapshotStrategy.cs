﻿using System;
using Stacy.Core.Domain.Abstract;

namespace Stacy.Core.Snapshots.Abstract
{
    public interface ISnapshotStrategy
    {
        bool ShouldMakeSnapshot(AggregateRoot aggregate);
        bool IsSnapshotable(Type aggregateType);
    }
}