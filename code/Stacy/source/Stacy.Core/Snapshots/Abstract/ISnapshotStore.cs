﻿using System;
using Stacy.Core.Snapshots.Concrete;

namespace Stacy.Core.Snapshots.Abstract
{
    public interface ISnapshotStore
    {
        Snapshot Get(Guid id);

        void Save(Snapshot snapshot);
    }
}