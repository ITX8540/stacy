﻿using System;
using System.Linq;
using Stacy.Core.Domain.Abstract;
using Stacy.Core.Domain.Factories;
using Stacy.Core.Events;
using Stacy.Core.Infrastructure;
using Stacy.Core.Snapshots.Abstract;

namespace Stacy.Core.Snapshots.Concrete
{
    public class SnapshotRepository
    {
        private readonly ISnapshotStore _snapshotStore;
        private readonly ISnapshotStrategy _snapshotStrategy;
        private readonly IAggregateRepository _aggregateRepository;
        private readonly IEventStore _eventStore;

        public SnapshotRepository(ISnapshotStore snapshotStore, ISnapshotStrategy snapshotStrategy, IAggregateRepository aggregateRepository, IEventStore eventStore)
        {
            if (snapshotStore == null) throw new ArgumentNullException(nameof(snapshotStore));
            if (snapshotStrategy == null) throw new ArgumentNullException(nameof(snapshotStrategy));
            if (aggregateRepository == null) throw new ArgumentNullException(nameof(aggregateRepository));
            if (eventStore == null) throw new ArgumentNullException(nameof(eventStore));
            _snapshotStore = snapshotStore;
            _snapshotStrategy = snapshotStrategy;
            _aggregateRepository = aggregateRepository;
            _eventStore = eventStore;
        }
        public void Save<T>(T aggregate, int? exectedVersion = null) where T : AggregateRoot
        {
            TryMakeSnapshot(aggregate);
            _aggregateRepository.Save(aggregate, exectedVersion);
        }

        public T Get<T>(Guid aggregateId) where T : AggregateRoot
        {
            var aggregate = AggregateFactory.CreateAggregate<T>();
            var snapshotVersion = TryRestoreAggregateFromSnapshot(aggregateId, aggregate);
            if (snapshotVersion == -1)
            {
                return _aggregateRepository.Get<T>(aggregateId);
            }
            var events = _eventStore.Get<T>(aggregateId, snapshotVersion).Where(desc => desc.Version > snapshotVersion);
            aggregate.LoadFromHistory(events);

            return aggregate;
        }

        private int TryRestoreAggregateFromSnapshot<T>(Guid id, T aggregate)
        {
            var version = -1;
            if (_snapshotStrategy.IsSnapshotable(typeof(T)))
            {
                var snapshot = _snapshotStore.Get(id);
                if (snapshot != null)
                {
                    aggregate.AsDynamic().Restore(snapshot);
                    version = snapshot.Version;
                }
            }
            return version;
        }

        private void TryMakeSnapshot(AggregateRoot aggregate)
        {
            if (!_snapshotStrategy.ShouldMakeSnapshot(aggregate))
            {
                return;
            }
            var snapshot = aggregate.AsDynamic().GetSnapshot().RealObject;
            snapshot.Version = aggregate.Version + aggregate.GetUnCommittedChanges().Count();
            _snapshotStore.Save(snapshot);
        }
    }
}
