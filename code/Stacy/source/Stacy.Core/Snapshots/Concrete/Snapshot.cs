﻿using System;

namespace Stacy.Core.Snapshots.Concrete
{
    public class Snapshot
    {
        public Guid Id { get; set; }
        public int Version { get; set; }

    }
}
