﻿using Stacy.Core.Messages;

namespace Stacy.Core.Events
{
    /// <summary>
    /// Event Handler for Event  of type <see cref="IEvent"/>
    /// </summary>
    /// <typeparam name="T">Event that will be handled by the handler</typeparam>
    public interface IEventHandler<in T> : IHandler<T> where T : IEvent
    { }
}