﻿namespace Stacy.Core.Events
{
    /// <summary>
    /// Event Publisher.
    /// </summary>
    public interface IEventPublisher
    {
        /// <summary>
        /// Publish the events in the Eventstore
        /// </summary>
        /// <typeparam name="T">Event type</typeparam>
        /// <param name="event">Event to be published</param>
        void Publish<T>(T @event) where T : IEvent;
    }
}