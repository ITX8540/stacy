﻿using System;
using Stacy.Core.Messages;

namespace Stacy.Core.Events
{
    /// <summary>
    /// Event handled in the system
    /// </summary>
    public interface IEvent : IMessage
    {
        /// <summary>
        /// Event Version. Used to keep track of order
        /// </summary>
        int Version { get; set; }
        /// <summary>
        /// Time of the Event creation
        /// </summary>
        DateTimeOffset TimeStamp { get; set; }
    }
}