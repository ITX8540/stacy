﻿using System;
using System.Collections.Generic;

namespace Stacy.Core.Events
{
    public interface IEventStore
    {
        /// <summary>
        /// Save the event.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="events"></param>
        void Save<T>(IEnumerable<IEvent> events);

        /// <summary>
        /// Retrieve the events for the aggregate
        /// </summary>
        /// <typeparam name="T">Aggregate <see cref="Stacy.Core.Domain.Abstract.AggregateRoot"/></typeparam>
        /// <param name="aggregateId">Aggregate id</param>
        /// <param name="fromVersion">From what version to get from</param>
        /// <returns></returns>
        IEnumerable<IEvent> Get<T>(Guid aggregateId, int fromVersion);
    }
}