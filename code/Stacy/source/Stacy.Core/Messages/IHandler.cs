﻿namespace Stacy.Core.Messages
{
    /// <summary>
    /// Message handler
    /// </summary>
    /// <typeparam name="T">Handle messages of type <see cref="IMessage"/></typeparam>
    public interface IHandler<in T> where T : IMessage
    {
        void Handle(T message);
    }
}