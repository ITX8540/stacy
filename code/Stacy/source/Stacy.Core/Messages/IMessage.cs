﻿using System;

namespace Stacy.Core.Messages
{
    /// <summary>
    /// Message sent within the system
    /// </summary>
    public interface IMessage
    {
        Guid Id { get; set; }
    }
}