﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Stacy.Core.DTOs.ApiAi
{
    public class ApiAiContext
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "parameters")]
        public Dictionary<string, string> Parameters { get; set; }

        [JsonProperty(PropertyName = "lifespan")]
        public int Lifespan { get; set; }
    }
}