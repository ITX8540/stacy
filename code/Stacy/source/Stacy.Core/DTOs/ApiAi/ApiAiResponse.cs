﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Stacy.Core.DTOs.ApiAi
{
    // Models taken from https://docs.api.ai/docs/webhook

    public class ApiAiResponse
    {
        [JsonProperty(PropertyName = "speech")]
        public string Speech { get; set; }

        [JsonProperty(PropertyName = "displayText")]
        public string DisplayText { get; set; }

        [JsonProperty(PropertyName = "data")]
        public Dictionary<string, object> Data { get; set; }

        [JsonProperty(PropertyName = "contextOut")]
        public ApiAiContext[] ContextOut { get; set; }

        [JsonProperty(PropertyName = "source")]
        public string Source { get; set; }        
    }
}