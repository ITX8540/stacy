﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Stacy.Core.DTOs.ApiAi
{
    public class ApiAiResult
    {
        [JsonProperty(PropertyName = "source")]
        public string Source { get; set; }

        [JsonProperty(PropertyName = "resolvedQuery")]
        public string ResolvedQuery { get; set; }

        [JsonProperty(PropertyName = "action")]
        public string Action { get; set; }

        [JsonProperty(PropertyName = "actionIncomplete")]
        public bool ActionIncomplete { get; set; }

        [JsonProperty(PropertyName = "parameters")]
        public Dictionary<string, string> Parameters { get; set; }

        [JsonProperty(PropertyName = "contexts")]
        public ApiAiContext[] Contexts { get; set; }

        [JsonProperty(PropertyName = "metadata")]
        public ApiAiMetadata Metadata { get; set; }

        [JsonProperty(PropertyName = "fulfillment")]
        public ApiAiFulfillment Fulfillment { get; set; }

        [JsonProperty(PropertyName = "score")]
        public double Score { get; set; }
    }
}