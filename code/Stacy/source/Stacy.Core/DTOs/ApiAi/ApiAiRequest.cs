﻿using System;
using Newtonsoft.Json;

namespace Stacy.Core.DTOs.ApiAi
{
    public class ApiAiRequest
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "timestamp")]
        public DateTime Timestamp { get; set; }

        [JsonProperty(PropertyName = "result")]
        public ApiAiResult Result { get; set; }

        [JsonProperty(PropertyName = "status")]
        public ApiAiStatus Status { get; set; }

        [JsonProperty(PropertyName = "sessionId")]
        public Guid SessionId { get; set; }
    }
}