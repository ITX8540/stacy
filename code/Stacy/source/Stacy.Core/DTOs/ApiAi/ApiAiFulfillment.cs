﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.ApiAi
{
    public class ApiAiFulfillment
    {
        [JsonProperty(PropertyName = "speech")]
        public string Speech { get; set; }
    }
}