﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.ApiAi
{
    public class ApiAiStatus
    {
        [JsonProperty(PropertyName = "code")]
        public int Code { get; set; }

        [JsonProperty(PropertyName = "errorType")]
        public string ErrorType { get; set; }
    }
}