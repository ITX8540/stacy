﻿using System;
using Newtonsoft.Json;

namespace Stacy.Core.DTOs.ApiAi
{
    public class ApiAiMetadata
    {
        [JsonProperty(PropertyName = "intentId")]
        public Guid IntentId { get; set; }

        [JsonProperty(PropertyName = "webhookUsed")]
        public string WebhookUsed { get; set; } // TODO: test if we can convert to bool instead

        [JsonProperty(PropertyName = "intentName")]
        public string IntentName { get; set; }
    }
}