﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Google
{
    public class GoogleMapsResponse
    {
        [JsonProperty(PropertyName = "results")]
        public GoogleMapsResult[] Results { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
    }
}