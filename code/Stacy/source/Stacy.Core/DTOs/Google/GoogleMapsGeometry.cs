﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Google
{
    public class GoogleMapsGeometry
    {
        [JsonProperty(PropertyName = "location")]
        public GoogleMapsLocation Location { get; set; }
    }
}