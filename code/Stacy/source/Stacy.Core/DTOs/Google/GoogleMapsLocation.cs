﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Google
{
    public class GoogleMapsLocation
    {
        [JsonProperty(PropertyName = "lat")]
        public double Lat { get; set; }

        [JsonProperty(PropertyName = "lng")]
        public double Lng { get; set; }
    }
}