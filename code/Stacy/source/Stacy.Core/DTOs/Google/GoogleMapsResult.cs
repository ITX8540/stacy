﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Google
{
    public class GoogleMapsResult
    {
        [JsonProperty(PropertyName = "formatted_address")]
        public string FormattedAddress { get; set; }

        [JsonProperty(PropertyName = "geometry")]
        public GoogleMapsGeometry Geometry { get; set; }
    }
}