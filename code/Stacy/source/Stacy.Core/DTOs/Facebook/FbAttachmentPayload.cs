﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbAttachmentPayload
    {
        [JsonProperty(PropertyName = "url", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "template_type", NullValueHandling = NullValueHandling.Ignore)]
        public string TemplateType { get; set; }

        [JsonProperty(PropertyName = "elements", NullValueHandling = NullValueHandling.Ignore)]
        public FbAttachmentPayloadElement[] Elements { get; set; }

        [JsonProperty(PropertyName = "coordinates", NullValueHandling = NullValueHandling.Ignore)]
        public FbCoordinates Coordinates { get; set; }
    }
}