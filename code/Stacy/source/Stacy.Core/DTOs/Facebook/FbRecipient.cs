﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbRecipient
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
    }
}