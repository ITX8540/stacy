﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbButton
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "url", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "payload", NullValueHandling = NullValueHandling.Ignore)]
        public string Payload { get; set; }
    }
}