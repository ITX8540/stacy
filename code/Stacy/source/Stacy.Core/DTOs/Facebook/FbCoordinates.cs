﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbCoordinates
    {
        [JsonProperty(PropertyName = "lat")]
        public double Lat { get; set; }

        [JsonProperty(PropertyName = "long")]
        public double Long { get; set; }
    }
}