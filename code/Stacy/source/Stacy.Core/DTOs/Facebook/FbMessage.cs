﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbMessage
    {
        [JsonProperty(PropertyName = "mid", NullValueHandling = NullValueHandling.Ignore)]
        public string MessageId { get; set; }

        [JsonProperty(PropertyName = "seq", NullValueHandling = NullValueHandling.Ignore)]
        public long? Seq { get; set; }

        [JsonProperty(PropertyName = "text", NullValueHandling = NullValueHandling.Ignore)]
        public object Text { get; set; }

        [JsonProperty(PropertyName = "quick_reply", NullValueHandling = NullValueHandling.Ignore)]
        public FbMessagePayload QuickReply { get; set; }

        /// <summary>
        /// NB! Only valid for responses to Facebook!!
        /// </summary>
        [JsonProperty(PropertyName = "attachment", NullValueHandling = NullValueHandling.Ignore)]
        public FbAttachment Attachment { get; set; }

        /// <summary>
        /// NB! Only valid for requests coming from Facebook!!
        /// </summary>
        [JsonProperty(PropertyName = "attachments", NullValueHandling = NullValueHandling.Ignore)]
        public FbAttachment[] Attachments { get; set; }
    }
}