﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FacebookUserInfo
    {
        [JsonProperty(PropertyName = "first_name", NullValueHandling = NullValueHandling.Ignore)]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "last_name", NullValueHandling = NullValueHandling.Ignore)]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "profile_pic", NullValueHandling = NullValueHandling.Ignore)]
        public string ProfileImageUrl { get; set; }
    }
}