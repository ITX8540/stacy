﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbSender
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
    }
}