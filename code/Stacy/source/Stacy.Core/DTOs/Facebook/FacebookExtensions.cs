﻿

using System;

namespace Stacy.Core.DTOs.Facebook
{
    public static class FacebookExtensions
    {
        public static FbMessaging AddMessage(this FbMessaging messaging, FbMessage message )
        {
            messaging.Message = message;
            return messaging;
        }

        public static FbMessage GetComposedFbMessage(this FbMessage message, FbAttachmentPayloadElement[] payloadElements)
        {
            message.Attachment = new FbAttachment
            {
                Type = "template",
                Payload = new FbAttachmentPayload
                {
                    TemplateType = "generic",
                    Elements = payloadElements
                }
            };
            return message;
        }

        public static FbMessage GetComposedFbMessage(FbAttachmentPayloadElement[] payloadElements)
        {
            return new FbMessage
            {
                Attachment = new FbAttachment
                {
                    Type = "template",
                    Payload = new FbAttachmentPayload
                    {
                        TemplateType = "generic",
                        Elements = payloadElements
                    }
                }
            };
        }
    }
}
