﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbAttachmentPayloadElement
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "subtitle")]
        public string Subtitle { get; set; }

        [JsonProperty(PropertyName = "image_url")]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "item_url", NullValueHandling = NullValueHandling.Ignore)]
        public string ItemUrl { get; set; }

        [JsonProperty(PropertyName = "buttons")]
        public FbButton[] Buttons { get; set; }
    }
}