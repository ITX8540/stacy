﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    // https://developers.facebook.com/docs/messenger-platform/webhook-reference


    public class FbPostback
    {
        [JsonProperty(PropertyName = "payload", NullValueHandling = NullValueHandling.Ignore)]
        public string Payload { get; set; }

        [JsonProperty(PropertyName = "referral", NullValueHandling = NullValueHandling.Ignore)]
        public FbPostbackReferral PostbackReferral { get; set; }
    }
}