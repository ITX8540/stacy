﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbAction
    {
        [JsonProperty(PropertyName = "recipient", NullValueHandling = NullValueHandling.Ignore)]
        public FbRecipient Recipient { get; set; }

        [JsonProperty(PropertyName = "sender_action", NullValueHandling = NullValueHandling.Ignore)]
        public string SenderAction { get; set; }
    }
}
