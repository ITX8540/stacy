﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbDelivery
    {
        [JsonProperty(PropertyName = "mids", NullValueHandling = NullValueHandling.Ignore)]
        public string[] MessageIds { get; set; }

        [JsonProperty(PropertyName = "watermark", NullValueHandling = NullValueHandling.Ignore)]
        public long? Watermark { get; set; }

        [JsonProperty(PropertyName = "seq", NullValueHandling = NullValueHandling.Ignore)]
        public long? Sequence { get; set; }
    }
}