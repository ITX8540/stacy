﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbMessagePayload
    {
        [JsonProperty(PropertyName = "payload")]
        public string Payload { get; set; }
    }
}