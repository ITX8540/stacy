﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbMessaging
    {
        [JsonProperty(PropertyName = "timestamp", NullValueHandling = NullValueHandling.Ignore)]
        public long? Timestamp { get; set; }

        [JsonProperty(PropertyName = "sender", NullValueHandling = NullValueHandling.Ignore)]
        public FbSender Sender { get; set; }

        [JsonProperty(PropertyName = "recipient", NullValueHandling = NullValueHandling.Ignore)]
        public FbRecipient Recipient { get; set; }

        [JsonProperty(PropertyName = "message", NullValueHandling = NullValueHandling.Ignore)]
        public FbMessage Message { get; set; }

        [JsonProperty(PropertyName = "optin", NullValueHandling = NullValueHandling.Ignore)]
        public FbOptin Optin { get; set; }

        [JsonProperty(PropertyName = "delivery", NullValueHandling = NullValueHandling.Ignore)]
        public FbDelivery Delivery { get; set; }

        [JsonProperty(PropertyName = "postback", NullValueHandling = NullValueHandling.Ignore)]
        public FbPostback Postback { get; set; }

        [JsonProperty(PropertyName = "read", NullValueHandling = NullValueHandling.Ignore)]
        public FbRead Read { get; set; }
    }

    public class FbRead
    {
        [JsonProperty(PropertyName = "watermark", NullValueHandling = NullValueHandling.Ignore)]
        public long? Watermark { get; set; }

        [JsonProperty(PropertyName = "seq", NullValueHandling = NullValueHandling.Ignore)]
        public long? Sequence { get; set; }
    }
}