﻿using Newtonsoft.Json;

namespace Stacy.Core.DTOs.Facebook
{
    public class FbPostbackReferral
    {
        [JsonProperty(PropertyName = "ref", NullValueHandling = NullValueHandling.Ignore)]
        public object Ref { get; set; }
        [JsonProperty(PropertyName = "source", NullValueHandling = NullValueHandling.Ignore)]
        public string Source { get; set; }
        [JsonProperty(PropertyName = "type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

    }
}