'use strict';

// Wit.ai parameters
const WIT_TOKEN = process.env.WIT_TOKEN;
if (!WIT_TOKEN) {
  throw new Error('missing WIT_TOKEN');
}

// Messenger API parameters
const FB_PAGE_TOKEN = process.env.FB_PAGE_TOKEN;

const APP_NAME = process.env.APP_NAME

var FB_VERIFY_TOKEN = process.env.FB_VERIFY_TOKEN;
if (!FB_VERIFY_TOKEN) {
  FB_VERIFY_TOKEN = "just_do_it";
}

const PersonEnum = {
    PRESENTER: 0,
    COORDINATOR: 1,
    VISITOR: 2,
    ORGANISER: 3,
    VOLUNTEER: 4,
    EMPLOYEE: 5,
    MENTOR: 6
};

const EventEnum = {
    CONFERENCE: 0,
    WORKSHOP: 1,
    LECTURE: 2,
    PARTY: 3,
    MISC: 4
};

const LocationInfoEnum = {
    NAME: 0,
    ADDRESS: 1,
    LONGITUDE: 2,
    LATITUDE: 3,
    DESCRIPTION: 4
}

const PersonInfoEnum = {
    NAME : 0,
    SURNAME : 1,
    BIRTHDAY : 2,
    EDUCATION : 3,
    COUNTRY_OF_ORIGIN: 4,
    ORGANISATION: 5,
    EMAIL: 6,
    CONTACT_PHONE: 7,
    OCCUPATION: 8,
    COMPETENCE: 9
}

const LocationEnum = {
    MAINVENUE: 0,
    HOUSING: 1,
    AUDITORY: 2,
    WORKBENCH : 3
}

const ClassificationTypeEnum = {
    PERSON: 0,
    PERSONINFO: 1,
    EVENT: 2,
    LOCATION: 3,
    LOCATIONINFO: 4,
    GROUP : 5
}

const GroupEnum = {
    CHIEFMENTORS: 0,
    TEAM: 1,
    ITMENTORS: 2,
    MARKETINGMENTORS: 3
}


module.exports = {
  WIT_TOKEN: WIT_TOKEN,
  FB_PAGE_TOKEN: FB_PAGE_TOKEN,
  FB_VERIFY_TOKEN: FB_VERIFY_TOKEN,
  APP_NAME: APP_NAME,
  PersonEnum: PersonEnum,
  EventEnum: EventEnum,
  LocationInfoEnum: LocationInfoEnum,
  ClassificationTypeEnum : ClassificationTypeEnum
};