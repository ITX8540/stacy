﻿require("babel-polyfill");
jest.dontMock('../models/eia_denormalised_data.js');

describe('Document data tests', () => {
    
    it('Check constructor', () => {
        var EiaDocumentData = require('../models/eia_denormalised_data.js');
        var eiaData = new EiaDocumentData();
        expect(eiaData.Users.length).not.toEqual(null);
        expect(eiaData.Teams.length).not.toEqual(null);
        expect(eiaData.Events.length).not.toEqual(null);
    });

    it('Get Team by Name', () => {
        var EiaDocumentData = require('../models/eia_denormalised_data.js');
        var eiaData = new EiaDocumentData();
        var team = eiaData.getTeamByNameFirstOrDefault("Stacy");
        expect(team.name).toEqual('Stacy');
    });

    it('Search for a team not in db', () => {
        var EiaDocumentData = require('../models/eia_denormalised_data.js');
        var eiaData = new EiaDocumentData();
        var team = eiaData.getTeamByNameFirstOrDefault("Lucy");
        expect(team).toEqual(null);
    });

    it('Get team for member name', () => {
        var EiaDocumentData = require('../models/eia_denormalised_data.js');
        var eiaData = new EiaDocumentData();
        var team = eiaData.getTeamByMemberName("Toomas");
        expect(team[0].name).toEqual('Stacy');
    });

    it('Get team for member name different case', () => {
        var EiaDocumentData = require('../models/eia_denormalised_data.js');
        var eiaData = new EiaDocumentData();
        var team = eiaData.getTeamByMemberName("toomas");
        expect(team[0].name).toEqual('Stacy');
    });

    it('Get team for member name different case', () => {
        var EiaDocumentData = require('../models/eia_denormalised_data.js');
        var eiaData = new EiaDocumentData();
        var team = eiaData.getTeamByMemberName("toomas");
        expect(team[0].name).toEqual('Stacy');
    });
    it('Get user by properties', () => {
        var queryData = {
            gender: "Male",
            firstName: "Toomas"
        }
        var EiaDocumentData = require('../models/eia_denormalised_data.js');
        var eiaData = new EiaDocumentData();
        var users = eiaData.getUserByPropsValue(queryData);
        console.log(users);
        expect(users).not.toEqual(null);
    });


});