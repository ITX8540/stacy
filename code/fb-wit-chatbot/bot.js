'use strict';


const Wit = require('node-wit').Wit;

const FB = require('./facebook.js');
const Config = require('./const.js');
const StacyEvent = require('./models/StacyEvent.js');
const EiaDocumentData = require('./models/eia_denormalised_data.js');
//const EiaData = require('./models/eia_stub_data.js');

const firstEntityValue = (entities, entity) => {
    const val = entities && entities[entity] &&
        Array.isArray(entities[entity]) &&
        entities[entity].length > 0 &&
        entities[entity][0].value;
if (!val) {
    return null;
  }
  return typeof val === 'object' ? val.value : val;
};

// Bot actions
const actions = {
  say(sessionId, context, message, cb) {
    console.log(message);

    // Bot testing mode, run cb() and return
    if (require.main === module) {
      cb();
      return;
    }

    // Our bot has something to say!
    // Let's retrieve the Facebook user whose session belongs to from context
    // TODO: need to get Facebook user name
    const recipientId = context._fbid_;
    if (recipientId) {
      // Yay, we found our recipient!
      // Let's forward our bot response to her.

      FB.fbMessage(recipientId, message, (err, data) => {
        if (err) {
          console.log(
            'Oops! An error occurred while forwarding the response to',
            recipientId,
            ':',
            err
          );
        }

        // Let's give the wheel back to our bot
        cb();
      });
    } else {
      console.log('Oops! Couldn\'t find user in context:', context);
      // Giving the wheel back to our bot
      cb();
    }
  },
  merge(sessionId, context, entities, message, cb) {
      // Retrieve the location entity and store it into a context field
      console.log('user said', message);
    const recipientId = context._fbid_;
    cb(context);
  },

  error(sessionId, context, error) {
    console.log(error.message);
  },

  // fetch-weather bot executes
  ['fetch-event-route'](sessionId, context, cb) {
    const recipientId = context._fbid_;

    let routeUrl = 'https://goo.gl/maps/bYgdcEiPnRM2';
    FB.googleRouteMessage(recipientId, routeUrl, (err, data) => {
        if (err) {
          console.log(
            'Oops! An error occurred while forwarding the response to',
            recipientId,
            ':',
            err
          );
        }

        // Let's give the wheel back to our bot
        cb();
      });
  },
  ['fetch-next-event'](sessionId, context, cb) {
      
      context.nextEvent = "Marketing lecture @ InspireLabs";

      cb(context);
  },
  //// fetch-weather bot executes
  //['fetch-weather'](sessionId, context, cb) {
  //  // Here should go the api call, e.g.:
  //  // context.forecast = apiCall(context.loc)
  //  context.forecast = 'sunny';
  //  cb(context);
  //},

  //['fetch-event'](sessionId, context, cb) {
    
  //  const stubData = new EiaData();

  //    let test = stubData.MainEvent;

  //    context.event = test.name + ' @ ' + test.location;

  //    cb(context);
  //}
};


const getWit = () => {
    return new Wit(Config.WIT_TOKEN, actions);
};

exports.getWit = getWit;

// bot testing mode
// http://stackoverflow.com/questions/6398196
if (require.main === module) {
    let a = 1;
    console.log("Bot testing mode.");
    const client = getWit();
    client.interactive();

}