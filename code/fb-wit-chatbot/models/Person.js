﻿'use strict';

class Person {
    constructor(id, firstName, lastName, personTypeId, parentId) {
        this.id = id;
        this.personTypeId = personTypeId;
        this.parentId = parentId;
    }

    get Id() {
        return this.id;
    }
    
    get PersonTypeId() {
        return this.personTypeId;
    }
}

module.exports = Person;