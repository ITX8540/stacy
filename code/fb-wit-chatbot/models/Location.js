﻿'use strict';

class Location {
    constructor(id, name, longitude, latitude, locationTypeId, parentId) {
        this.id = id;
        this.locationTypeId;
        this.parentId = parentId;
    }
    get Id() {
        return this.id;
    }
    get LocationTypeId() {
        return this.locationTypeId;
    }
    get ParentId() {
        return this.parentId;
    }
}

module.exports = Location;