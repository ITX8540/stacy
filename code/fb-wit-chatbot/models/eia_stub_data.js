'use strict';

const StacyEvent = require('./StacyEvent.js');
const Person = require('./Person.js');
const PersonInfo = require('./PersonInfo.js');
const Classification = require('./Classification.js');
const ClassificationType = require('./ClassificationType.js');
const Location = require('./Location.js');
const LocationInfo = require('./LocationInfo.js');
const Constants = require('../const.js');

class EiaData {
    
  constructor() {
    
      this.Events = [];
      this.Persons = [];
      this.Locations = [];
      this.LocationInfos = [];
      this.Classifications = [];
      this.ClassificationTypes = [];

      this.addClassificationTypes();
      this.addClassifications();
  }

  toObject() {
    return this.properties;
  }
  get MainEvent() {
      let mainEvents = this.EiaEvents.filter(event => event.parentId === undefined);
      if (mainEvents.length > 0) {
          return mainEvents[0];
      }
      return undefined;
  }
  get NextEvent() {
      var dateNow = Date.now();
      let nextEvent = this.EiaEvents.sort(x => x.start).filter(event => event.start > dateNow);

  }
  addClassificationTypes() {
      let id = 0;
        for (let clasType in Constants.ClassificationTypeEnum) {
            const newClassificationType = new ClassificationType(id, clasType, undefined);
            this.ClassificationTypes.push(newClassificationType);
            id = id + 1;
        }
  }
  addPersons() {
      const presenterClassification
          = this.getClassificationByValueAndType(Constants.PersonEnum.MENTOR, Constants.ClassificationType.PERSON);
      const onLu = new Person(0, "On", "Lu", presenterClassification.Id, undefined);
      let nextId = addToEiaArray(onlu, this.Persons);
      const krL6 = new Person(nextId, "Kristiina", "Lille�is", presenterClassification.Id, undefined);
      addToEiaArray(krL6, this.Persons);
  }


  getClassificationByValueAndType(classificationValue, classificationType) {
      const classificationTypeId = this.ClassificationTypes.filter(x => x.name = classificationType)[0].Id;
      const classification = this.Classifications
          .filter(x => x.value == classificationValue)
          .filter(x => x.classificationTypeId == classificationTypeId)[0];
  }
    addClassifications() {
        
        // Persons
        addClassificationsForType(Constants.ClassificationTypeEnum.PERSON, Constants.PersonEnum, this.ClassificationTypes, this.Classifications);

        // Person Info
        addClassificationsForType(Constants.ClassificationTypeEnum.PERSONINFO, Constants.PersonInfoEnum, this.ClassificationTypes, this.Classifications);

        // Events
        addClassificationsForType(Constants.ClassificationTypeEnum.EVENT, Constants.EventEnum, this.ClassificationTypes, this.Classifications);

        // Location
        addClassificationsForType(Constants.ClassificationTypeEnum.LOCATION, Constants.LocationEnum, this.ClassificationTypes, this.Classifications);

        // LocationInfo
        addClassificationsForType(Constants.ClassificationTypeEnum.LOCATIONINFO, Constants.LocationInfoEnum, this.ClassificationTypes, this.Classifications);

        // Groups
        addClassificationsForType(Constants.ClassificationTypeEnum.GROUP, Constants.GroupEnum, this.ClassificationTypes, this.Classifications);
  }

    addLocations() {
        const auditory = this.getClassificationByValueAndType(Constants.LocationEnum.AUDITORY, Constants.ClassificationType.LOCATION).Id;
        const workbench = this.getClassificationByValueAndType(Constants.LocationEnum.WORKBENCH, Constants.ClassificationType.LOCATION).Id;
        const inspireLabLoc = new Location(0, "InspireLab", 0, 0, auditory, undefined);
        let nextId = addToEiaArray(inspireLabLoc, this.Locations);
        const teamWorkRoomsLoc = new Location(nextId, "Teamwork Rooms", 0, 0, workbench, undefined);
        nextId = addToEiaArray(teamWorkRoomsLoc, this.Locations);
    }
    addEIAEvents() {
        const workshop = this.getClassificationByValueAndType(Constants.EventEnum.WORKSHOP, Constants.ClassificationType.EVENT).Id;
        const lecture = this.getClassificationByValueAndType(Constants.EventEnum.LECTURE, Constants.ClassificationType.EVENT).Id;
        const event1 = new StacyEvent(0, "Independent Team Work", "", new Date(2016, 7, 28, 10), new Date(2016, 7, 28, 14));
    }

    getLocations() {
        return this.Locations;
    }

    getLocationsByName(name) {
        return this.Locations.filter(x => x.Name == name)[0];
    }

    getClassificationType(classificationType) {
        return this.ClassificationTypes.filter(x => x.Id == classificationType)[0];
    }
}

function addClassificationsForType(classificationType, classifications,classificationTypes, dataArray) {
    const classificationTypeId = classificationTypes.filter(x => x.Id == classificationType)[0].Id;
    let id = dataArray.length;
    for (let classification in classifications) {
        const newClassification =
            new Classification(id, classification, classification, classificationTypeId, undefined);
        id  = addToEiaArray(newClassification, dataArray);
    }
}

function addToEiaArray(item, dataArray) {
    const oldDataArrayLength = dataArray.length;
    dataArray.push(item);
    const newDataArrayLength = dataArray.length;
    if (newDataArrayLength - oldDataArrayLength == 1) {
        return newDataArrayLength;
    }
}

module.exports = EiaData;