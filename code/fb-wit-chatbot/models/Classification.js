﻿'use strict';

class Classification {
    constructor(id, name, value, classificationTypeId, parentId) {

        this.id = id;
        this.name = name;
        this.value = value;
        this.classificationTypeId = classificationTypeId;
        this.parentId = parentId;

    }
    get Id() {
        return this.id;
    }
    get Name() {
        return this.id;
    }
    get Value() {
        return this.value;
    }
    get ClassificationTypeId() {
        return this.classificationTypeId;
    }

}

module.exports = Classification;