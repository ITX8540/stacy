﻿'use strict';

class StacyEvent {

    constructor(id, name, description, start, end, locationId, parentId, cordinatorId, organiserId, presenterIds, eventTypeId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.start = start;
        this.end = end;
        this.locationId = locationId;
        this.parentId = parentId;
        this.organiserId = organiserId;
        this.cordinatorId = cordinatorId;
        this.presenterIds = presenterIds;
        this.eventTypeId = eventTypeId;
    }

    get Id() {
        return this.id;
    }

    get Name() {
        return this.name;
    }

    get Description() {
        return this.description;
    }

    get StartFullDate() {
        return this.start;
    }

    get StartTime() {
        let hour = start.getHours();
        let minutes = start.getMinutes();
        return hour + ':' + minutes;
    }

    get EndFullDate() {
        return this.end;
    }

    get EndTime() {
        let hour = end.getHours();
        let minutes = end.getMinutes();
        return hour + ':' + minutes;
    }

    get Location() {
        return this.location;
    }
}

module.exports = StacyEvent;