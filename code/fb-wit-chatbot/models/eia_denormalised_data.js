﻿'use strict';

class EiaDocumentData {
    constructor() {
        this.Events = [];
        this.Teams = [];
        this.Users = [];
        initEvents(this.Events);
        initTeamsAndUsers(this.Teams, this.Users);
    }

    
    getEvents() {
        let events = this.Events;
        return events;
    }

    getNextEvent(currentDate) {
        let event = this.Events.filter(x => x.startDate > currentDate)[0];
        return event;
    }

    getEventByDate(targetDate) {
        let event = this.Events.filter(x => x.startDate == targetDate)[0];
        return event;
    }


    getTeams() {
        return this.Teams;
    }

    getTeamByNameFirstOrDefault(teamName) {
        let team = this.Teams.filter(x => x.name == teamName)[0];
        return team;
    }

    getTeamByMemberName(memberName) {
        let teams = [];
        let query = memberName.toLowerCase();
        let users = this.Users.filter(x => x.fullName.toLowerCase().includes(query));

        for (let i = 0; i < users.length; i++) {
            let teamName = users[i].team.name;
            let team = this.getTeamByNameFirstOrDefault(teamName);
            teams.push(team);
        }
        return teams;
    }

    getUserByPropsValue(propertySearcValueDict) {
        var results = [];
        for (let searchPropKey in propertySearcValueDict) {
            if (propertySearcValueDict.hasOwnProperty(searchPropKey)) {
                for (let i = 0; i < this.Users.length; i++) {
                    let user = this.Users[i];
                    let isUserValid = false;
                    if (user[searchPropKey] == propertySearcValueDict[searchPropKey]) {
                        isUserValid = true;
                    }
                    if (isUserValid) {
                        results.push(user);
                    }
                }
            }
        }
        //Filter out results that are matched for one property, but not other. 
        //Final results must match all properties.
        return results;
    }


}

const initTeamsAndUsers = (teamArray, userArray) => {
    let stacyTeam = {
        id: 1,
        name: "Stacy",
        mentors: [
            {
                name: "Bret Waters",
                type: "Chief Mentor",
                organisation: "Tivix"
            },
            {
                name: "Walter Dal Nut",
                type: "IT Mentor",
                organisation: "Corvy"
            },
            {
                name: "Mattias Liivak",
                type: "Marketing Mentor",
                organisation: "Fortumo"
            }
        ],
        members: [
            {
                firstName: "Toomas",
                lastName: "Talviste",
                fullName: "Toomas Talviste",
                occupation: "Software developer",
                organisation: "Tallinn Uninversity of Technology"
            },
            {
                firstName: "Ahmed",
                lastName: "H",
                fullName: "Ahmed H",
                occupation: "Marketing",
                organisation: "UK"
            },
            {
                firstName: "Emanuel",
                lastName: "W",
                fullName: "Emanuel W",
                occupation: "International Business",
                organisation: "AUS"
            },
            {
                firstName: "Frederico",
                lastName: "C",
                fullName: "Frederico C",
                occupation: "Business",
                organisation: "ITA"
            },
            {
                firstName: "Mark",
                lastName: "K",
                fullName: "Mark K",
                occupation: "Software developer",
                organisation: "EST"
            }
        ]
    }
    teamArray.push(stacyTeam);

    let toomas = {
        id: 1,
        firstName: "Toomas",
        lastName: "Talviste",
        fullName: "Toomas Talviste",
        occupation: "Software developer",
        organisation: "Tallinn Uninversity of Technology",
        team: stacyTeam,
        gender: 'Male'
    }
    let ahmed = {
        id: 2,
        firstName: "Ahmed",
        lastName: "H",
        fullName: "Ahmed H",
        occupation: "Marketing",
        organisation: "UK",
        team: stacyTeam,
        gender: 'Male'
    }
    let emanuel = {
        id: 3,
        firstName: "Emanuel",
        lastName: "W",
        fullName: "Emanuel W",
        occupation: "International Business",
        organisation: "AUS",
        team: stacyTeam,
        gender: 'Male'
    }
    let frederico = {
        id: 4,
        firstName: "Frederico",
        lastName: "C",
        fullName: "Frederico C",
        occupation: "Business",
        organisation: "ITA",
        team: stacyTeam,
        gender: 'Male'
    }
    let mark = {
        id: 5,
        firstName: "Mark",
        lastName: "K",
        fullName: "Mark K",
        occupation: "Software developer",
        organisation: "EST",
        team: stacyTeam,
        gender: 'Male'
    }

    userArray.push(toomas);
    userArray.push(ahmed);
    userArray.push(emanuel);
    userArray.push(frederico);
    userArray.push(mark);
}

const initEvents = (eventsArray) => {

    let inspireLabInfo = {
        name : "InspireLab",
        coordinates: { longitude: 45.0330565, latitude: 7.6664868 }
    }
    let eventId = 1;
    let eventName1 = "Fundraising in Focus: How Does It Work?";
    let eventSpeakers1 = [
        {
            name: "Ravi Belani",
            occupation: "Managing Partner, Alchemist",
            organisation: "Stanford University"
        }
    ];

    const event1 = createEventData(eventId, eventName1, inspireLabInfo, eventSpeakers1, new Date(2016, 7, 28, 10),  new Date(2016,7,28,11,30), 1.5);

    eventsArray.push(event1);

    eventId = eventId + 1;

    let eventName2 = "Fireside Chat: What Investors Look for - or Not - in Pitches?";
    let eventSpeakers2 = [
        {
            name: "Mike Reiner",
            occupation: "",
            organisation: "Amazon Web Services"
        },
        {
            name: "Ravi Belani",
            occupation: "Managing Partner, Alchemist",
            organisation: "Stanford University"
        },
        {
            name: "Simone Cimminelli",
            occupation: "",
            organisation: "iStarter"
        }
    ];

    const event2 = createEventData(eventId, eventName2, inspireLabInfo, eventSpeakers2, new Date(2016, 7, 28, 11,30), new Date(2016, 7, 28, 12), 0.5);
    eventsArray.push(event2);
}
const createEventData = (id,name, venueInfo, speakersData, startDate, endDate, duration) => {
    let event = {
        id : id,
        name: name,
        venue: venueInfo,
        speakers: speakersData,
        startDate: startDate,
        endDate: endDate,
        duration: duration
    };
    return event;
}

const findByProp = (element, property, searchValue) => {
    return element[property] == searchValue;
};

module.exports = EiaDocumentData;