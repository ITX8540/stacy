'use strict';

class LocationInfo {
    constructor(id, infoTypeId, parentId, value) {
        this.id = id;
        this.infoTypeId = infoTypeId;
        this.parentId = parentId;
        this.value = value;
    }

    get Id() {
        return this.id;
    }
    get InfoTypeId() {
        return this.infoTypeId;
    }
    get ParentId() {
        return this.parentId;
    }
    get Value() {
        return this.value;
    }
}

module.exports = LocationInfo;