# STACY [![Build status](https://ci.appveyor.com/api/projects/status/vsu5xcjo1rpc94de/branch/develop?svg=true)](https://ci.appveyor.com/api/projects/status/vsu5xcjo1rpc94de/branch/develop)
This project and repo are created for the purpose of the cource ITX8540 Team project - Startup

## Repository structure:

Documentation and code are kept in separate folders.


### Kausta code/database
In this folder we hold the database schema, init and migration scripts.

For these scripts to run.
PostgresSQL must be installed on your machine with all the command line tools.


*reinit.bat* re-creates the localhost database with the initial tables, functions.
*applypatches.bat* runs all the migration patches in the /patches folder.

#### patches
The template for a patch is the folder /script_templates folder

Patches must be named with the previous version and and the new version. Format is yyyy.mm.dd.nr_yyyy.mm.dd.nr+1.sql, for example 2016.09.27.01_2016.09.27.02.sql

NB! Pay attention to the DatabaseSchemaVersion


#### document store / event store

Also there is a added document store use case, which uses the a postgres database as a document store.
http://jasperfx.github.io/marten/getting_started/